#include "Frontend.h"

using namespace Magma;
using namespace MagmaFrontend;


FrontendApplication::FrontendApplication() {
	
}

FrontendApplication::~FrontendApplication() {
	SDELETE(m_ObjectCB);
}

void FrontendApplication::EnqueueMesh(Mesh* mesh) {

	/*
	DrawableItem drawable;
	drawable.CB.push_back(ConstantBufferBinding(m_ObjectCB));
	drawable.VB.push_back(VertexBufferBinding(mesh->GetVertexBuffer(), mesh->GetVertexDataStructureSize()));
	drawable.IB = IndexBufferBinding(mesh->GetIndexBuffer());
	drawable.DrawIndexed = DrawIndexedDesc(mesh->GetIndexCount());

	gRenderer->EnqueueDrawableItem(std::move(drawable));

	for (size_t i = 0; i < mesh->GetSubMeshCount(); i++) {
		EnqueueMesh(mesh->GetSubMesh(i));
	}
	*/
}

void FrontendApplication::OnInitialise() {
	Application::OnInitialise();

	m_MeshShader = gResources->GetDefaultShaderResourceMap().Add("gbuffer", "Assets\\Shaders\\GBuffer.hlsl", ERenderTechnique::TECHNIQUE_DEFERRED_GEOMETRY_PASS);
	m_FSQShader = gResources->GetDefaultShaderResourceMap().Add("FSQ_Textured", "Assets\\Shaders\\FSQ_Textured.hlsl", ERenderTechnique::TECHNIQUE_FULLSCREEN_QUAD);

	m_LoadedMeshResource = gResources->GetDefaultMeshResourceMap().Add("sponza", "Assets\\Models\\crytek_sponza\\banner.obj");
	m_LoadedMesh = m_LoadedMeshResource->GetMesh();

	m_RootNode = gScene->AddSceneNode("Root");
	Transform* root_node_transform = m_RootNode->GetComponent<Transform>();
	root_node_transform->SetScaling(Vector3(1,1,1));
	root_node_transform->SetPosition(Vector3(0, 0, 0), ESpace::LOCAL);

	m_SponzaNode = gScene->AddSceneNode("Triangle", "Root");
	MeshRenderProvider* tri_provider = m_SponzaNode->AddComponent<MeshRenderProvider>();
	tri_provider->SetMesh(m_LoadedMesh);
	tri_provider->SetShader(m_MeshShader->GetShader());

	SceneNode* camera_node = gScene->AddSceneNode("MainCamera");
	camera_node->AddComponent<CameraProvider>(45.0f, 1.0f, 0.1f, 1000.0f);
		
	gScene->SetMainCameraProvider(camera_node);	
}

void FrontendApplication::OnUpdate(float deltaSeconds) {
	Application::OnUpdate(deltaSeconds);

	assert(gGfx);

	m_Yaw += deltaSeconds * 10.0f;
	
	SceneNode* camera_node = gScene->GetMainCameraProviderNode();
	Transform* camera_transform = camera_node->GetComponent<Transform>();
	camera_transform->SetPosition(Vector3(0, 20, -50), ESpace::WORLD);
	camera_transform->SetRotation(Quaternion(0.0f, 0.0f, 0.0f), ESpace::LOCAL);
	//camera_transform->AddRotation(Quaternion(deltaSeconds*10.0f, 0.0f, 0.0f), ESpace::WORLD);

	Transform* t = m_SponzaNode->GetComponent<Transform>();
	t->SetPosition(Vector3(0, 0, 0), ESpace::WORLD);
	//t->SetRotation(Quaternion(m_Yaw, 0.0f, 0.0f), ESpace::WORLD);

	//gObjectCB.WorldMatrix = t->GetWorldMatrix();//Matrix4x4::Rotation(Quaternion(m_Yaw, 0.0f, 0.0f));
	//m_ObjectCB->Fill(&gObjectCB, sizeof(gObjectCB), D3D11_MAP_WRITE_DISCARD);	
}

// entry point
int APIENTRY wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR pScmdline, int iCmdshow) {
	FrontendApplication app;
	return app.Run();
}
