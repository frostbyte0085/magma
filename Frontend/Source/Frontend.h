#pragma once

#include <FrameworkInclude.hpp>


#ifdef _DEBUG
#pragma comment (lib, "Framework_d.lib")
#else
#pragma comment (lib, "Framework.lib")
#endif

namespace MagmaFrontend {
	class FrontendApplication : public Application {
	public:
		FrontendApplication();
		~FrontendApplication();

		virtual void OnInitialise() override;
		virtual void OnUpdate(float deltaSeconds) override;

		void EnqueueMesh(Mesh* mesh);
	private:
		SceneNode* m_RootNode = nullptr;
		SceneNode* m_SponzaNode = nullptr;
		ShaderResource* m_Shader = nullptr;
		ShaderResource* m_MeshShader = nullptr;
		ShaderResource* m_FSQShader = nullptr;

		ConstantBuffer* m_ObjectCB = nullptr;

		Mesh* m_LoadedMesh = nullptr;
		MeshResource* m_LoadedMeshResource = nullptr;
		
		float m_Yaw = 0.0f;				
	};
}