#pragma once

#include "../Framework.h"

namespace Magma {
	class UID {
	public:
		UID();

		std::string AsString() const;
		GUID Guid;
	};

	static bool operator == (const UID& lhs, const UID& rhs) {
		return (IsEqualGUID(lhs.Guid, rhs.Guid) == TRUE);
	}

	static bool operator != (const UID& lhs, const UID& rhs) {
		return !(lhs == rhs);
	}
}