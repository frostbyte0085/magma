#pragma once

#include <string>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

namespace Magma {
	// trim from start (in place)
	static inline void gTrimStringLeft(std::string &s) {
		s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
	}

	// trim from end (in place)
	static inline void gTrimStringRight(std::string &s) {
		s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
	}

	// trim from both ends (in place)
	static inline void gTrimString(std::string &s) {
		gTrimStringLeft(s);
		gTrimStringRight(s);
	}

	// lower string (in place)
	static inline void gLowerStringASCII(std::string &s) {
		std::transform(s.begin(), s.end(), s.begin(), ::tolower);
	}

	// upper string (in place)
	static inline void gUpperStringASCII(std::string &s) {
		std::transform(s.begin(), s.end(), s.begin(), ::toupper);
	}

	static void gSplitString(const std::string& s, char delim, std::vector<std::string>& elements) {
		std::stringstream ss;
		ss.str(s);
		std::string item;
		while (std::getline(ss, item, delim)) {
			elements.emplace_back(item);
		}
	}
}