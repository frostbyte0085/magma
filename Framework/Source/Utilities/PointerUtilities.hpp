#pragma once

#include <Framework.h>

namespace Magma {
	extern uint8_t* gAlignMemory(uint8_t* bytes, uintptr_t alignment);
}