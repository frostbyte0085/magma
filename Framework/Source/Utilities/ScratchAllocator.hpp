#pragma once

#include "AllocatorBase.hpp"

namespace Magma {
	class ScratchAllocator : public Allocator {
	public:
		ScratchAllocator(uint8_t* inStart, uint8_t* inEnd) : m_Start(inStart), m_End(inEnd), m_Next(inStart), m_MustFree(false) {

		}

		ScratchAllocator(size_t inSize) : m_Start(nullptr), m_End(nullptr), m_Next(nullptr), m_MustFree(true) {
			m_Start = (uint8_t*)realloc(m_Start, inSize);
			m_End = ((uint8_t*)m_Start) + inSize;
			m_Next = m_Start;
		}

		~ScratchAllocator() {
			if (m_Start && m_MustFree) {
				free(m_Start);
			}
			m_Start = m_End = m_Next = nullptr;
		}

		inline virtual uint8_t* Allocate(size_t inSize, uintptr_t inAlign) override {
			m_Next = gAlignMemory(m_Next, inAlign);

			uint8_t *newNextAllocation = ((uint8_t*)m_Next) + inSize;

			if (newNextAllocation > m_End) return nullptr;

			m_Next = newNextAllocation;
			return newNextAllocation;
		}

		inline virtual void Free(uint8_t* bytes) override {}

		inline void Reset() {
			m_Next = m_Start;
		}

		inline virtual float GetFreeMemoryKB() const override {
			return ( GetTotalMemoryKB() - GetUsedMemoryKB() );
		}

		inline virtual float GetUsedMemoryKB() const override {
			uint8_t* start = (uint8_t*)m_Start;
			uint8_t* next = (uint8_t*)m_Next;
			size_t diff = (size_t)next - (size_t)start;
			
			return static_cast<float>(diff) / 1024.0f;
		}

		inline virtual float GetTotalMemoryKB() const override {
			return static_cast<float>(_msize(m_Start)) / 1024.0f;
		}

	private:
		uint8_t *m_Start, *m_End, *m_Next;
		size_t m_MemoryUsed;
		bool m_MustFree;
	};
}