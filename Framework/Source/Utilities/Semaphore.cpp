#include "Semaphore.hpp"

using namespace Magma;

//
// Base Semaphore
//
Semaphore::Semaphore() {

}

Semaphore::~Semaphore() {

}

void Semaphore::Signal() {
	std::unique_lock<std::mutex> lock(m_Mutex);
	m_CV.notify_one();
}

void Semaphore::SignalAll() {
	std::unique_lock<std::mutex> lock(m_Mutex);
	m_CV.notify_all();
}

void Semaphore::Wait() {
	std::unique_lock<std::mutex> lock(m_Mutex);
	m_CV.wait(lock);
}

void Semaphore::WaitExpression(bool inExpression) {
	std::unique_lock<std::mutex> lock(m_Mutex);
	m_CV.wait(lock, [&]() {return inExpression; });
}

//
// Counter Semaphore
//
CounterSemaphore::CounterSemaphore(uint32_t inCount) : m_Count(inCount) {

}

CounterSemaphore::~CounterSemaphore() {

}

void CounterSemaphore::Signal() {
	std::unique_lock<std::mutex> lock(m_Mutex);
	++m_Count;
	m_CV.notify_one();
}

void CounterSemaphore::Wait() {
	std::unique_lock<std::mutex> lock(m_Mutex);
	m_CV.wait(lock, [&]()->bool { return m_Count > 0; });
}