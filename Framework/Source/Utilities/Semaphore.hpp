#pragma once

#include <Framework.h>

namespace Magma {
	class Semaphore : public NonCopyable {
	public:
		Semaphore();
		virtual ~Semaphore();

		void SignalAll();
		virtual void Signal();		
		virtual void Wait();
		void WaitExpression(bool inExpression);
		
	protected:
		std::condition_variable m_CV;
		std::mutex m_Mutex;
	};

	class CounterSemaphore : public Semaphore {
	public:
		CounterSemaphore(uint32_t inCount=0);
		~CounterSemaphore();

		virtual void Signal() override;
		virtual void Wait() override;
		uint32_t GetCount() const { return m_Count; }

	private:
		std::atomic_uint32_t m_Count;		
	};
}