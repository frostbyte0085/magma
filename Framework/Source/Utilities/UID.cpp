#include "Utilities/UID.hpp"

using namespace Magma;

UID::UID() {
	CoCreateGuid(&Guid);
}

std::string UID::AsString() const {
	char guidString[256];
	ZeroMemory(guidString, sizeof(char) * 256);
	sprintf_s(guidString, "%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X", Guid.Data1, Guid.Data2, Guid.Data3, Guid.Data4[0], Guid.Data4[1], Guid.Data4[2], Guid.Data4[3], Guid.Data4[4],
																				Guid.Data4[5], Guid.Data4[6], Guid.Data4[7]);
	std::stringstream stream;
	stream << guidString;

	return stream.str();
}