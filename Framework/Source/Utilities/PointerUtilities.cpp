#include "PointerUtilities.hpp"

namespace Magma {
	uint8_t* gAlignMemory(uint8_t* inBytes, uintptr_t inAlignment) {
		uintptr_t m = (uintptr_t)inBytes;
		m = (m + (inAlignment - 1)) & -static_cast<intptr_t>(inAlignment);

		assert(m >= (uintptr_t)inBytes);
		return (uint8_t*)m;
	}
}