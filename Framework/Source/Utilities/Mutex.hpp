#pragma once

#include <Framework.h>

namespace Magma {
	
	// a mutex that needs to be manually locked/unlocked
	template<class T>
	class Mutex : public NonCopyable {
	public:
		Mutex() {}
		~Mutex() {}

		inline void Lock() { m_Mutex.lock(); }
		inline void Unlock() { m_Mutex.unlock(); }
		
	private:
		T m_Mutex;
	};

	typedef Mutex<std::mutex> BasicMutex;
	typedef Mutex<std::recursive_mutex> RecursiveMutex;

	// an auto mutex, using raii paradigm
	template<class T>
	class ScopedLock : public NonCopyable {
	public:
		ScopedLock(const T* inMutex) { assert(inMutex); m_Mutex = const_cast<T*>(inMutex); m_Mutex->Lock(); }
		~ScopedLock() { m_Mutex->Unlock(); m_Mutex = nullptr; }

	private:
		T* m_Mutex;
	};
}