#pragma once

#include "PointerUtilities.hpp"

namespace Magma {
	class Allocator {
	public:
		inline virtual uint8_t* Allocate(size_t bytes, uintptr_t align) = 0;
		inline virtual void Free(uint8_t* bytes) = 0;
		inline virtual float GetFreeMemoryKB() const = 0;
		inline virtual float GetUsedMemoryKB() const = 0;
		inline virtual float GetTotalMemoryKB() const = 0;

		template <class T, std::uintptr_t alignment = sizeof(T), class... P> T* AllocateType(P... args) {
			uint8_t* bytes = Allocate(sizeof(T), alignment);
			if (bytes) {
				return new (bytes) T(std::forward<P>(args)...);
			}
			return nullptr;
		}

		template <class T> void FreeType(T* p) {
			if (p) {
				p->~T();
				Free((uint8_t*)p);
			}
		}
	};
}