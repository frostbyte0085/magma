#pragma once

#include "../Framework.h"

namespace Magma {
	//
	// some exceptions
	//
	class BaseException : public std::exception {
	public:
		explicit BaseException(const std::string& message) : msg(message) {
			std::cout << "Exception thrown: " << message << std::endl;
		}

		virtual const char* what() const _NOEXCEPT override { return msg.c_str(); }

	protected:
		std::string msg;

	};

	class BootException : public BaseException {
	public:
		explicit BootException(const std::string& message) : BaseException(message) {}
	};

	class GraphicsException : public BaseException {
	public:
		explicit GraphicsException(const std::string& message) : BaseException(message) {}
	};

	class SceneException : public BaseException {
	public:
		explicit SceneException(const std::string& message) : BaseException(message) {}
	};

	class InputException : public BaseException {
	public:
		explicit InputException(const std::string& message) : BaseException(message) {}
	};

	class IOException : public BaseException {
	public:
		explicit IOException(const std::string& message) : BaseException(message) {}
	};
}