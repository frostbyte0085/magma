#pragma once

#include "../Framework.h"

namespace Magma {
	class Window;
	class System;
	class Graphics;
	class Time;
	class Scene;
	class Resources;
	class Input;

	static const std::string kSystemWindow = "window";
	static const std::string kSystemGraphics = "graphics";
	static const std::string kSystemTime = "time";
	static const std::string kSystemScene = "scene";
	static const std::string kSystemResources = "resources";
	static const std::string kSystemInput = "input";
}