#include <Application/Application.hpp>
#include <Systems/Graphics/Graphics.hpp>
#include <Systems/Scene/Scene.hpp>
#include <Systems/Timing/Timing.hpp>
#include <Systems/Window/Window.hpp>
#include <Systems/Resources/Resources.hpp>
#include <Systems/Input/Input.hpp>
#include <Systems/Allocators.hpp>
#include <TaskManager/TaskManager.hpp>

using namespace Magma;

std::shared_ptr<Graphics> Magma::gGfx;
std::shared_ptr<Time> Magma::gTime;
std::shared_ptr<Window> Magma::gWnd;
std::shared_ptr<Scene> Magma::gScene;
std::shared_ptr<Resources> Magma::gResources;
std::shared_ptr<Input> Magma::gInput;

Application::Application() {
	
}

Application::~Application() {	
	m_Systems.clear();

	gFreeAllocators();
}

void Application::OnUpdate(float deltaSeconds) {

}

struct MyStruct {
	MyStruct() = default;
	MyStruct(uint32_t inIndex) : Index(inIndex) {}

	uint32_t Index = 0;
};

void HeavyTask(Task* inTask, const void* inData) {
	MyStruct *d = (MyStruct*)inData;

	for (int i = 0; i < 1000; i++) {
		int j = (i * i) + sqrtf((float)(i + 123));
		j = sinf(j + 1);
		j *= cosf(j + 1);
	}

	gTrace("Hello %i", d->Index);
}


int Application::Run() {
	
	try {
		OnInitialise();

		// and the task manager queues
		

		while (m_IsRunning && gWnd->IsAlive()) {

			// reset scratch allocators
			gScratchAlloc->Reset();
			
			gTaskMgr->Reset();

			/*
			const int N = 100;
			MyStruct s[N];

			gTrace("\nSTARTED\n");

			Semaphore sema;
			for (unsigned int i = 0; i < N; ++i)
			{
				s[i].Index = i;

				Task* job = gTaskMgr->CreateTask(&HeavyTask, &s[i]);
				job->Sema = &sema;				
				gTaskMgr->AddTask(job);
			}

			for (unsigned int i = 0; i < N; ++i) sema.Wait();

			gTrace("\nENDED\n");
			*/
			/*
			Task* task1 = gTaskMgr->CreateTask(HeavyTask, &MyStruct(1), nullptr, ETaskFlag::SYNCHRONOUS_FOLLOWUP_TASKS);

			Task* task2 = gTaskMgr->CreateTask(HeavyTask, &MyStruct(2));
			Task* task3 = gTaskMgr->CreateTask(HeavyTask, &MyStruct(3));
			Task* task4 = gTaskMgr->CreateTask(HeavyTask, &MyStruct(4));

			task1->AddFollowUpTask(task2);
			task1->AddFollowUpTask(task3);
			task1->AddFollowUpTask(task4);

			gTaskMgr->AddTask(task1);
			*/
			/*
			gTaskMgr->AddTask(task2);
			gTaskMgr->AddTask(task3);
			gTaskMgr->AddTask(task4);
			*/

			for (auto p : m_Systems) {
				p.second->Begin();
				p.second->Update(gTime->GetDeltaSeconds());

			}

			OnUpdate(gTime->GetDeltaSeconds());

			for (auto p : m_Systems) {
				p.second->End();
			}
		}
	}
	catch (BaseException& e) {
		std::cout << e.what() << std::endl;
		MessageBoxA(gWnd->GetHandle(), e.what(), "Exception caught!", MB_OK | MB_ICONERROR);
		return -1;
	}

	return 0;
}

#include <Systems/Graphics/CommandList.hpp>
#include <Systems/Graphics/Buffer.hpp>

void Application::OnInitialise() {
	
	gInitialiseAllocators();

	// task manager
	gTaskMgr = std::make_shared<TaskManager>();
	gTaskMgr->Initialise();

	// systems registration
	gWnd = RegisterSystem<Window>(kSystemWindow);
	gTime = RegisterSystem<Time>(kSystemTime);
	gResources = RegisterSystem<Resources>(kSystemResources);
	gGfx = RegisterSystem<Graphics>(kSystemGraphics);
	gScene = RegisterSystem<Scene>(kSystemScene);	
	gInput = RegisterSystem<Input>(kSystemInput);
	
	for (auto p : m_Systems) {
		p.second->Initialise();
	}

	m_IsRunning = true;
}
