#pragma once

#include "Enumerations.hpp"
#include <ErrorHandling/Exceptions.hpp>

namespace Magma {

	class Application : public std::enable_shared_from_this<Application> {
	public:
		Application();
		Application(const Application& other) = delete;
		Application& operator = (const Application& other) = delete;
		virtual ~Application();
				
		int Run();
		virtual void OnInitialise();
		virtual void OnUpdate(float deltaSeconds);

		template<class T>
		std::shared_ptr<T> RegisterSystem(const std::string& name) {
			if (m_Systems.find(name) != m_Systems.end()) {
				throw BootException("System with name " + name + " already exists!");
			}

			std::shared_ptr<T> instance = std::make_shared<T>();
			m_Systems[name] = instance;

			return instance;
		}

		template<class T>
		std::shared_ptr<T> GetSystem(const std::string& searchName) const {

			SystemMap::const_iterator it = m_Systems.find(searchName);
			if (it != m_Systems.end()) {
				return std::dynamic_pointer_cast<T>(it->second);
			}
			return nullptr;
		}

	protected:
		bool m_IsRunning;

		typedef std::unordered_map<std::string, std::shared_ptr<System>> SystemMap;
		SystemMap m_Systems;
	};

	extern std::shared_ptr<Graphics> gGfx;
	extern std::shared_ptr<Window> gWnd;
	extern std::shared_ptr<Time> gTime;
	extern std::shared_ptr<Scene> gScene;
	extern std::shared_ptr<Resources> gResources;
	extern std::shared_ptr<Input> gInput;
}
