#pragma once

#include<Application/Application.hpp>

// graphics
#include<Systems/Graphics/Bindings.hpp>
#include<Systems/Graphics/Shader.hpp>
#include<Systems/Graphics/Texture.hpp>
#include<Systems/Graphics/RenderTargetView.hpp>
#include<Systems/Graphics/DepthStencilView.hpp>
#include<Systems/Graphics/Buffer.hpp>
#include<Systems/Graphics/VertexFormats.hpp>
#include<Systems/Graphics/RenderPipelineSetup.hpp>
#include<Systems/Graphics/SamplerState.hpp>
#include<Systems/Graphics/RenderTarget.hpp>
#include<Systems/Graphics/DefaultResources.hpp>

// high level renderer
#include<Systems/Graphics/HighLevel/Renderer.hpp>
#include<Systems/Graphics/HighLevel/Mesh.hpp>
#include<Systems/Graphics/HighLevel/FSQ.hpp>

// timing
#include<Systems/Timing/Timer.hpp>
#include<Systems/Timing/Timing.hpp>

// scene
#include<Systems/Scene/Scene.hpp>
#include<Systems/Scene/SceneNode.hpp>
#include<Systems/Scene/Components/CameraProvider.hpp>
#include<Systems/Scene/Components/Transform.hpp>
#include<Systems/Scene/Components/MeshRenderProvider.hpp>

// resources
#include<Systems/Resources/Resources.hpp>
#include<Systems/Resources/MeshResource.hpp>
#include<Systems/Resources/TextResource.hpp>
#include<Systems/Resources/ShaderResource.hpp>

// task manager
#include<TaskManager/TaskManager.hpp>

// window
#include<Systems/Window/Window.hpp>

// math
#include<Math/MathInclude.h>

// utilities
#include<Utilities/ScratchAllocator.hpp>
#include<Utilities/StringHelper.hpp>
#include<Utilities/UID.hpp>
#include<Utilities/Hash.hpp>
#include <Utilities/Semaphore.hpp>
#include <Utilities/Mutex.hpp>

using namespace Magma;