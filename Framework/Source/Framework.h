#pragma once

#ifdef _MSC_VER
	#pragma comment(lib, "dxgi.lib")
	#pragma comment(lib, "d3d11.lib")
	#pragma comment(lib, "d3dcompiler.lib")
	#pragma comment(lib, "FreeImage.lib")

	#pragma comment(lib, "dinput8.lib")
	#pragma comment(lib, "dxguid.lib")

	#pragma comment(lib, "winmm.lib")

	#ifdef _DEBUG
	#pragma comment (lib, "tinyxml2_d.lib")
	#else
	#pragma comment (lib, "tinyxml2.lib")
	#endif

	#ifndef WIN32_LEAN_AND_MEAN
	#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers.
	#endif

	#ifndef NOMINMAX
	#define NOMINMAX
	#endif
#endif

#include <windows.h>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <assert.h>
#include <mutex>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <list>
#include <unordered_map>
#include <set>
#include <string>
#include <map>
#include <queue>
#include <stack>
#include <thread>
#include <memory>
#include <typeinfo>
#include <regex>
#include <sstream>
#include <wrl.h>
#include <ppltasks.h>
#include <cstdarg>
#include <cstdint>
#include <random>
#include <mmsystem.h>

// enum_flags
#include <enum_flags.h>

// libraries
// directx
#include <d3d11.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>

#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>

// parsers and serializers
#include <tinyxml/tinyxml2.h>

// freeimage
#include <freeimage/FreeImage.h>

// some common includes
#include <Utilities/StringHelper.hpp>
#include <ErrorHandling/Exceptions.hpp>
#include <Utilities/Mutex.hpp>

#pragma warning(disable:4201) // nonstandard extension used : nameless struct/union
#pragma warning(disable:4328) // nonstandard extension used : class rvalue used as lvalue
#pragma warning(disable:4324) // structure was padded due to __declspec(align())

using namespace Microsoft::WRL;

#ifndef SRELEASE
#define SRELEASE(x) if ((x)!=nullptr) (x)->Release(); (x)=nullptr;
#endif

#ifndef SDELETE
#define SDELETE(x) if ((x)!=nullptr) delete (x); (x)=nullptr;
#endif

#ifndef SDELETE_ARRAY
#define SDELETE_ARRAY(x) if ((x)!=nullptr) delete[] (x); (x)=nullptr;
#endif

#ifndef THROWFAIL
#define THROWFAIL(x, msg)  if (FAILED((x))) { throw std::exception(msg); }
#endif

#ifndef THROWEXPR
#define THROWEXPR(x, msg) if ((x) == nullptr) { throw std::exception(msg); }
#endif

#ifndef COMPILER_BARRIER
#define COMPILER_BARRIER _ReadWriteBarrier()
#endif

#ifndef MEMORY_BARRIER
#define MEMORY_BARRIER MemoryBarrier()
#endif

#ifndef ALIGN
#define ALIGN(x) __declspec(align((x)))
#define ALIGN8 ALIGN(8)
#define ALIGN16 ALIGN(16)
#define ALIGN32 ALIGN(32)
#define ALIGN64 ALIGN(64)
#define ALIGN128 ALIGN(128)
#endif

extern "C" {
	extern void gTrace(const char* inStr, ...);
}

#ifndef gAssert
#if defined (_DEBUG) || defined(DEBUG)
static void gAssertMsg(const char* inExpressionString, const char* inMessage, bool inExpression) {
	if (!inExpression)
	{
		std::stringstream error_string;
		error_string << "Assert failed:\t" << inMessage << std::endl << "Expected:\t" << inExpressionString << std::endl << "Source:\t\t" << __FILE__ << ", line " << __LINE__ << std::endl;
		gTrace(error_string.str().c_str());

		abort();
	}
}
#define gAssert(x, msg) gAssertMsg(#x, msg, (x))
#define gAssertD3D(x, msg) gAssert(SUCCEEDED((x)), msg)
#else
#define gAssert(x, msg) 
#endif
#endif

class NonCopyable
{
public:
	NonCopyable() = default;
	NonCopyable(const NonCopyable&) = delete;
	NonCopyable & operator=(const NonCopyable&) = delete;
};

void gSIMDMemCopy(void* __restrict Dest, const void* __restrict Source, size_t NumQuadwords);
void gSIMDMemFill(void* __restrict Dest, __m128 FillVector, size_t NumQuadwords);

static void hash_combine(std::size_t& seed) { }

template <typename T, typename... Rest>
static void hash_combine(std::size_t& seed, const T& v, Rest... rest) {
	std::hash<T> hasher;
	seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
	hash_combine(seed, rest...);
}

#ifndef MAKE_HASHABLE
#define MAKE_HASHABLE(type, ...) \
    namespace std {\
        template<> struct hash<type> {\
            size_t operator()(const type &t) const {\
                size_t ret = 0;\
                hash_combine(ret, __VA_ARGS__);\
                return ret;\
            }\
        };\
    }
#endif