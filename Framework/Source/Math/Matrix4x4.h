#pragma once

#include "MathLib.h"
#include "Vector4.h"

namespace Magma {
	class Vector3;
	class Quaternion;

	class Matrix4x4 {

	public:
		Matrix4x4(bool identity = true);
		Matrix4x4(float data[4][4]);
		Matrix4x4(const Matrix4x4 &other);
		~Matrix4x4();

		// assuming a world matrix, get the following:
		Vector3 GetTranslation() const;
		Vector3 GetScale() const;
		Quaternion GetRotation() const;

		// static members
		static Matrix4x4 identity;
		static Matrix4x4 zero;

		// static functions
		static Matrix4x4 Scale(const Vector3 &vec);
		static Matrix4x4 Scale(float x, float y, float z);

		static Matrix4x4 Translation(const Vector3 &vec);
		static Matrix4x4 Translation(float x, float y, float z);

		static Matrix4x4 Rotation(const Quaternion &rotation);

		static Matrix4x4 TRS(const Vector3& translation, const Quaternion& rotation, const Vector3& scale);

		// projections
		static Matrix4x4 Orthographic(float left, float right, float top, float bottom, float nearPlane, float farPlane);
		static Matrix4x4 Orthographic(float size, float aspect, float nearPlane, float farPlane);
		static Matrix4x4 Perspective(float fov, float aspect, float nearPlane, float farPlane);
		static Matrix4x4 Cabinet(float angle, float ratio, float nearPlane, float farPlane);

		// functions
		void SetRow(int row, float data[4]);
		void SetColumn(int column, float data[4]);

		Vector4 GetRow(int row) const;
		Vector4 GetColumn(int column) const;
		std::vector<float> GetMatrix() const;

		bool IsIdentity() const;

		void Transpose();
		Matrix4x4 GetTransposed();

		void Inverse();
		Matrix4x4 GetInverse();

		float GetDeterminant();

		// FIX ME NOW
		// operators
		Matrix4x4& operator *= (const Matrix4x4& rhs) {
			
			Matrix4x4 result = Matrix4x4::zero;

			for (int j = 0; j < 4; j++)
			{
				for (int i = 0; i < 4; i++)
				{
					for (int k = 0; k < 4; k++)
					{
						result.m_Matrix[j][i] += m_Matrix[j][k] * rhs.m_Matrix[k][i];
					}
				}				
			}
			
			for (int j = 0; j < 4; j++) {
				for (int i = 0; i < 4; i++) {
					m_Matrix[j][i] = result.m_Matrix[j][i];
				}
			}

			return *this;
		}

		const float* GetData() { return &m_Matrix[0][0]; }

	private:
		float m_Matrix[4][4];
	};

	inline static Matrix4x4 operator * (Matrix4x4 lhs, const Matrix4x4& rhs) {
		lhs *= rhs;
		return lhs;
	}

	static bool operator == (const Matrix4x4 &lhs, const Matrix4x4 &rhs) {

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				const std::vector<float>& mat1 = lhs.GetMatrix();
				const std::vector<float>& mat2 = rhs.GetMatrix();

				if (!CoreMath::IsNearlyEqual(mat1[i + j * 4], mat2[i + j * 4])) {
					return false;
				}
			}
		}

		return true;
	}

	static bool operator != (const Matrix4x4 &lhs, const Matrix4x4 &rhs) {

		return !(lhs == rhs);
	}
}