#include "Math/Vector4.h"
#include "Math/Vector3.h"

using namespace Magma;

// static members
Vector4 Vector4::one (1,1,0,1);
Vector4 Vector4::zero (0,0,0,0);
Vector4 Vector4::right (1,0,0,1);
Vector4 Vector4::up (0,1,0,1);
Vector4 Vector4::down (0,-1,0,1);
Vector4 Vector4::left (-1,0,0,1);
Vector4 Vector4::forward (0,0,1,1);
Vector4 Vector4::back (0,0,-1,1);


// static functions
Vector4 Vector4::Max(const Vector4 &v1, const Vector4 &v2) {
    float maxX = CoreMath::Max(v1[0], v2[0]);
    float maxY = CoreMath::Max(v1[1], v2[1]);
    float maxZ = CoreMath::Max(v1[2], v2[2]);
    float maxW = CoreMath::Max(v1[3], v2[3]);
    
    return Vector4(maxX, maxY, maxZ, maxW);
}

Vector4 Vector4::Min(const Vector4 &v1, const Vector4 &v2) {
    float minX = CoreMath::Min(v1[0], v2[0]);
    float minY = CoreMath::Min(v1[1], v2[1]);
    float minZ = CoreMath::Min(v1[2], v2[2]);
    float minW = CoreMath::Min(v1[3], v2[3]);
    
    return Vector4(minX, minY, minZ, minW);
}

Vector4 Vector4::Max3(const Vector4 &v1, const Vector4 &v2, const Vector4 &v3) {
    float maxX = CoreMath::Max3(v1[0], v2[0], v3[0]);
    float maxY = CoreMath::Max3(v1[1], v2[1], v3[1]);
    float maxZ = CoreMath::Max3(v1[2], v2[2], v3[2]);
    float maxW = CoreMath::Max3(v1[3], v2[3], v3[3]);
    
    return Vector4(maxX, maxY, maxZ, maxW);
}

Vector4 Vector4::Min3(const Vector4 &v1, const Vector4 &v2, const Vector4 &v3) {
    float minX = CoreMath::Min3(v1[0], v2[0], v3[0]);
    float minY = CoreMath::Min3(v1[1], v2[1], v3[1]);
    float minZ = CoreMath::Min3(v1[2], v2[2], v3[2]);
    float minW = CoreMath::Min3(v1[3], v2[3], v3[3]);
    
    return Vector4(minX, minY, minZ, minW);
}

float Vector4::Distance(const Vector4 &v1, const Vector4 &v2) {
    Vector4 v = (v2 - v1);
    return v.GetLength();
}

float Vector4::DistanceSqr(const Vector4 &v1, const Vector4 &v2) {
    Vector4 v = (v2 - v1);
    return v.GetLengthSqr();
}

float Vector4::Dot(const Vector4 &v1, const Vector4 &v2) {
    return (v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2] + v1[3] * v2[3]);
}

Vector4 Vector4::Lerp(const Vector4 &v1, const Vector4 &v2, float factor) {
    
    factor = CoreMath::Clamp01(factor);
    
    float xx = CoreMath::Lerp(v1[0], v2[0], factor);
    float yy = CoreMath::Lerp(v1[1], v2[1], factor);
    float zz = CoreMath::Lerp(v1[2], v2[2], factor);
    float ww = CoreMath::Lerp(v1[3], v2[3], factor);
    
    return Vector4(xx,yy,zz,ww);
}

// functions
Vector4::Vector4() {
	ZeroMemory(&Components, sizeof(float) * 4);
}

Vector4::Vector4(float x, float y, float z, float w) {
    Components[0] = x;
	Components[1] = y;
	Components[2] = z;
	Components[3] = w;
}

Vector4::Vector4(const Vector3& v, float w) {
	Components[0] = v[0];
	Components[1] = v[1];
	Components[2] = v[2];
	Components[3] = w;
}

Vector4::Vector4(float x) {
	for (uint32_t i = 0; i < 4; i++) {
		Components[i] = x;
	}
}

Vector4::Vector4(const Vector4& orig) {
	for (uint32_t i = 0; i < 4; i++) {
		Components[i] = orig[i];
	}
}

Vector4::Vector4(const float* xyzw) {
	for (uint32_t i = 0; i < 4; i++) {
		Components[i] = xyzw[i];
	}
}

Vector4::~Vector4() {
}

float Vector4::GetLength() const {
    return CoreMath::Sqrt( GetLengthSqr() );
}

float Vector4::GetLengthSqr() const {
    return Components[0] * Components[0] + Components[1] * Components[1] + Components[2] * Components[2] + Components[3] * Components[3];
}

void Vector4::Normalize() {
    float len = GetLength();
    if (CoreMath::IsNearlyZero(len)) {
		ZeroMemory(&Components, sizeof(float) * 4);
    }
    else {
		for (uint32_t i = 0; i < 4; i++) {
			Components[i] /= len;
		}
    }
}

Vector4 Vector4::GetNormalized() const {
    
    Vector4 v = *this;
    v.Normalize();
    return v;
}

void Vector4::MakeAbsolute() {
	for (uint32_t i = 0; i < 4; i++) {
		Components[i] = CoreMath::Abs(Components[i]);
	}    
}

Vector4 Vector4::GetAbsolute() const {
    Vector4 v = *this;
    v.MakeAbsolute();
    return v;
}

Vector4& Vector4::operator *= (const Matrix4x4& rhs) {
	float x =  Dot(rhs.GetColumn(0), Vector4(Components[0], Components[1], Components[2], Components[3]));
	float y = Dot(rhs.GetColumn(1), Vector4(Components[0], Components[1], Components[2], Components[3]));
	float z = Dot(rhs.GetColumn(2), Vector4(Components[0], Components[1], Components[2], Components[3]));
	float w = Dot(rhs.GetColumn(3), Vector4(Components[0], Components[1], Components[2], Components[3]));

	Components[0] = x;
	Components[1] = y;
	Components[2] = z;
	Components[3] = w;

	return *this;
}