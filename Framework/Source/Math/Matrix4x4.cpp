#include "Math/Matrix4x4.h"
#include "Math/Vector3.h"
#include "Math/Quaternion.h"

using namespace Magma;

Matrix4x4 Matrix4x4::identity(true);
Matrix4x4 Matrix4x4::zero(false);

Matrix4x4 Matrix4x4::TRS (const Vector3& translation, const Quaternion& rotation, const Vector3& scale) {
    Matrix4x4 m;
    
    Matrix4x4 t = Translation (translation);
    Matrix4x4 r = Rotation(rotation);
    Matrix4x4 s = Scale(scale);
    
    return t * r * s;
}

Matrix4x4 Matrix4x4::Rotation (const Quaternion& rotation) {
    Matrix4x4 m;
    
    Quaternion normalizedRotation = rotation.GetNormalized();
    
    float qx = normalizedRotation[0];
    float qy = normalizedRotation[1];
    float qz = normalizedRotation[2];
    float qw = normalizedRotation[3];
    
    float r1[4] = {1.0f - 2.0f*qy*qy - 2.0f*qz*qz, 2.0f*qx*qy - 2.0f*qz*qw, 2.0f*qx*qz + 2.0f*qy*qw, 0.0f};
    float r2[4] = {2.0f*qx*qy + 2.0f*qz*qw, 1.0f - 2.0f*qx*qx - 2.0f*qz*qz, 2.0f*qy*qz - 2.0f*qx*qw, 0.0f};
    float r3[4] = {2.0f*qx*qz - 2.0f*qy*qw, 2.0f*qy*qz + 2.0f*qx*qw, 1.0f - 2.0f*qx*qx - 2.0f*qy*qy, 0.0f};
    float r4[4] = {0, 0, 0, 1};
    
    m.SetRow(0, r1);
    m.SetRow(1, r2);
    m.SetRow(2, r3);
    m.SetRow(3, r4);
    
    return m;
}

Matrix4x4 Matrix4x4::Scale(const Vector3 &vec) {
    return Scale (vec[0], vec[1], vec[2]);
}

Matrix4x4 Matrix4x4::Scale(float x, float y, float z) {
    Matrix4x4 m;
    
    float r1[4] = {x, 0, 0, 0};
    float r2[4] = {0, y, 0, 0};
    float r3[4] = {0, 0, z, 0};
    float r4[4] = {0, 0, 0, 1};
    
    m.SetRow(0, r1);
    m.SetRow(1, r2);
    m.SetRow(2, r3);
    m.SetRow(3, r4);
    
    return m;
}

Matrix4x4 Matrix4x4::Translation(const Vector3 &vec) {
    return Translation(vec[0], vec[1], vec[2]);
}

Matrix4x4 Matrix4x4::Translation(float x, float y, float z) {
    Matrix4x4 m;

    float r[4] = {x, y, z, 1};
    m.SetRow(3, r);
    
    return m;
}

Matrix4x4 Matrix4x4::Orthographic(float left, float right, float top, float bottom, float nearPlane, float farPlane) {
    Matrix4x4 m;
    
    float c1[4] = { 2.0f / (right - left), 0, 0, 0 };
    float c2[4] = { 0, 2.0f / (top - bottom), 0, 0 };
    float c3[4] = { 0, 0, -1.0f / (farPlane - nearPlane), 0 };
    float c4[4] = { -(right + left) / (right - left), -(top + bottom) / (top - bottom), -nearPlane / (farPlane - nearPlane), 1 };
    
    m.SetRow(0, c1);
    m.SetRow(1, c2);
    m.SetRow(2, c3);
    m.SetRow(3, c4);
    
    return m;
}

Matrix4x4 Matrix4x4::Orthographic(float size, float aspect, float nearPlane, float farPlane) {
    
    float left = -size * (1.0f/aspect);
    float right = size * (1.0f/aspect);
    float top = size * aspect;
    float bottom = -size * aspect;
    
    return Matrix4x4::Orthographic(left, right, top, bottom, nearPlane, farPlane);
}

Matrix4x4 Matrix4x4::Perspective(float fov, float aspect, float nearPlane, float farPlane) {
    Matrix4x4 m;
    
    assert (fov > 0 && aspect > 0);
    
	fov = sDeg2Rad(fov);
    
    float depth = (farPlane - nearPlane);
    float invDepth = 1.0f / depth;
    
    float uh = 1.0f / CoreMath::Tan(0.5f * fov);
    float uw = uh / aspect;
    
    float c1[] = {uw, 0, 0, 0};
    float c2[] = {0, uh, 0, 0};
    float c3[] = {0, 0, farPlane * invDepth, 1};
    float c4[] = {0, 0, (-farPlane * nearPlane) * invDepth, 0};
    
	DirectX::XMMATRIX ss = DirectX::XMMatrixPerspectiveFovLH(fov, aspect, nearPlane, farPlane);

    m.SetRow(0, c1);
    m.SetRow(1, c2);
    m.SetRow(2, c3);
    m.SetRow(3, c4);
    
    return m;
}

Matrix4x4 Matrix4x4::Cabinet(float angle, float ratio, float near, float far) {
    Matrix4x4 m;
    
    return m;
}

Matrix4x4::Matrix4x4(bool identity) {
    memset(m_Matrix, 0, sizeof(float) * 16);
    
    if (identity) {

		for (int i = 0; i < 4; ++i) {
			for (int j = 0; j < 4; ++j) {
				if (i == j) {
					m_Matrix[i][j] = 1.0f;
				}
			}
        }
    }
}

Matrix4x4::Matrix4x4(const Matrix4x4& other) {
    memcpy(m_Matrix, other.m_Matrix, sizeof(other.m_Matrix));
}

Matrix4x4::Matrix4x4(float data[4][4]) {
    memcpy (m_Matrix, data, sizeof(float) * 16);
}

Matrix4x4::~Matrix4x4() {
    
}

Vector3 Matrix4x4::GetTranslation() const {
	return GetRow(3);
}

Vector3 Matrix4x4::GetScale() const {
	return Vector3(GetColumn(0).GetLength(), GetColumn(1).GetLength(), GetColumn(2).GetLength());
}

Quaternion Matrix4x4::GetRotation() const {
	Quaternion q;
	return q;
}

void Matrix4x4::SetRow(int row, float data[4]) {
    for (int i=0; i<4; ++i) {
        m_Matrix[row][i] = data[i];
    }
}

void Matrix4x4::SetColumn(int column, float data[4]) {
    for (int i=0; i<4; ++i) {
        m_Matrix[i][column] = data[i];
    }
}

std::vector<float> Matrix4x4::GetMatrix() const {
    std::vector<float> r;
    
    for (int i=0; i<4; ++i) {
        for (int j=0; j<4; ++j) {
            r.push_back(m_Matrix[i][j]);
        }
    }
  
    return r;
}

Vector4 Matrix4x4::GetRow(int row) const {
	Vector4 v;
    
    for (int i=0; i<4; ++i) {
        v.Components[i] = m_Matrix[row][i];
    }
    
    return v;
}

Vector4 Matrix4x4::GetColumn(int column) const {
	Vector4 v;
    
    for (int i=0; i<4; ++i) {
        v.Components[i] = m_Matrix[i][column];
    }
    
    return v;
}

void Matrix4x4::Transpose() {
    Matrix4x4 temp (*this);
    
    for (int i=0; i<4; ++i) {
        for (int j=0; j<4; ++j) {
            m_Matrix[i][j] = temp.m_Matrix[j][i];
        }
    }
}

Matrix4x4 Matrix4x4::GetTransposed() {
    Matrix4x4 m = *this;
    m.Transpose();
    return m;
}

Matrix4x4 Matrix4x4::GetInverse() {
    Matrix4x4 m = *this;
    m.Inverse();
    return m;
}

void Matrix4x4::Inverse() {
    
	float f_00 = m_Matrix[2][2] * m_Matrix[3][3] - m_Matrix[3][2] * m_Matrix[2][3];
	float f_01 = m_Matrix[2][1] * m_Matrix[3][3] - m_Matrix[3][1] * m_Matrix[2][3];
	float f_02 = m_Matrix[2][1] * m_Matrix[3][2] - m_Matrix[3][1] * m_Matrix[2][2];
	float f_03 = m_Matrix[2][0] * m_Matrix[3][3] - m_Matrix[3][0] * m_Matrix[2][3];
	float f_04 = m_Matrix[2][0] * m_Matrix[3][2] - m_Matrix[3][0] * m_Matrix[2][2];
	float f_05 = m_Matrix[2][0] * m_Matrix[3][1] - m_Matrix[3][0] * m_Matrix[2][1];
	float f_06 = m_Matrix[1][2] * m_Matrix[3][3] - m_Matrix[3][2] * m_Matrix[1][3];
	float f_07 = m_Matrix[1][1] * m_Matrix[3][3] - m_Matrix[3][1] * m_Matrix[1][3];
	float f_08 = m_Matrix[1][1] * m_Matrix[3][2] - m_Matrix[3][1] * m_Matrix[1][2];
	float f_09 = m_Matrix[1][0] * m_Matrix[3][3] - m_Matrix[3][0] * m_Matrix[1][3];
	float f_10 = m_Matrix[1][0] * m_Matrix[3][2] - m_Matrix[3][0] * m_Matrix[1][2];
	float f_11 = m_Matrix[1][1] * m_Matrix[3][3] - m_Matrix[3][1] * m_Matrix[1][3];
	float f_12 = m_Matrix[1][0] * m_Matrix[3][1] - m_Matrix[3][0] * m_Matrix[1][1];
	float f_13 = m_Matrix[1][2] * m_Matrix[2][3] - m_Matrix[2][2] * m_Matrix[1][3];
	float f_14 = m_Matrix[1][1] * m_Matrix[2][3] - m_Matrix[2][1] * m_Matrix[1][3];
	float f_15 = m_Matrix[1][1] * m_Matrix[2][2] - m_Matrix[2][1] * m_Matrix[1][2];
	float f_16 = m_Matrix[1][0] * m_Matrix[2][3] - m_Matrix[2][0] * m_Matrix[1][3];
	float f_17 = m_Matrix[1][0] * m_Matrix[2][2] - m_Matrix[2][0] * m_Matrix[1][2];
	float f_18 = m_Matrix[1][0] * m_Matrix[2][1] - m_Matrix[2][0] * m_Matrix[1][1];
	
	float m[16];
	m[0] = +m_Matrix[1][1] * f_00 - m_Matrix[1][2] * f_01 + m_Matrix[1][3] * f_02;
	m[1] = -m_Matrix[1][0] * f_00 + m_Matrix[1][2] * f_03 - m_Matrix[1][3] * f_04;
	m[2] = +m_Matrix[1][0] * f_01 - m_Matrix[1][1] * f_03 + m_Matrix[1][3] * f_05;
	m[3] = -m_Matrix[1][0] * f_02 + m_Matrix[1][1] * f_04 - m_Matrix[1][2] * f_05;

	m[4] = -m_Matrix[0][1] * f_00 + m_Matrix[0][2] * f_01 - m_Matrix[0][3] * f_02;
	m[5] = +m_Matrix[0][0] * f_00 - m_Matrix[0][2] * f_03 + m_Matrix[0][3] * f_04;
	m[6] = -m_Matrix[0][0] * f_01 + m_Matrix[0][1] * f_03 - m_Matrix[0][3] * f_05;
	m[7] = +m_Matrix[0][0] * f_02 - m_Matrix[0][1] * f_04 + m_Matrix[0][2] * f_05;

	m[8] = +m_Matrix[0][1] * f_06 - m_Matrix[0][2] * f_07 + m_Matrix[0][3] * f_08;
	m[9] = -m_Matrix[0][0] * f_06 + m_Matrix[0][2] * f_09 - m_Matrix[0][3] * f_10;
	m[10] = +m_Matrix[0][0] * f_11 - m_Matrix[0][1] * f_09 + m_Matrix[0][3] * f_12;
	m[11] = -m_Matrix[0][0] * f_08 + m_Matrix[0][1] * f_10 - m_Matrix[0][2] * f_12;

	m[12] = -m_Matrix[0][1] * f_13 + m_Matrix[0][2] * f_14 - m_Matrix[0][3] * f_15;
	m[13] = +m_Matrix[0][0] * f_13 - m_Matrix[0][2] * f_16 + m_Matrix[0][3] * f_17;
	m[14] = -m_Matrix[0][0] * f_14 + m_Matrix[0][1] * f_16 - m_Matrix[0][3] * f_18;
	m[15] = +m_Matrix[0][0] * f_15 - m_Matrix[0][1] * f_17 + m_Matrix[0][2] * f_18;
	
	for (uint32_t i = 0; i < 4; i++) {
		for (uint32_t j = 0; j < 4; j++) {
			m_Matrix[i][j] = m[i + j * 4];
		}
	}

	float determinant = this->m_Matrix[0][0] * m[0] + this->m_Matrix[0][1] * m[1] + this->m_Matrix[0][2] * m[2] + this->m_Matrix[0][3] * m[3];
	
	for (uint32_t i = 0; i < 4; i++) {
		for (uint32_t j = 0; j < 4; j++) {
			m_Matrix[i][j] /= determinant;
		}
	}
}

float Matrix4x4::GetDeterminant() {
    
    float m[4][4];
    memcpy (m, m_Matrix, sizeof(m_Matrix));
    
    return
    m[0][3] * m[1][2] * m[2][1] * m[3][0] - m[0][2] * m[1][3] * m[2][1] * m[3][0] -
    m[0][3] * m[1][1] * m[2][2] * m[3][0] + m[0][1] * m[1][3] * m[2][2] * m[3][0] +
    m[0][2] * m[1][1] * m[2][3] * m[3][0] - m[0][1] * m[1][2] * m[2][3] * m[3][0] -
    m[0][3] * m[1][2] * m[2][0] * m[3][1] + m[0][2] * m[1][3] * m[2][0] * m[3][1] +
    m[0][3] * m[1][0] * m[2][2] * m[3][1] - m[0][0] * m[1][3] * m[2][2] * m[3][1] -
    m[0][2] * m[1][0] * m[2][3] * m[3][1] + m[0][0] * m[1][2] * m[2][3] * m[3][1] +
    m[0][3] * m[1][1] * m[2][0] * m[3][2] - m[0][1] * m[1][3] * m[2][0] * m[3][2] -
    m[0][3] * m[1][0] * m[2][1] * m[3][2] + m[0][0] * m[1][3] * m[2][1] * m[3][2] +
    m[0][1] * m[1][0] * m[2][3] * m[3][2] - m[0][0] * m[1][1] * m[2][3] * m[3][2] -
    m[0][2] * m[1][1] * m[2][0] * m[3][3] + m[0][1] * m[1][2] * m[2][0] * m[3][3] +
    m[0][2] * m[1][0] * m[2][1] * m[3][3] - m[0][0] * m[1][2] * m[2][1] * m[3][3] -
    m[0][1] * m[1][0] * m[2][2] * m[3][3] + m[0][0] * m[1][1] * m[2][2] * m[3][3];
}

bool Matrix4x4::IsIdentity() const {
    return (m_Matrix == Matrix4x4::identity);
}