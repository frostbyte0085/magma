#pragma once

#include <Framework.h>

namespace Magma {
	class Vector2;
	class Vector3;
	class Vector4;
	class Quaternion;
	class Rect;
	class Ray;
	class Plane;
	class Matrix4x4;
	class Color;
	class LinearColor;

	static float sPI() {
		return 3.14159265358979f;
	}

	static float sPISquared() {
		return sPI() * sPI();
	}

	static float sPIOver2() {
		return sPI() / 2.0f;
	}

	static float sPIOver4() {
		return sPI() / 4.0f;
	}

	static float sPI2() {
		return sPI() * 2.0f;
	}

	static float sRad2Deg(float rad) {
		return (180.0f / sPI()) * rad;
	}

	static float sDeg2Rad(float deg) {
		return (sPI() / 180.0f) * deg;
	}

	static float sOneOverPI() {
		return 1.0f / sPI();
	}

	static float sOneOver2PI() {
		return 1.0f / sPI2();
	}

	class CoreMath : public NonCopyable {
	public:		
		//
		// functions
		//

		// checks if the absolute difference between f1 and f2 is less than a very small number
		static bool IsNearlyEqual(float f1, float f2);

		// checks if the value is nearly 0
		static bool IsNearlyZero(float f);

		template<typename T> static bool IsWithinRange(T min, T max, T value) {
			return ((value >= min) && (value <= max));
		}

		// returns the next power of two
		static int NextPowerOfTwo(int value);

		// returns 1d perlin noise
		static float PerlinNoise1D(float x);

		// returns 2d perlin noise
		static float PerlinNoise2D(float x, float y);

		// trig
		static void Sincos(float rads, float *sin, float *cos);
		static float Sin(float rads);
		static float Cos(float rads);
		static float Tan(float rads);
		static float Sinh(float rads);
		static float Cosh(float rads);
		static float Tanh(float rads);
		static float Atan(float x);
		static float Atan2(float y, float x);
		static float Acos(float x);
		static float Asin(float x);
		//

		// returns the absolute value
		template<typename T> static T Abs(T value) {

			if (value < T(0))
				value *= T(-1);

			return value;
		}

		// returns a value between 0 and 1, of the input smoothed between min and max
		static float Smoothstep(float min, float max, float value);

		// linearly interpolates between from and to based on an interpolation factor.
		static float Lerp(float from, float to, float factor);

		// returns the interpolation factor for a value between from and to
		static float InverseLerp(float from, float to, float value);

		// maps a value which lies within the source range to the destination range
		static float RemapRange(const Vector2& source, const Vector2& destination, float value);

		// clamps the value between min and max
		template<typename T> static T Clamp(T value, T min, T max) {
			value = PosNeg(value - min, value, min);
			return PosNeg(value - max, max, value);
		}

		// returns forPositiveOrZero if value is greater or equal than 0, otherwise returns forNegative
		template<typename T> static T PosNeg(T value, T forPositiveOrZero, T forNegative) {
			return value >= T(0) ? forPositiveOrZero : forNegative;
		}

		// clamps the value between 0 and 1
		template<typename T> static T Clamp01(T value) {

			return Clamp<T>(value, T(0), T(1));
		}

		// clamps and angle between min and max
		static float ClampAngle(float min, float max, float angle);

		// returns the smallest of the two values
		template<typename T> static T Min(T value1, T value2) {

			return value1 < value2 ? value1 : value2;
		}

		// returns the largest of the two values
		template<typename T> static T Max(T value1, T value2) {

			return value2 < value1 ? value2 : value1;
		}

		// returns the smallest of the three values
		template<typename T> static T Min3(T value1, T value2, T value3) {
			return Min(value1, Min(value2, value3));
		}

		// returns the largest of the three values
		template<typename T> static T Max3(T value1, T value2, T value3) {
			return Max(value1, Max(value2, value3));
		}

		// returns the power of base raised to exponent
		static float Pow(float base, float exponent);

		// returns the square of the value
		static float Square(float value);

		// returns the cube of the value
		static float Cube(float value);

		static float Log(float value);

		// calculates the base-2 log of the input value
		static float Log2(float value);

		// calculates the base-10 log of the input value
		static float Log10(float value);

		// calculates the base-BASE log of the input value
		static float LogBase(float value, float base);

		// calculates the e^value
		static float Exp(float value);

		// calculates the 2^value
		static float Exp2(float value);

		// returns the rounded value
		static float Round(float value);

		// returns the rounded value to the previous float
		static float Floor(float value);

		// returns the rounded value to the next float
		static float Ceil(float value);

		// returns the integral part of input value
		static int Trunc(float value);

		// returns the fractional part of the input value
		static float Fract(float value);

		// returns 0 if value is 0, -1 if negative, 1 if positive
		template<typename T> static int Sign(T value) {

			return (T(0) < value) - (value < T(0));
		}

		// returns the square root of the value.
		static float Sqrt(float value);

		static float Rsqrt(float value);

		template <typename T> static T AlignUpWithMask(T value, size_t mask)
		{
			return (T)(((size_t)value + mask) & ~mask);
		}

		template <typename T> static T AlignDownWithMask(T value, size_t mask)
		{
			return (T)((size_t)value & ~mask);
		}

		template <typename T> static T AlignUp(T value, size_t alignment)
		{
			return AlignUpWithMask(value, alignment - 1);
		}

		template <typename T> static T AlignDown(T value, size_t alignment)
		{
			return AlignDownWithMask(value, alignment - 1);
		}

		template <typename T> static bool IsAligned(T value, size_t alignment)
		{
			return 0 == ((size_t)value & (alignment - 1));
		}

		template <typename T> static T DivideByMultiple(T value, size_t alignment)
		{
			return (T)((value + alignment - 1) / alignment);
		}

		template <typename T> static bool IsPowerOfTwo(T value)
		{
			return 0 == (value & (value - 1));
		}

		template <typename T> static bool IsDivisible(T value, T divisor)
		{
			return (value / divisor) * divisor == value;
		}
	};
}
