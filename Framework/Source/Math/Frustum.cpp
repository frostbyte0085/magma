#include "Frustum.hpp"

using namespace Magma;

Frustum::Frustum() {

}

Frustum::Frustum(const Plane* inPlanes) {
	for (uint32_t i = 0; i < 6; i++) {
		m_Planes[i] = inPlanes[i];
	}
}

Frustum::~Frustum() {

}

void Frustum::Calculate(const Matrix4x4& inMV, const Matrix4x4& inProjection) {

	Matrix4x4 mvp = inProjection * inMV;
	mvp.Transpose();

	const std::vector<float>& data = mvp.GetMatrix();

	SetPlane(EFrustumPlane::PLANE_RIGHT, Plane(data[3] - data[0], data[7] - data[4], data[11] - data[8], data[15] - data[12]) );
	SetPlane(EFrustumPlane::PLANE_LEFT, Plane(data[3] + data[0], data[7] + data[4], data[11] + data[8], data[15] + data[12]) );
	SetPlane(EFrustumPlane::PLANE_BOTTOM, Plane(data[3] + data[1], data[7] + data[5], data[11] + data[9], data[15] + data[13]) );
	SetPlane(EFrustumPlane::PLANE_TOP, Plane(data[3] - data[1], data[7] - data[5], data[11] - data[9], data[15] - data[13]) );
	SetPlane(EFrustumPlane::PLANE_FAR, Plane(data[3] - data[2], data[7] - data[6], data[11] - data[10], data[15] - data[14]) );
	SetPlane(EFrustumPlane::PLANE_NEAR, Plane(data[3] + data[2], data[7] + data[6], data[11] + data[10], data[15] + data[14]) );	
}