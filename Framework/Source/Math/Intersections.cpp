//
//  Intersections.cpp
//  MathLib
//
//  Created by Alkis Lekakis on 10/07/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#include "Intersections.h"
#include "Vector3.h"
#include "Sphere.hpp"
#include "Frustum.hpp"
#include "Plane.h"
#include "Line.h"

namespace Magma {
	bool gIntersects(const Sphere& inSphere, const Frustum& inFrustum, EIntersection* outIntersection) {
		float d = 0.0f;

		// calculate our distances to each of the planes
		for (uint32_t i = 0; i < 6; ++i) {

			// find the distance to this plane
			const Plane& p = inFrustum.GetPlane(i);
			d = Vector3::Dot(p.GetNormal(), inSphere.GetCenter()) + p.GetDistance();

			// if this distance is < -sphere.radius, we are outside
			if (d < -inSphere.GetRadius()) {
				if (outIntersection) *outIntersection = EIntersection::OUTSIDE;
				return false;
			}

			// else if the distance is between +- radius, then we intersect
			if (CoreMath::Abs(d) < inSphere.GetRadius()) {
				if (outIntersection) *outIntersection = EIntersection::INTERSECT;
				return true;
			}
		}

		// otherwise we are fully in view
		if (outIntersection) *outIntersection = EIntersection::INSIDE;
		return true;
	}

	bool gIntersects(const Sphere& inSphere1, const Sphere& inSphere2) {
		
		Vector3 center_diff = inSphere1.GetCenter() - inSphere2.GetCenter();		
		float r = inSphere1.GetRadius() + inSphere2.GetRadius();

		// intersection if the product of their radii sums is larger than the sqr length of their centers' difference.
		return center_diff.GetLengthSqr() < (r * r);
	}
}