//
//  Range.h
//  MathLib
//
//  Created by Alkis Lekakis on 21/05/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#ifndef __MathLib__Range__
#define __MathLib__Range__

#include "MathLib.h"

namespace Magma {
	template<class T>
	class Range {
	public:
		Range() : from(), to() {}
		Range(const T& from, const T& to) {
			this->from = from;
			this->to = to;
		}

		T from;
		T to;

		bool Contains(const T& value) {
			return ((value >= from) && (value <= to));
		}

		void Set(const T& from, const T& to) {
			this->from = from;
			this->to = to;
		}

		void MinMax(const T& min, const T& max) {
			Set(CoreMath::Min(from, min), CoreMath::Max(to, max));
		}

		void Min(const T& min) {
			Set(CoreMath::Min(from, min), to);
		}

		void Max(const T& max) {
			Set(from, CoreMath::Max(to, max));
		}
	};
}

#endif /* defined(__MathLib__Range__) */
