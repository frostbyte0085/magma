#include "Math/MathLib.h"
#include "Math/Vector2.h"
#include "Math/Vector3.h"
#include "Math/Vector4.h"
#include "Math/Color.h"

using namespace Magma;

//
//
// math functions
//

void CoreMath::Sincos(float rads, float *sin, float *cos) {
    *sin = Sin(rads);
    *cos = Cos(rads);
}

float CoreMath::Sin(float rads) {
    return sinf(rads);
}

float CoreMath::Cos(float rads) {
    return cosf(rads);
}

float CoreMath::Tan(float rads) {
    return Sin(rads) / Cos(rads);
}

float CoreMath::Atan(float rads) {
    return atanf(rads);
}

float CoreMath::Atan2(float y, float x) {
    return atan2f(y, x);
}

float CoreMath::Acos(float x) {
    return acosf(x);
}

float CoreMath::Asin(float x) {
    return asinf(x);
}

float CoreMath::Sinh (float rads) {
    return sinhf(rads);
}

float CoreMath::Cosh(float rads) {
    return coshf(rads);
}

float CoreMath::Tanh(float rads) {
    return tanhf(rads);
}

bool CoreMath::IsNearlyEqual(float f1, float f2) {
    return Abs(f1-f2) < std::numeric_limits<float>::epsilon();
}

bool CoreMath::IsNearlyZero(float f) {
    return IsNearlyEqual(f, 0.0f);
}

int CoreMath::NextPowerOfTwo(int value) {
    if (value <= 1)
        return value;
    
    double d = value-1;
    return 1 << ((((int*)&d)[1]>>20)-1022); 
}

float CoreMath::Rsqrt(float value) {
    return Pow(value, -0.5f);
}

float CoreMath::Sqrt(float value) {
    return sqrtf(value);
}

float CoreMath::Exp (float value) {
    return expf (value);
}

float CoreMath::Exp2(float value) {
    return exp2f(value);
}

float CoreMath::Smoothstep(float min, float max, float value) {
    float x = Clamp01( (value - min)/(max - min) );
    
    return x * x * (3.0f - 2.0f * x);
}

float CoreMath::Lerp (float from, float to, float factor) {
    
    return Clamp(from * (1.0f - factor) + to * factor, from, to);
}

float CoreMath::InverseLerp (float from, float to, float value) {
    value = Clamp(value, from, to);
    return 1.0f - (value - to) / (from - to);
}

float CoreMath::ClampAngle(float min, float max, float angle) {
    angle = fmodf(angle, max);
    if (angle < min) {
        angle += max;
    }
    return angle;
}

float CoreMath::RemapRange(const Vector2 &source, const Vector2 &destination, float value) {
    float x = InverseLerp(source[0], source[1], value);
    return Lerp(destination[0], destination[1], x);
}

float CoreMath::PerlinNoise1D(float x) {
    return 0.0f;
}

float CoreMath::PerlinNoise2D(float x, float y) {
    return 0.0f;
}

float CoreMath::Square(float value) {
    return value * value;
}

float CoreMath::Cube(float value) {
    return value * value * value;
}

float CoreMath::Log2(float value) {
    return Log(value) / Log(2.0f);
}

float CoreMath::Log10(float value) {
    return Log(value) / Log(10.0f);
}

float CoreMath::LogBase(float value, float base) {
    return Log(value) / Log(base);
}

float CoreMath::Pow(float base, float exponent) {
    float s = log(base) * exponent;
    return exp (s);
}

float CoreMath::Log(float value) {
    return logf(value);
}

float CoreMath::Round(float value) {
    return Floor(value + 0.5f);
}

float CoreMath::Floor(float value) {
    return floor(value);
}

float CoreMath::Ceil(float value) {
    return ceil(value);
}

int CoreMath::Trunc(float value) {
    return static_cast<int>((value < 0) ? -Floor(-value) : Floor(value));
}

float CoreMath::Fract(float value) {
    return value - Floor(value);
}