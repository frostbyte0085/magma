//
//  Plane.cpp
//  MathLib
//
//  Created by Alkis Lekakis on 14/05/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#include "Math/Plane.h"

using namespace Magma;

Plane::Plane() {
	plane = fromPointAndNormal(Vector3::zero, Vector3::up);
}

Plane::Plane (const Vector3& p, const Vector3& n) {
    plane = fromPointAndNormal(p, n);
}

Plane::Plane (const Vector3& n, float d) {
    plane[0] = n[0];
    plane[1] = n[1];
    plane[2] = n[2];
    plane[3] = d;
}

Plane::Plane (const Vector3& p1, const Vector3& p2, const Vector3& p3) {
    Vector3 n = Vector3::Cross(p2-p1, p3-p1);
    plane = fromPointAndNormal(p1, n);
}

Plane::Plane (float a, float b, float c, float d) {
    plane = Vector4(a,b,c,d);
}

Plane::Plane (const Plane& other) {
    plane = Vector4(other.GetEquation());
}

Plane::~Plane() {
    
}

float Plane::GetDistance() const {
    return plane[3];
}

Vector3 Plane::GetNormal() const {
    return Vector3(plane[0], plane[1], plane[2]);
}

void Plane::Normalize() {
    plane.Normalize();
}

Plane Plane::GetNormalized() const {
    Plane p = *this;
    p.Normalize();
    return p;
}

Vector4 Plane::fromPointAndNormal(const Vector3 &p, const Vector3 &n) const {
    return Vector4(n[0], n[1], n[2], -Vector3::Dot(p, n));
}

float Plane::GetDistanceToPoint(const Vector3 &p) const {
    return plane[0] * p[0] + plane[1] * p[1] * plane[2] * p[2] + plane[3];
}

float Plane::Dot(const Plane &plane, const Vector4 &v) {
    return Vector4::Dot(plane.GetEquation(), v);
}

float Plane::Dot(const Plane &plane, const Vector3 &v) {
    return Vector4::Dot(plane.GetEquation(), Vector4(v, 1.0f));
}

float Plane::DotNormal(const Plane &plane, const Vector3 &n) {
    return Vector4::Dot(plane.GetEquation(), Vector4(n, 0.0f));
}

PlaneSide Plane::GetPointSide(const Vector3 &p) const {
    float d = GetDistanceToPoint(p);
    if (d > 0.0f) {
        return PlaneSide::Positive;
    }
    
    return PlaneSide::Negative;
}

bool Plane::PointsSameSide(const Vector3 &p1, const Vector3 &p2) const {
    PlaneSide side1 = GetPointSide(p1);
    PlaneSide side2 = GetPointSide(p2);
    
    return side1 == side2;
}

void Plane::Flip() {
    
}

Plane Plane::GetFlipped() const {
    Plane p = *this;
    p.Flip();
    return p;
}