#pragma once

#include "MathLib.h"
#include "Quaternion.h"

namespace Magma {
	class Vector3 {
	public:
		Vector3();
		Vector3(float x, float y, float z);
		Vector3(const float* xyz);
		Vector3(const Vector2& v, float z);
		Vector3(const Vector4& v);
		Vector3(const Vector3& orig);
		~Vector3();

		// functions
		float GetLength() const;
		float GetLengthSqr() const;
		Vector3 GetNormalized() const;
		void Normalize();
		void MakeAbsolute();
		Vector3 GetAbsolute() const;

		// static members
		static Vector3 up;
		static Vector3 down;
		static Vector3 zero;
		static Vector3 one;
		static Vector3 right;
		static Vector3 left;
		static Vector3 forward;
		static Vector3 back;


		// static functions
		static Vector3 Max(const Vector3 &v1, const Vector3 &v2);
		static Vector3 Max3(const Vector3 &v1, const Vector3 &v2, const Vector3 &v3);
		static Vector3 Min(const Vector3 &v1, const Vector3 &v2);
		static Vector3 Min3(const Vector3 &v1, const Vector3 &v2, const Vector3 &v3);

		static float Distance(const Vector3 &v1, const Vector3& v2);
		static float DistanceSqr(const Vector3 &v1, const Vector3 &v2);
		static float Dot(const Vector3& v1, const Vector3 &v2);
		static Vector3 Cross(const Vector3 &v1, const Vector3 &v2);
		static Vector3 Lerp(const Vector3 &v1, const Vector3& v2, float factor);
		static void OrthoNormalize(Vector3 *v1, Vector3 *v2);

		// operators
		Vector3& operator - () {
			for (uint32_t i = 0; i < 3; i++) {
				Components[i] *= -1.0f;
			}

			return *this;
		}

		Vector3& operator += (const Vector3& rhs) {
			for (uint32_t i = 0; i < 3; i++) {
				Components[i] += rhs[i];
			}

			return *this;
		}

		Vector3& operator -= (const Vector3& rhs) {
			for (uint32_t i = 0; i < 3; i++) {
				Components[i] -= rhs[i];
			}

			return *this;
		}

		Vector3& operator *= (const Vector3& rhs) {
			for (uint32_t i = 0; i < 3; i++) {
				Components[i] *= rhs[i];
			}

			return *this;
		}

		Vector3& operator /= (const Vector3& rhs) {
			for (uint32_t i = 0; i < 3; i++) {
				Components[i] /= rhs[i];
			}

			return *this;
		}

		Vector3& operator *= (float v) {
			for (uint32_t i = 0; i < 3; i++) {
				Components[i] *= v;
			}

			return *this;
		}

		Vector3& operator /= (float v) {
			for (uint32_t i = 0; i < 3; i++) {
				Components[i] /= v;
			}

			return *this;
		}

		Vector3& operator *= (const Quaternion& rhs) {
			float num = rhs[0] * 2.0f;
			float num2 = rhs[1] * 2.0f;
			float num3 = rhs[2] * 2.0f;
			float num4 = rhs[0] * num;
			float num5 = rhs[1] * num2;
			float num6 = rhs[2] * num3;
			float num7 = rhs[0] * num2;
			float num8 = rhs[0] * num3;
			float num9 = rhs[1] * num3;
			float num10 = rhs[3] * num;
			float num11 = rhs[3] * num2;
			float num12 = rhs[3] * num3;

			Components[0] = (1.0f - (num5 + num6)) * Components[0] + (num7 - num12) * Components[1] + (num8 + num11) * Components[2];
			Components[1] = (num7 + num12) * Components[0] + (1.0f - (num4 + num6)) * Components[1] + (num9 - num10) * Components[2];
			Components[2] = (num8 - num11) * Components[0] + (num9 + num10) * Components[1] + (1.0f - (num4 + num5)) * Components[2];

			return *this;
		}

		Vector3& operator *= (const Matrix4x4& rhs);

		Vector3& operator = (const Vector3& other) {
			if (&other != this) {
				memcpy(Components, other.Components, sizeof(Components));
			}

			return *this;
		}	

		float& operator[] (size_t idx) { return Components[idx]; }
		const float& operator[] (size_t idx) const { return Components[idx]; }

		float Components[3];
	};

	inline static Vector3 operator + (Vector3 lhs, const Vector3& rhs) {
		lhs += rhs;
		return lhs;
	}

	inline static Vector3 operator - (Vector3 lhs, const Vector3& rhs) {
		lhs -= rhs;
		return lhs;
	}

	inline static Vector3 operator * (Vector3 lhs, const Vector3& rhs) {
		lhs *= rhs;
		return lhs;
	}

	inline static Vector3 operator / (Vector3 lhs, const Vector3& rhs) {
		lhs /= rhs;
		return lhs;
	}

	inline static Vector3 operator * (Vector3 lhs, float v) {
		lhs *= v;
		return lhs;
	}

	inline static Vector3 operator / (Vector3 lhs, float v) {
		lhs /= v;
		return lhs;
	}

	inline static Vector3 operator * (Vector3 lhs, const Quaternion& rhs) {
		lhs *= rhs;
		return lhs;
	}

	inline static Vector3 operator * (Vector3 lhs, const Matrix4x4& rhs) {
		lhs *= rhs;
		return lhs;
	}

	static bool operator == (const Vector3& lhs, const Vector3& rhs) {
		bool x = CoreMath::IsNearlyEqual(lhs[0], rhs[0]);
		bool y = CoreMath::IsNearlyEqual(lhs[1], rhs[1]);
		bool z = CoreMath::IsNearlyEqual(lhs[2], rhs[2]);

		return (x && y && z);
	}

	static bool operator != (const Vector3& lhs, const Vector3& rhs) {
		return !(lhs == rhs);
	}
}

MAKE_HASHABLE(Magma::Vector3, t.Components[0], t.Components[1], t.Components[2])