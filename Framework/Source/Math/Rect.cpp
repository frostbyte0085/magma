#include "Math/Rect.h"

using namespace Magma;

Rect::Rect() {
    SetRect(0.0f, 0.0f, 0.0f, 0.0f);
}

Rect::Rect (const Rect &other) {
    SetRect(other.m_X, other.m_Y, other.m_Width, other.m_Height);
}

Rect::Rect(float x, float y, float width, float height) {
    SetRect(x,y,width,height);
}

Rect::~Rect() {
    
}

void Rect::SetRect(float x, float y, float width, float height) {
    m_X = x;
    m_Y = y;
    m_Width = width;
    m_Height = height;
    
    calculateCenter();
}

bool Rect::Contains(const Rect &other) {
    
    return ( (m_X > other.m_X) && ((m_X+m_Width)<(other.m_X+other.m_Height)) &&
            (m_Y > other.m_Y) && ((m_Y+m_Height)<(other.m_Y+other.m_Height)) );
}

void Rect::Expand(float x, float y) {
    m_Width *= x;
    m_Height *= y;
    
    calculateCenter();
}

void Rect::calculateCenter() {
    m_Center = Vector2( (m_X + m_Width)*0.5f, (m_Y + m_Height)*0.5f );
}