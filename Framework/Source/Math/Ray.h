#pragma once

#include "Vector3.h"

namespace Magma {
	class Ray {
	public:
		Ray(const Vector3 &origin, const Vector3 &direction);
		Ray(const Ray &other);
		Ray();
		~Ray();

		Vector3 origin;
		Vector3 direction;

	private:
	};
}
