#include "Vector2.h"
#include "Vector3.h"
#include "Vector4.h"

using namespace Magma;

// static members
Vector2 Vector2::one (1,1);
Vector2 Vector2::zero (0,0);
Vector2 Vector2::right (1,0);
Vector2 Vector2::up (0,1);
Vector2 Vector2::down (0,-1);
Vector2 Vector2::left (-1,0);


// static functions
Vector2 Vector2::Max(const Vector2 &v1, const Vector2 &v2) {
    float maxX = CoreMath::Max(v1[0], v2[0]);
    float maxY = CoreMath::Max(v1[1], v2[1]);
    
    return Vector2(maxX, maxY);
}

Vector2 Vector2::Min(const Vector2 &v1, const Vector2 &v2) {
    float minX = CoreMath::Min(v1[0], v2[0]);
    float minY = CoreMath::Min(v1[1], v2[1]);
    
    return Vector2(minX, minY);
}

Vector2 Vector2::Max3(const Vector2 &v1, const Vector2 &v2, const Vector2 &v3) {
    float maxX = CoreMath::Max3(v1[0], v2[0], v3[0]);
    float maxY = CoreMath::Max3(v1[1], v2[1], v3[1]);
    
    return Vector2(maxX, maxY);
}

Vector2 Vector2::Min3(const Vector2 &v1, const Vector2 &v2, const Vector2 &v3) {
    float minX = CoreMath::Min3(v1[0], v2[0], v3[0]);
    float minY = CoreMath::Min3(v1[1], v2[1], v3[1]);
    
    return Vector2(minX, minY);
}

float Vector2::Distance(const Vector2 &v1, const Vector2 &v2) {
    Vector2 v = (v2 - v1);
    return v.GetLength();
}

float Vector2::DistanceSqr(const Vector2 &v1, const Vector2 &v2) {
    Vector2 v = (v2 - v1);
    return v.GetLengthSqr();
}

float Vector2::Dot(const Vector2 &v1, const Vector2 &v2) {
    return (v1[0] * v2[0] + v1[1] * v2[1]);
}

Vector2 Vector2::Lerp(const Vector2 &v1, const Vector2 &v2, float factor) {
    
    factor = CoreMath::Clamp01(factor);
    
    float xx = CoreMath::Lerp(v1[0], v2[0], factor);
    float yy = CoreMath::Lerp(v1[1], v2[1], factor);
    
    return Vector2(xx,yy);
}

// functions
Vector2::Vector2() {
	ZeroMemory(&Components, sizeof(float) * 2);
}

Vector2::Vector2(float x, float y) {
    Components[0] = x;
    Components[1] = y;
}

Vector2::Vector2(const Vector2& orig) {
    Components[0] = orig[0];
    Components[1] = orig[1];
}

Vector2::Vector2(const Vector3& orig) {
	Components[0] = orig[0];
	Components[1] = orig[1];
}

Vector2::Vector2(const Vector4& orig) {
	Components[0] = orig[0];
	Components[1] = orig[1];
}

Vector2::Vector2(const float* xy) {

}

Vector2::~Vector2() {
}

float Vector2::GetLength() const {
    return CoreMath::Sqrt( GetLengthSqr() );
}

float Vector2::GetLengthSqr() const {
    return Components[0]*Components[0] + Components[1]*Components[1];
}

void Vector2::Normalize() {
    float len = GetLength();
    if (CoreMath::IsNearlyZero(len)) {
		ZeroMemory(&Components, sizeof(float) * 2);
    }
    else {
		for (uint32_t i = 0; i < 2; i++) {
			Components[i] /= len;
		}
    }
}

Vector2 Vector2::GetNormalized() const {

    Vector2 v = *this;
    v.Normalize();
    return v;
}

void Vector2::MakeAbsolute() {
	for (uint32_t i = 0; i < 2; i++) {
		Components[i] = CoreMath::Abs(Components[i]);
	}    
}

Vector2 Vector2::GetAbsolute() const {
    Vector2 v = *this;
    v.MakeAbsolute();
    return v;
}
