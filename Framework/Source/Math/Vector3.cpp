#include "Vector4.h"
#include "Vector3.h"
#include "Vector2.h"
#include "Matrix4x4.h"

using namespace Magma;

// static members
Vector3 Vector3::one (1,1,0);
Vector3 Vector3::zero (0,0,0);
Vector3 Vector3::right (1,0,0);
Vector3 Vector3::up (0,1,0);
Vector3 Vector3::down (0,-1,0);
Vector3 Vector3::left (-1,0,0);
Vector3 Vector3::forward(0,0,1);
Vector3 Vector3::back(0,0,-1);

// static functions
Vector3 Vector3::Max(const Vector3 &v1, const Vector3 &v2) {
    float maxX = CoreMath::Max(v1[0], v2[0]);
    float maxY = CoreMath::Max(v1[1], v2[1]);
    float maxZ = CoreMath::Max(v1[2], v2[2]);
    
    return Vector3(maxX, maxY, maxZ);
}

Vector3 Vector3::Min(const Vector3 &v1, const Vector3 &v2) {
    float minX = CoreMath::Min(v1[0], v2[0]);
    float minY = CoreMath::Min(v1[1], v2[1]);
    float minZ = CoreMath::Min(v1[2], v2[2]);
    
    return Vector3(minX, minY, minZ);
}

Vector3 Vector3::Max3(const Vector3 &v1, const Vector3 &v2, const Vector3 &v3) {
    float maxX = CoreMath::Max3(v1[0], v2[0], v3[0]);
    float maxY = CoreMath::Max3(v1[1], v2[1], v3[1]);
    float maxZ = CoreMath::Max3(v1[2], v2[2], v3[2]);
    
    return Vector3(maxX, maxY, maxZ);
}

Vector3 Vector3::Min3(const Vector3 &v1, const Vector3 &v2, const Vector3 &v3) {
    float minX = CoreMath::Min3(v1[0], v2[0], v3[0]);
    float minY = CoreMath::Min3(v1[1], v2[1], v3[1]);
    float minZ = CoreMath::Min3(v1[2], v2[2], v3[2]);
    
    return Vector3(minX, minY, minZ);
}

float Vector3::Distance(const Vector3 &v1, const Vector3 &v2) {
    Vector3 v = (v2 - v1);
    return v.GetLength();
}

float Vector3::DistanceSqr(const Vector3 &v1, const Vector3 &v2) {
    Vector3 v = (v2 - v1);
    return v.GetLengthSqr();
}

float Vector3::Dot(const Vector3 &v1, const Vector3 &v2) {
    return (v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]);
}

void Vector3::OrthoNormalize(Vector3 *v1, Vector3 *v2) {
    assert (v1 || v2);

    v1->Normalize();
    float d = Dot(*v1, *v2);
  
    Vector3 p = (*v1) * d;
    
    (*v2) -= p;
    v2->Normalize();
}

Vector3 Vector3::Lerp(const Vector3 &v1, const Vector3 &v2, float factor) {
    
    factor = CoreMath::Clamp01(factor);
    
    float xx = CoreMath::Lerp(v1[0], v2[0], factor);
    float yy = CoreMath::Lerp(v1[1], v2[1], factor);
    float zz = CoreMath::Lerp(v1[2], v2[2], factor);
    
    return Vector3(xx,yy, zz);
}

Vector3 Vector3::Cross(const Vector3 &v1, const Vector3 &v2) {
    float xx = v1[1] * v2[2] - v1[2] * v2[1];
    float yy = v1[2] * v2[0] - v1[0] * v2[2];
    float zz = v1[0] * v2[1] - v1[1] * v2[2];
    
    return Vector3(xx, yy, zz);
}

// functions
Vector3::Vector3() {
	ZeroMemory(&Components, sizeof(float) * 3);
}

Vector3::Vector3(float x, float y, float z) {
    Components[0] = x;
    Components[1] = y;
    Components[2] = z;
}

Vector3::Vector3(const Vector2& v, float z) {
    Components[0] = v[0];
    Components[1] = v[1];
    Components[2] = z;
}

Vector3::Vector3(const Vector4& v) {
	for (uint32_t i = 0; i < 3; i++) {
		Components[i] = v[i];
	}
}

Vector3::Vector3(const Vector3& orig) {
	for (uint32_t i = 0; i < 3; i++) {
		Components[i] = orig[i];
	}
}

Vector3::Vector3(const float* xyz) {
	for (uint32_t i = 0; i < 3; i++) {
		Components[i] = xyz[i];
	}
}

Vector3::~Vector3() {
}

float Vector3::GetLength() const {
    return CoreMath::Sqrt( GetLengthSqr() );
}

float Vector3::GetLengthSqr() const {
    return Components[0]*Components[0] + Components[1]*Components[1] + Components[2]*Components[2];
}

void Vector3::Normalize() {
    float len = GetLength();
    if (CoreMath::IsNearlyZero(len)) {
		ZeroMemory(&Components, sizeof(float) * 3);
    }
    else {
		for (uint32_t i = 0; i < 3; i++) {
			Components[i] /= len;
		}
    }
}

Vector3 Vector3::GetNormalized() const {
    
    Vector3 v = *this;
    v.Normalize();
    return v;
}

void Vector3::MakeAbsolute() {
	for (uint32_t i = 0; i < 3; i++) {
		Components[i] = CoreMath::Abs(Components[i]);
	}    
}

Vector3 Vector3::GetAbsolute() const {
    Vector3 v = *this;
    v.MakeAbsolute();
    return v;
}

Vector3& Vector3::operator *= (const Matrix4x4& rhs) {
	float x = Vector4::Dot(rhs.GetColumn(0), Vector4(Components[0], Components[1], Components[2], 1.0f));
	float y = Vector4::Dot(rhs.GetColumn(1), Vector4(Components[0], Components[1], Components[2], 1.0f));
	float z = Vector4::Dot(rhs.GetColumn(2), Vector4(Components[0], Components[1], Components[2], 1.0f));

	Components[0] = x;
	Components[1] = y;
	Components[2] = z;

	return *this;
}