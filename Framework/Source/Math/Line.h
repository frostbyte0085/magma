//
//  Line.h
//  MathLib
//
//  Created by Alkis Lekakis on 10/07/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#pragma once


#include "Vector3.h"

namespace Magma {
	class Line {
	public:
		Line(const Vector3& from, const Vector3& to);
		Line();
		~Line();

		Vector3 from;
		Vector3 to;
	};
}