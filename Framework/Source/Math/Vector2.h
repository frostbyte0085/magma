#pragma once

#include "MathLib.h"

namespace Magma {
	class Vector2 {
	public:
		Vector2();
		Vector2(float x, float y);
		Vector2(const float* xy);
		Vector2(const Vector3& v);
		Vector2(const Vector4& v);
		Vector2(const Vector2& orig);
		~Vector2();

		// functions
		float GetLength() const;
		float GetLengthSqr() const;
		Vector2 GetNormalized() const;
		void Normalize();
		void MakeAbsolute();
		Vector2 GetAbsolute() const;

		// static members
		static Vector2 up;
		static Vector2 down;
		static Vector2 zero;
		static Vector2 one;
		static Vector2 right;
		static Vector2 left;

		// static functions
		static Vector2 Max(const Vector2 &v1, const Vector2 &v2);
		static Vector2 Max3(const Vector2 &v1, const Vector2 &v2, const Vector2 &v3);
		static Vector2 Min(const Vector2 &v1, const Vector2 &v2);
		static Vector2 Min3(const Vector2 &v1, const Vector2 &v2, const Vector2 &v3);

		static float Distance(const Vector2 &v1, const Vector2& v2);
		static float DistanceSqr(const Vector2 &v1, const Vector2 &v2);
		static float Dot(const Vector2& v1, const Vector2 &v2);

		static Vector2 Lerp(const Vector2 &v1, const Vector2& v2, float factor);

		// operators
		Vector2& operator - () {
			Components[0] *= -1.0f;
			Components[1] *= -1.0f;

			return *this;
		}

		Vector2& operator += (const Vector2& rhs) {
			Components[0] += rhs[0];
			Components[1] += rhs[1];

			return *this;
		}

		Vector2& operator -= (const Vector2& rhs) {
			Components[0] -= rhs[0];
			Components[1] -= rhs[1];

			return *this;
		}

		Vector2& operator *= (const Vector2& rhs) {
			Components[0] *= rhs[0];
			Components[1] *= rhs[1];

			return *this;
		}

		Vector2& operator /= (const Vector2& rhs) {
			Components[0] /= rhs[0];
			Components[1] /= rhs[1];

			return *this;
		}

		Vector2& operator *= (float v) {
			Components[0] *= v;
			Components[1] *= v;

			return *this;
		}

		Vector2& operator /= (float v) {
			Components[0] /= v;
			Components[1] /= v;

			return *this;
		}

		Vector2& operator = (const Vector2& other) {
			if (&other != this) {
				memcpy(Components, other.Components, sizeof(Components));
			}

			return *this;
		}

		float& operator[](size_t idx) { return Components[idx]; }
		const float& operator[](size_t idx) const { return Components[idx]; }

		float Components[2];
	};

	inline static Vector2 operator + (Vector2 lhs, const Vector2& rhs) {
		lhs += rhs;
		return lhs;
	}

	inline static Vector2 operator - (Vector2 lhs, const Vector2& rhs) {
		lhs -= rhs;
		return lhs;
	}

	inline static Vector2 operator * (Vector2 lhs, const Vector2& rhs) {
		lhs *= rhs;
		return lhs;
	}

	inline static Vector2 operator / (Vector2 lhs, const Vector2& rhs) {
		lhs /= rhs;
		return lhs;
	}

	inline static Vector2 operator * (Vector2 lhs, float v) {
		lhs *= v;
		return lhs;
	}

	inline static Vector2 operator / (Vector2 lhs, float v) {
		lhs /= v;
		return lhs;
	}

	static bool operator == (const Vector2& lhs, const Vector2& rhs) {
		bool x = CoreMath::IsNearlyEqual(lhs[0], rhs[0]);
		bool y = CoreMath::IsNearlyEqual(lhs[1], rhs[1]);

		return (x && y);
	}

	static bool operator != (const Vector2& lhs, const Vector2& rhs) {
		return !(lhs == rhs);
	}

}

MAKE_HASHABLE(Magma::Vector2, t.Components[0], t.Components[1])