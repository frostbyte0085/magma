#include "Math/Quaternion.h"
#include "Math/Vector3.h"

using namespace Magma;

Quaternion Quaternion::zero(0.0f, 0.0f, 0.0f, 0.0f);
Quaternion Quaternion::identity (0.0f, 0.0f, 0.0f, 1.0f);

float Quaternion::Dot(const Quaternion &q1, const Quaternion &q2) {
    return (q1[0] * q2[0] + q1[1] * q2[1] + q1[2] * q2[2] + q1[3] * q2[3]);
}

Quaternion Quaternion::Slerp(const Quaternion &q1, const Quaternion &q2, float factor) {
    factor = CoreMath::Clamp01(factor);
    float o = acosf (CoreMath::Clamp(Dot(q1, q2), -1.0f, 1.0f));
    
    if (CoreMath::Abs(o) < 1e-10) o = 1e-10;
    
    float denom = CoreMath::Sin(o);
    float st0 = CoreMath::Sin(1-factor) * o / denom;
    float st1 = CoreMath::Sin(factor) * o / denom;
    
    return Quaternion (q1[0]*st0 + q2[0]*st1, q1[1]*st0 + q2[1]*st1, q1[2]*st0 + q2[2]*st1, q1[3]*st0 + q2[3]*st1);
}

Quaternion Quaternion::Lerp(const Quaternion &q1, const Quaternion &q2, float factor) {
    factor = CoreMath::Clamp01<float>(factor);
    
    float xx = CoreMath::Lerp(q1[0], q2[0], factor);
    float yy = CoreMath::Lerp(q1[1], q2[1], factor);
    float zz = CoreMath::Lerp(q1[2], q2[2], factor);
    float ww = CoreMath::Lerp(q1[3], q2[3], factor);
    
    return Quaternion(xx,yy,zz,ww).GetNormalized();
}

Quaternion::Quaternion() {
    Components[0] = 0.0f;
	Components[1] = 0.0f;
	Components[2] = 0.0f;
	Components[3] = 1.0f;
}

Quaternion::Quaternion (float x, float y, float z, float w) {
	Components[0] = x;
	Components[1] = y;
	Components[2] = z;
	Components[3] = w;
}

Quaternion::Quaternion(const Vector3 &lookDir, const Vector3& upDir) {
    Vector3 fwd = lookDir;
    Vector3 up = upDir;
    
    Vector3::OrthoNormalize(&fwd, &up);
    
    Vector3 right = Vector3::Cross(up, fwd);
    
    Components[3] = CoreMath::Sqrt(1.0f + right[0] + up[1] + fwd[2]) * 0.5f;
    float wrecp = 1.0f / (4.0f * Components[3]);
    
    Components[0] = (up[2] - fwd[1]) * wrecp;
    Components[1] = (fwd[0] - right[2]) * wrecp;
    Components[2] = (right[1] - up[0]) * wrecp;
    
    Normalize();
}

Quaternion::Quaternion(float yaw, float pitch, float roll) {
    float half_yaw_rad = sDeg2Rad(yaw * 0.5f);
    float half_pitch_rad = sDeg2Rad(pitch * 0.5f);
    float half_roll_rad = sDeg2Rad(roll * 0.5f);
    
	float Cx, Sx;
	CoreMath::Sincos(half_pitch_rad, &Sx, &Cx);

	float Cy, Sy;
	CoreMath::Sincos(half_yaw_rad, &Sy, &Cy);

	float Cz, Sz;
	CoreMath::Sincos(half_roll_rad, &Sz, &Cz);

	// multiply it out	
	Components[0] = Sx*Cy*Cz + Cx*Sy*Sz;
	Components[1] = Cx*Sy*Cz - Sx*Cy*Sz;
	Components[2] = Cx*Cy*Sz + Sx*Sy*Cx;
	Components[3] = Cx*Cy*Cz - Sx*Sy*Sz;
}

Quaternion::Quaternion (const Quaternion &other) {
	for (uint32_t i = 0; i < 4; i++) {
		Components[i] = other[i];
	}
}

Quaternion::Quaternion(const float* xyzw) {
	for (uint32_t i = 0; i < 4; i++) {
		Components[i] = xyzw[i];
	}
}

Quaternion::~Quaternion() {
    
}

void Quaternion::Inverse() {
    Components[0] *= -1.0f;
    Components[1] *= -1.0f;
    Components[2] *= -1.0f;
    
    Normalize();
}

Quaternion Quaternion::GetInverse() const {
    Quaternion q = *this;
    q.Inverse();
    return q;
}

float Quaternion::GetLengthSqr() const {
    return Components[0] * Components[0] + Components[1] * Components[1] + Components[2] * Components[2] + Components[3] * Components[3];
}

float Quaternion::GetLength() const {
    return CoreMath::Sqrt(GetLengthSqr());
}

void Quaternion::Normalize() {
    float len = GetLength();
    if (CoreMath::IsNearlyZero(len)) {
		Components[0] = 0.0f;
		Components[1] = 0.0f;
		Components[2] = 0.0f;
		Components[3] = 1.0f;
    }
    else {
		for (uint32_t i = 0; i < 4; i++) {
			Components[i] /= len;
		}
    }
}

Quaternion Quaternion::GetNormalized() const {
    Quaternion q = *this;
    q.Normalize();
    return q;
}

