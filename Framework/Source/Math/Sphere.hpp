#pragma once

#include "Vector3.h"

namespace Magma {
	class Matrix4x4;

	class Sphere {
	public:
		Sphere();
		Sphere(const Vector3& inCenter, float inRadius);
		Sphere(const Vector3* inPoints, uint32_t inLength);
		~Sphere();

		inline void SetCenter(const Vector3& inCenter) { m_Center = inCenter; }
		inline const Vector3& GetCenter() const { return m_Center; }

		inline void SetRadius(float inRadius) { m_Radius = inRadius; }
		inline float GetRadius() const { return m_Radius; }

		Sphere GetTransformed(const Matrix4x4& inMatrix) const;
		void Transform(const Matrix4x4& inMatrix);

	private:
		Vector3 m_Center;
		float m_Radius;
	};
}