#pragma once

#include "MathLib.h"
#include "Matrix4x4.h"

namespace Magma {

	class Vector3;

	class Quaternion {
	public:
		Quaternion();
		Quaternion(float yaw, float pitch, float roll);
		Quaternion(const Vector3 &lookDir, const Vector3 &upDir);
		Quaternion(float x, float y, float z, float w);
		Quaternion(const float* xyzw);
		Quaternion(const Quaternion &other);
		~Quaternion();

		// static members
		static Quaternion identity;
		static Quaternion zero;

		// static functions
		static float Dot(const Quaternion& q1, const Quaternion& q2);
		static Quaternion Slerp(const Quaternion &q1, const Quaternion &q2, float factor);
		static Quaternion Lerp(const Quaternion &q1, const Quaternion &q2, float factor);

		// functions
		void Normalize();
		Quaternion GetNormalized() const;

		float GetLength() const;
		float GetLengthSqr() const;

		void Inverse();
		Quaternion GetInverse() const;

		// operators
		Quaternion& operator - () {
			for (uint32_t i = 0; i < 4; i++) {
				Components[i] *= -1.0f;
			}

			return *this;
		}

		Quaternion& operator += (const Quaternion& rhs) {
			for (uint32_t i = 0; i < 4; i++) {
				Components[i] += rhs[i];
			}

			return *this;
		}

		Quaternion& operator -= (const Quaternion& rhs) {
			for (uint32_t i = 0; i < 4; i++) {
				Components[i] -= rhs[i];
			}

			return *this;
		}

		Quaternion& operator *= (const Quaternion& rhs) {			
			Components[0] = (w()*rhs.x() + x()*rhs.w() + y()*rhs.z() - z()*rhs.y());
			Components[1] = (w()*rhs.y() + y()*rhs.w() + z()*rhs.x() - x()*rhs.z());
			Components[2] = (w()*rhs.z() + z()*rhs.w() + x()*rhs.y() - y()*rhs.x());
			Components[3] = (w()*rhs.w() - x()*rhs.x() - y()*rhs.y() - z()*rhs.z());

			return *this;
		}

		Quaternion& operator *= (float v) {
			for (uint32_t i = 0; i < 4; i++) {
				Components[i] *= v;
			}

			return *this;
		}

		Quaternion& operator /= (float v) {
			for (uint32_t i = 0; i < 4; i++) {
				Components[i] /= v;
			}

			return *this;
		}

		inline float x() const { return Components[0]; }
		inline float y() const { return Components[1]; }
		inline float z() const { return Components[2]; }
		inline float w() const { return Components[3]; }

		float& operator[](size_t idx) { return Components[idx]; }
		const float& operator[](size_t idx) const { return Components[idx]; }

		float Components[4];
	};

	inline static Quaternion operator * (Quaternion lhs, float v) {
		lhs *= v;
		return lhs;
	}

	inline static Quaternion operator / (Quaternion lhs, float v) {
		lhs /= v;
		return lhs;
	}

	inline static Quaternion operator + (Quaternion lhs, const Quaternion& rhs) {
		lhs += rhs;
		return lhs;
	}

	inline static Quaternion operator - (Quaternion lhs, const Quaternion& rhs) {
		lhs -= rhs;
		return lhs;
	}

	inline static Quaternion operator * (Quaternion lhs, const Quaternion& rhs) {
		lhs *= rhs;
		return lhs;
	}

	static bool operator == (const Quaternion& lhs, const Quaternion& rhs) {
		bool x = CoreMath::IsNearlyEqual(lhs[0], rhs[0]);
		bool y = CoreMath::IsNearlyEqual(lhs[1], rhs[1]);
		bool z = CoreMath::IsNearlyEqual(lhs[2], rhs[2]);
		bool w = CoreMath::IsNearlyEqual(lhs[3], rhs[3]);

		return (x && y && z && w);
	}

	static bool operator != (const Quaternion& lhs, const Quaternion& rhs) {
		return !(lhs == rhs);
	}
}