//
//  Color.h
//  MathLib
//
//  Created by Alkis Lekakis on 26/05/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#ifndef __MathLib__Color__
#define __MathLib__Color__

#include "MathLib.h"

namespace Magma {
	class Color {
	public:
		Color() {}
		Color(const Color& other);
		Color(float r, float g, float b, float a);
		Color(const float* rgba);
		~Color();

		float GetLuminance() const;
		Color AsLinear() const;

		// static functions
		template<class T>
		static T sLerp(const T& from, const T& to, float factor) {
			factor = CoreMath::Clamp01(factor);

			T c(0, 0, 0, 0);
			for (uint32_t i = 0; i < 4; i++) {
				c[i] = CoreMath::Lerp(from[i], to[i], factor);
			}
			return c;
		}

		Color& operator += (const Color& rhs) {

			for (uint32_t i = 0; i < 4; i++) {
				Components[i] += rhs[i];
			}

			return *this;
		}

		Color& operator -= (const Color& rhs) {
			for (uint32_t i = 0; i < 4; i++) {
				Components[i] -= rhs[i];
			}

			return *this;
		}

		Color& operator *= (const Color& rhs) {
			for (uint32_t i = 0; i < 4; i++) {
				Components[i] *= rhs[i];
			}

			return *this;
		}

		Color& operator /= (const Color& rhs) {
			for (uint32_t i = 0; i < 4; i++) {
				Components[i] /= rhs[i];
			}

			return *this;
		}

		Color& operator *= (float v) {
			for (uint32_t i = 0; i < 4; i++) {
				Components[i] *= v;
			}

			return *this;
		}

		Color& operator /= (float v) {
			for (uint32_t i = 0; i < 4; i++) {
				Components[i] /= v;
			}

			return *this;
		}

		Color& operator = (const Color& other) {
			if (&other != this) {
				memcpy(Components, other.Components, sizeof(Components));
			}

			return *this;
		}

		float& operator[](size_t idx) { return Components[idx]; }
		const float& operator[](size_t idx) const { return Components[idx]; }

		float Components[4];

		float LinearToSRGB(float f) const;
		float SRGBToLinear(float f) const;

		inline float GetGamma() const {
			return 2.2f;
		}

	private:
		float luminance(float r, float g, float b) const;
	};

	inline static Color operator + (Color lhs, const Color& rhs) {
		lhs += rhs;
		return lhs;
	}

	inline static Color operator - (Color lhs, const Color& rhs) {
		lhs -= rhs;
		return lhs;
	}

	inline static Color operator * (Color lhs, const Color& rhs) {
		lhs *= rhs;
		return lhs;
	}

	inline static Color operator / (Color lhs, const Color& rhs) {
		lhs /= rhs;
		return lhs;
	}

	inline static Color operator * (Color lhs, float v) {
		lhs *= v;
		return lhs;
	}

	inline static Color operator / (Color lhs, float v) {
		lhs /= v;
		return lhs;
	}

	static bool operator == (const Color& lhs, const Color& rhs) {
		bool r = CoreMath::IsNearlyEqual(lhs[0], rhs[0]);
		bool g = CoreMath::IsNearlyEqual(lhs[1], rhs[1]);
		bool b = CoreMath::IsNearlyEqual(lhs[2], rhs[2]);
		bool a = CoreMath::IsNearlyEqual(lhs[3], rhs[3]);

		return (r && g && b && a);
	}

	static bool operator != (const Color& lhs, const Color& rhs) {
		return !(lhs == rhs);
	}
}

MAKE_HASHABLE(Magma::Color, t.Components[0], t.Components[1], t.Components[2], t.Components[3])

#endif /* defined(__MathLib__Color__) */
