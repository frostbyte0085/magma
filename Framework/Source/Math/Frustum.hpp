#pragma once

#include "Matrix4x4.h"
#include "Plane.h"

namespace Magma {
	ENUM_FLAGS(EFrustumPlane)
	enum class EFrustumPlane {
		PLANE_TOP = 0x01,
		PLANE_BOTTOM = 0x02,
		PLANE_LEFT = 0x04, 
		PLANE_RIGHT = 0x08,
		PLANE_NEAR = 0x16,
		PLANE_FAR = 0x32,
		PLANE_MAX = 6
	};

	class Frustum {
	public:
		Frustum();
		Frustum(const Plane* inPlanes);
		~Frustum();

		void Calculate(const Matrix4x4& inMV, const Matrix4x4& inProjection);
		inline const Plane& GetPlane(EFrustumPlane inPlaneIndex) const { return m_Planes[(uint32_t)inPlaneIndex]; }
		inline const Plane& GetPlane(uint32_t inPlaneIndex) const { assert(inPlaneIndex >= 0 && inPlaneIndex < (uint32_t)EFrustumPlane::PLANE_MAX); return GetPlane((EFrustumPlane)(1 << inPlaneIndex)); }
		inline void SetPlane(EFrustumPlane inPlaneIndex, const Plane& inPlane) { m_Planes[(uint32_t)inPlaneIndex] = inPlane; }
		inline void SetPlane(uint32_t inPlaneIndex, const Plane& inPlane) { assert(inPlaneIndex >= 0 && inPlaneIndex < (uint32_t)EFrustumPlane::PLANE_MAX); return SetPlane((EFrustumPlane)(1 << inPlaneIndex), inPlane); }

	private:
		Plane m_Planes[6];
	};
}