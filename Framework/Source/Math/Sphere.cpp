#include "Sphere.hpp"
#include "Matrix4x4.h"

using namespace Magma;

Sphere::Sphere() {
	SetCenter(Vector3(0, 0, 0));
	SetRadius(1.0f);
}

Sphere::Sphere(const Vector3& inCenter, float inRadius) {
	SetCenter(inCenter);
	SetRadius(inRadius);
}

Sphere::Sphere(const Vector3* inPoints, uint32_t inLength) {

	Vector3 avg_center;
	float max_distance = 0.0f;
	for (uint32_t i = 0; i < inLength; i++) {
		float dist = inPoints[i].GetLengthSqr();
		if (dist > max_distance) max_distance = dist;

		avg_center += inPoints[i];
	}

	if (inLength > 1) {
		avg_center /= static_cast<float>(inLength);
	}

	SetCenter(avg_center);
	SetRadius(CoreMath::Sqrt(max_distance));
}

Sphere::~Sphere() {

}

Sphere Sphere::GetTransformed(const Matrix4x4& inMatrix) const {
	Sphere s = *this;
	s.Transform(inMatrix);
	return s;
}

void Sphere::Transform(const Matrix4x4& inMatrix) {
	const Vector3& v_scale = inMatrix.GetScale();
	float scale = CoreMath::Max3(v_scale[0], v_scale[1], v_scale[2]);

	SetCenter(GetCenter() * inMatrix);
	SetRadius(GetRadius() * scale);
}