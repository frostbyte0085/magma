#pragma once

namespace Magma {
	class Vector2;
	class Vector3;
	class Vector4;
	class Color;
	class Rect;
	class Ray;
	template<class T> class Range;
	class Quaternion;
	class Matrix;
	class Line;
}