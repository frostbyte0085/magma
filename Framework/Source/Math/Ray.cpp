#include "Math/Ray.h"

using namespace Magma;

Ray::Ray(const Vector3& origin, const Vector3& direction) {
    this->origin = origin;
    this->direction = direction;
}

Ray::Ray(const Ray& other) {
    
    this->origin = other.origin;
    this->direction = other.direction;
}

Ray::Ray() {
    origin = Vector3::zero;
    direction = Vector3::forward;
}

Ray::~Ray() {
    
}