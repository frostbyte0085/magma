//
//  Intersections.h
//  MathLib
//
//  Created by Alkis Lekakis on 10/07/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#pragma once

#include "MathLib.h"

namespace Magma {
	class Sphere;
	class Frustum;
	
	enum class EIntersection {
		INTERSECT,
		INSIDE,
		OUTSIDE
	};

	extern bool gIntersects(const Sphere& inSphere, const Frustum& inFrustum, EIntersection* outIntersection);
	extern bool gIntersects(const Sphere& inSphere1, const Sphere& inSphere2);
}