//
//  Plane.h
//  MathLib
//
//  Created by Alkis Lekakis on 14/05/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#ifndef __MathLib__Plane__
#define __MathLib__Plane__

#include "Vector3.h"
#include "Vector4.h"

namespace Magma {
	enum class PlaneSide {
		Positive,
		Negative
	};

	class Plane {
	public:
		Plane();
		Plane(const Vector3& p, const Vector3& n);
		Plane(const Vector3& p1, const Vector3& p2, const Vector3& p3);
		Plane(const Vector3& n, float d);
		Plane(float a, float b, float c, float d);
		Plane(const Plane& other);
		~Plane();

		// static functions
		static float Dot(const Plane& plane, const Vector3& v);
		static float Dot(const Plane& plane, const Vector4& v);
		static float DotNormal(const Plane& plane, const Vector3& n);

		float GetDistance() const;
		Vector3 GetNormal() const;
		Vector4 GetEquation() const { return plane; }

		void Flip();
		Plane GetFlipped() const;

		void Normalize();
		Plane GetNormalized() const;

		float GetDistanceToPoint(const Vector3& p) const;
		PlaneSide GetPointSide(const Vector3& p) const;
		bool PointsSameSide(const Vector3& p1, const Vector3& p2) const;

	private:
		Vector4 plane;

		Vector4 fromPointAndNormal(const Vector3& p, const Vector3& n) const;
	};

	static bool operator == (const Plane& lhs, const Plane& rhs) {
		return lhs.GetEquation() == rhs.GetEquation();
	}

	static bool operator != (const Plane& lhs, const Plane& rhs) {
		return !(lhs == rhs);
	}
}
#endif /* defined(__MathLib__Plane__) */
