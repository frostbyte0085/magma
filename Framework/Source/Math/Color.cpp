//
//  Color.cpp
//  MathLib
//
//  Created by Alkis Lekakis on 26/05/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#include "Math/Color.h"

using namespace Magma;

Color::Color(const Color& other) {
	for (uint32_t i = 0; i < 4; i++) {
		Components[i] = other[i];
	}
}

Color::Color(float r, float g, float b, float a) {
	Components[0] = r;
	Components[1] = g;
	Components[2] = b;
	Components[3] = a;
}

Color::Color(const float* rgba) {
	for (uint32_t i = 0; i < 4; i++) {
		Components[i] = rgba[i];
	}
}

Color::~Color() {
    
}

float Color::LinearToSRGB(float f) const {
	return CoreMath::Pow(f, 1.0f / GetGamma());
}

float Color::SRGBToLinear(float f) const {
	return CoreMath::Pow(f, GetGamma());
}

float Color::luminance(float r, float g, float b) const {
	return r * 0.212655f + g * 0.715158f + b * 0.072187f;
}

Color Color::AsLinear() const {
    return Color(Components);
}

float Color::GetLuminance() const {
    return luminance(Components[0], Components[1], Components[2]);
}