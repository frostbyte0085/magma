#pragma once

#include "MathLib.h"

namespace Magma {
	class Vector4 {
	public:
		Vector4();
		Vector4(float x);
		Vector4(float x, float y, float z, float w);
		Vector4(const Vector3& v, float w);
		Vector4(const Vector4& orig);
		Vector4(const float* xyzw);
		~Vector4();

		// functions
		float GetLength() const;
		float GetLengthSqr() const;
		Vector4 GetNormalized() const;
		void Normalize();
		void MakeAbsolute();
		Vector4 GetAbsolute() const;

		// static members
		static Vector4 up;
		static Vector4 down;
		static Vector4 zero;
		static Vector4 one;
		static Vector4 right;
		static Vector4 left;
		static Vector4 forward;
		static Vector4 back;


		// static functions
		static Vector4 Max(const Vector4 &v1, const Vector4& v2);
		static Vector4 Max3(const Vector4 &v1, const Vector4& v2, const Vector4 &v3);
		static Vector4 Min(const Vector4 &v1, const Vector4& v2);
		static Vector4 Min3(const Vector4 &v1, const Vector4& v2, const Vector4 &v3);
		static float Distance(const Vector4 &v1, const Vector4& v2);
		static float DistanceSqr(const Vector4 &v1, const Vector4 &v2);
		static float Dot(const Vector4& v1, const Vector4 &v2);

		static Vector4 Lerp(const Vector4 &v1, const Vector4& v2, float factor);

		// operators
		Vector4& operator - () {
			for (uint32_t i = 0; i < 4; i++) {
				Components[i] *= -1.0f;
			}

			return *this;
		}

		Vector4& operator += (const Vector4& rhs) {
			for (uint32_t i = 0; i < 4; i++) {
				Components[i] += rhs[i];
			}

			return *this;
		}

		Vector4& operator -= (const Vector4& rhs) {
			for (uint32_t i = 0; i < 4; i++) {
				Components[i] -= rhs[i];
			}

			return *this;
		}

		Vector4& operator *= (const Vector4& rhs) {
			for (uint32_t i = 0; i < 4; i++) {
				Components[i] *= rhs[i];
			}

			return *this;
		}

		Vector4& operator /= (const Vector4& rhs) {
			for (uint32_t i = 0; i < 4; i++) {
				Components[i] /= rhs[i];
			}

			return *this;
		}

		Vector4& operator *= (float v) {
			for (uint32_t i = 0; i < 4; i++) {
				Components[i] *= v;
			}

			return *this;
		}

		Vector4& operator /= (float v) {
			for (uint32_t i = 0; i < 4; i++) {
				Components[i] /= v;
			}

			return *this;
		}

		Vector4& operator *= (const Matrix4x4& rhs);

		Vector4& operator = (const Vector4& other) {
			if (&other != this) {
				memcpy(Components, other.Components, sizeof(Components));
			}

			return *this;
		}

		float& operator[] (size_t idx) { return Components[idx]; }
		const float& operator[] (size_t idx) const { return Components[idx]; }

		float Components[4];
	};

	inline static Vector4 operator + (Vector4 lhs, const Vector4& rhs) {
		lhs += rhs;
		return lhs;
	}

	inline static Vector4 operator - (Vector4 lhs, const Vector4& rhs) {
		lhs -= rhs;
		return lhs;
	}

	inline static Vector4 operator * (Vector4 lhs, const Vector4& rhs) {
		lhs *= rhs;
		return lhs;
	}

	inline static Vector4 operator / (Vector4 lhs, const Vector4& rhs) {
		lhs /= rhs;
		return lhs;
	}

	inline static Vector4 operator * (Vector4 lhs, float v) {
		lhs *= v;
		return lhs;
	}

	inline static Vector4 operator / (Vector4 lhs, float v) {
		lhs /= v;
		return lhs;
	}

	inline static Vector4 operator * (Vector4 lhs, const Matrix4x4& rhs) {
		lhs *= rhs;
		return lhs;
	}

	static bool operator == (const Vector4& lhs, const Vector4& rhs) {
		bool x = CoreMath::IsNearlyEqual(lhs[0], rhs[0]);
		bool y = CoreMath::IsNearlyEqual(lhs[1], rhs[1]);
		bool z = CoreMath::IsNearlyEqual(lhs[2], rhs[2]);
		bool w = CoreMath::IsNearlyEqual(lhs[3], rhs[3]);

		return (x && y && z && w);
	}

	static bool operator != (const Vector4& lhs, const Vector4& rhs) {
		return !(lhs == rhs);
	}
}

MAKE_HASHABLE(Magma::Vector4, t.Components[0], t.Components[1], t.Components[2], t.Components[3], t.Components[4])