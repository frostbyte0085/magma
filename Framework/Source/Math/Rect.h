#pragma once

#include "MathLib.h"
#include "Vector2.h"

namespace Magma {
	class Rect {
	public:
		Rect();
		Rect(const Rect& other);
		Rect(float x, float y, float width, float height);
		~Rect();

		// functions
		void SetRect(float x, float y, float width, float height);

		float GetX() const { return m_X; }
		float GetY() const { return m_Y; }
		float GetWidth() const { return m_Width; }
		float GetHeight() const { return m_Height; }
		Vector2 GetCenter() const { return m_Center; }

		bool Contains(const Rect& other);
		void Expand(float x, float y);

		Rect& operator += (const Rect& rhs) {
			m_X += rhs.m_X;
			m_Y += rhs.m_Y;
			m_Width += rhs.m_Width;
			m_Height += rhs.m_Height;

			SetRect(m_X, m_Y, m_Width, m_Height);
			return *this;
		}

		Rect& operator -= (const Rect& rhs) {
			m_X -= rhs.m_X;
			m_Y -= rhs.m_Y;
			m_Width -= rhs.m_Width;
			m_Height -= rhs.m_Height;

			SetRect(m_X, m_Y, m_Width, m_Height);
			return *this;
		}

		Rect& operator *= (const Rect& rhs) {
			m_X *= rhs.m_X;
			m_Y *= rhs.m_Y;
			m_Width *= rhs.m_Width;
			m_Height *= rhs.m_Height;

			SetRect(m_X, m_Y, m_Width, m_Height);

			return *this;
		}

		Rect& operator /= (const Rect& rhs) {
			m_X /= rhs.m_X;
			m_Y /= rhs.m_Y;
			m_Width /= rhs.m_Width;
			m_Height /= rhs.m_Height;

			SetRect(m_X, m_Y, m_Width, m_Height);
			return *this;
		}

		Rect& operator *= (float v) {
			m_X *= v;
			m_Y *= v;
			m_Width *= v;
			m_Height *= v;

			SetRect(m_X, m_Y, m_Width, m_Height);
			return *this;
		}

		Rect& operator /= (float v) {
			m_X /= v;
			m_Y /= v;
			m_Width /= v;
			m_Height /= v;

			SetRect(m_X, m_Y, m_Width, m_Height);

			return *this;
		}

	private:
		float m_X, m_Y, m_Width, m_Height;
		Vector2 m_Center;

		inline void calculateCenter();

	};

	inline static Rect operator + (Rect lhs, const Rect& rhs) {
		lhs += rhs;
		return lhs;
	}

	inline static Rect operator - (Rect lhs, const Rect& rhs) {
		lhs -= rhs;
		return lhs;
	}

	inline static Rect operator * (Rect lhs, const Rect& rhs) {
		lhs *= rhs;
		return lhs;
	}

	inline static Rect operator / (Rect lhs, const Rect& rhs) {
		lhs /= rhs;
		return lhs;
	}

	inline static Rect operator * (Rect lhs, float v) {
		lhs *= v;
		return lhs;
	}

	inline static Rect operator / (Rect lhs, float v) {
		lhs /= v;
		return lhs;
	}

	static bool operator == (const Rect &lhs, const Rect &rhs) {

		bool x = CoreMath::IsNearlyEqual(lhs.GetX(), rhs.GetX());
		bool y = CoreMath::IsNearlyEqual(lhs.GetY(), rhs.GetY());
		bool w = CoreMath::IsNearlyEqual(lhs.GetWidth(), rhs.GetWidth());
		bool h = CoreMath::IsNearlyEqual(lhs.GetHeight(), rhs.GetHeight());

		return (x && y && w && h);
	}

	static bool operator != (const Rect &lhs, const Rect &rhs) {

		return !(lhs == rhs);
	}
}