#include "TaskManager.hpp"

using namespace Magma;

static long sMask = Magma::sMaxTaskCountPerQueue - 1U;

WorkQueue::WorkQueue() {
	for (uint32_t i = 0; i < sMaxTaskCountPerQueue; ++i) {
		m_Tasks[i] = nullptr;
	}

	Reset();
}

WorkQueue::~WorkQueue() {
		
}

void WorkQueue::Push(Task* inTask) {
	gAssert(inTask != nullptr, "Expected a valid Task instance");

#ifdef LOCK_FREE_QUEUE
	long bottom = m_Bottom;
	m_Tasks[bottom & sMask] = inTask;

	// Prevent compiler re-ordering
	// Ensure that the task is written before bottom + 1 is published to other threads
	MEMORY_BARRIER;

	m_Bottom = bottom + 1;
#else
	ScopedLock<BasicMutex> lock(&m_Mutex);
	m_Tasks[m_Bottom & sMask] = inTask;
	++m_Bottom;
#endif
}

Task* WorkQueue::Pop() {
#ifdef LOCK_FREE_QUEUE
	long bottom = m_Bottom - 1;
	MEMORY_BARRIER;
	//_InterlockedExchange(&m_Bottom, bottom); // barrier internally		
	long top = m_Top;

	if (top <= bottom) {
		// non-empty queue
		Task* task = m_Tasks[bottom & sMask];

		// there is still more than one item left in the queue
		if (top != bottom) {
			return task;
		}

		//if (_InterlockedCompareExchange(&m_Top, top + 1, top) != top) {
		if (!m_Top.compare_exchange_weak(top, top+1)) {
			task = nullptr;
		}

		m_Bottom = top + 1;
		return task;
	}

	// queue is empty
	m_Bottom = top;
	return nullptr;
#else
	ScopedLock<BasicMutex> lock(&m_Mutex);

	const long task_count = m_Bottom - m_Top;
	if (task_count <= 0)
	{
		// no job left in the queue
		return nullptr;
	}

	--m_Bottom;
	return m_Tasks[m_Bottom & sMask];
#endif
}

Task* WorkQueue::Steal() {
#ifdef LOCK_FREE_QUEUE
	long top = m_Top;

	// Prevent compiler re-ordering
	// Ensure that top is read before bottom
	MEMORY_BARRIER;

	long bottom = m_Bottom;

	// if top < bottom then it means that there are tasks in the queue
	if (top < bottom) {
		Task* task = m_Tasks[top & sMask];
		
		// if the cas fails, then it means that a concurrent steal/pop has removed a task from the queue
		//if (_InterlockedCompareExchange(&m_Top, top + 1, top) != top) {
		if (!m_Top.compare_exchange_weak(top, top+1)) {
			return nullptr;
		}
		
		return task;
	}

	// queue is empty
	return nullptr;
#else
	ScopedLock<BasicMutex> lock(&m_Mutex);
	const long task_count = m_Bottom - m_Top;
	if (task_count <= 0) {
		return nullptr;
	}

	Task* task = m_Tasks[m_Top & sMask];
	++m_Top;
	return task;
#endif
}

void WorkQueue::Reset() {
#ifndef LOCK_FREE_QUEUE
	ScopedLock<BasicMutex> lock(&m_Mutex);
#endif

	m_Top = 0L;
	m_Bottom = 0L;
}