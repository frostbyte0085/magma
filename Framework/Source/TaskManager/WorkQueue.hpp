#pragma once

#include <Utilities/Mutex.hpp>

// TODO: Uncomment to go lock-free
// Needs fixing, deadlocks somewhere.
// #define LOCK_FREE_QUEUE

namespace Magma {
	struct Task;
	class TaskManager;

	static const uint32_t sMaxTaskCountPerQueue = 4096;

	// a lock-free queue which every worker in the TaskManager has local to it
	//
	// Thanks to:
	// https://blog.molecular-matters.com/2015/09/25/job-system-2-0-lock-free-work-stealing-part-3-going-lock-free/
	class WorkQueue : public NonCopyable {
		friend class TaskManager;
	public:
		void Reset();
		void Push(Task* inTask);
		Task* Pop();
		Task* Steal();
		inline long GetTaskCount() const { 
			ScopedLock<BasicMutex> lock(&m_Mutex);
			return std::max(m_Bottom - m_Top, 0L);
		}

	private:
		WorkQueue();
		~WorkQueue();

#ifndef LOCK_FREE_QUEUE
		BasicMutex m_Mutex;
#endif

		Task* m_Tasks[sMaxTaskCountPerQueue];
		long m_Top;
		long m_Bottom;
	};	
}