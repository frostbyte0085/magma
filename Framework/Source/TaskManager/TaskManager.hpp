/*
Copyright(c) 2015-2017, Pantelis Lekakis

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include "WorkQueue.hpp"
#include <Utilities/Semaphore.hpp>

namespace Magma {
	
	typedef int16_t TaskId;
	typedef void(*TaskFunc)(struct Task*, const void*);

	static const int16_t sTaskNoParent = -1;

	ENUM_FLAGS(ETaskFlag)
	enum class ETaskFlag {
		NONE,
		SYNCHRONOUS_FOLLOWUP_TASKS
	};

	enum class EWorkQueueStealMode {
		MOST_BUSY,
		RANDOM
	};

	// a monolithic task to be executed on a thread
	// current size = 128bytes
	//ALIGN128
	struct Task {
		TaskFunc EntryPoint;						// 8
		void* TaskData = nullptr;					// 8
		TaskId Parent = sTaskNoParent;				// 2
		std::atomic_uint16_t OpenTasks = 0;			// 2
	};

	// a threaded task manager
	// performs task-stealing and auto-balancing
	class TaskManager : public NonCopyable {
	public:
		TaskManager();
		~TaskManager();

		void Initialise();
		void Reset();
		inline uint32_t GetWorkerCount() const { return m_ThreadCount; }
		TaskId CreateTask(TaskFunc inTaskEntryPoint, void* inTaskData, TaskId inParent = sTaskNoParent, ETaskFlag inFlags = ETaskFlag::NONE);
		TaskId CreateEmptyTask(TaskId inParent = sTaskNoParent);
		void AddTask(TaskId inTask);
		void SignalWork();
		
#ifdef WORK_STEALING
		inline void SetWorkQueueStealMode(EWorkQueueStealMode inMode) { m_WorkQueueStealMode = inMode; }
		inline EWorkQueueStealMode GetWorkQueueStealMode() const { return m_WorkQueueStealMode; }
#endif

		void WaitOnSemaphore(CounterSemaphore* const inSemaphore);
		void WaitOnTask(TaskId inTask);

	private:
		void doWork(uint32_t inThreadIndex);
		Task* getNextAvailableTask(uint32_t inThreadIndex);
		size_t getQueueLength(uint32_t inThreadIndex);
		void workOnTask(Task* inTask, uint32_t inThreadIndex);
		bool executeTask(Task* inTask);
		void finishTask(Task* inTask);
		void yield();

		std::thread* m_Threads = nullptr;
		uint32_t m_ThreadCount = 0;

		std::atomic_bool m_IsActive = true;		

#ifndef WORK_STEALING
		BasicMutex m_Mutex;
		std::queue<Task*> m_Tasks;
#else
		EWorkQueueStealMode m_WorkQueueStealMode;
		WorkQueue* m_Queues = nullptr;
#endif
				
		Semaphore m_WorkSemaphore;
		bool m_WorkNotified;
	};

	extern std::shared_ptr<TaskManager> gTaskMgr;
	
	// allocations
	extern Task* gAllocateTask();

	// taskid <-> taskptr
	extern TaskId gGetTaskId(Task* inTask);
	extern Task* gGetTaskPtr(TaskId inTaskId);
}