#include "TaskManager.hpp"

using namespace Magma;

// some globals and extern definitions
std::shared_ptr<TaskManager> Magma::gTaskMgr;
std::random_device gRndDevice;
std::mt19937 gRnd(gRndDevice());

// pool allocator
// TODO: move to tls
static Task gTaskAllocator[Magma::sMaxTaskCountPerQueue * 8];
static std::atomic_uint32_t gAllocatedTasks = 0;

Task* Magma::gAllocateTask() {
	const uint32_t index = (++gAllocatedTasks);
	return &gTaskAllocator[(index - 1) % Magma::sMaxTaskCountPerQueue];
}

// taskid <-> taskptr conversion
TaskId Magma::gGetTaskId(Task* inTask) {
	return static_cast<int16_t>(inTask - &gTaskAllocator[0]);
}

Task* Magma::gGetTaskPtr(TaskId inTaskId) {
	return &gTaskAllocator[0] + inTaskId;
}

//
// TaskManager
//
TaskManager::TaskManager() {

}

TaskManager::~TaskManager() {
	m_IsActive = false;
	m_WorkSemaphore.SignalAll();

	for (uint32_t i = 0; i < m_ThreadCount; ++i) {
		m_Threads[i].join();
	}

#ifdef WORK_STEALING
	SDELETE_ARRAY(m_Queues);
#endif
	SDELETE_ARRAY(m_Threads);
}

void TaskManager::Initialise() {
	m_ThreadCount = std::thread::hardware_concurrency() - 1;
	m_Threads = new std::thread[m_ThreadCount];

#ifdef WORK_STEALING
	m_Queues = new WorkQueue[m_ThreadCount];
	SetWorkQueueStealMode(EWorkQueueStealMode::MOST_BUSY);
#endif

	gTrace("TaskManager: Starting %i threads...", m_ThreadCount);
	
	for (uint32_t i = 0; i < m_ThreadCount; ++i) {
		m_Threads[i] = std::thread(&TaskManager::doWork, this, i);
	}
}

// this is called at the beginning of the frame to reset the bottom and top indices of each queue.
void TaskManager::Reset() {
#ifdef WORK_STEALING
	for (uint32_t i = 0; i < m_ThreadCount; ++i) {
		m_Queues[i].Reset();
	}
#endif
}

TaskId TaskManager::CreateEmptyTask(TaskId inParent) {
	Task* task = gAllocateTask();

	gAssert(task != nullptr, "Failed allocating a new Task instance");

	if (inParent != sTaskNoParent) {
		++gGetTaskPtr(inParent)->OpenTasks; // atomic
	}

	task->EntryPoint = nullptr;
	task->TaskData = nullptr;
	task->Parent = inParent;
	task->OpenTasks = 1;

	return gGetTaskId(task);
}

TaskId TaskManager::CreateTask(TaskFunc inTaskEntryPoint, void* inTaskData, TaskId inParent, ETaskFlag inFlags) {
	Task* task = gGetTaskPtr(CreateEmptyTask(inParent));

	task->EntryPoint = inTaskEntryPoint;
	task->TaskData = inTaskData;	
		
	return gGetTaskId(task);
}

void TaskManager::SignalWork() {
	m_WorkSemaphore.SignalAll();
}

void TaskManager::AddTask(TaskId inTask) {
#ifdef WORK_STEALING
	// find the first worker queue with the least elements and add this task to it
	// default to the first queue
	uint32_t least_busy_queue_index = 0;
	size_t last_count = Magma::sMaxTaskCountPerQueue;

	for (uint32_t i = 0; i < m_ThreadCount; ++i) {
		size_t count = m_Queues[i].GetTaskCount();
		if (count < last_count) {
			last_count = count;
			least_busy_queue_index = i;
		}
	}

	WorkQueue& queue = m_Queues[least_busy_queue_index];
	queue.Push(gGetTaskPtr(inTask));
#else
	ScopedLock<BasicMutex> lock(&m_Mutex);
	m_Tasks.push(gGetTaskPtr(inTask));
#endif	
}

void TaskManager::WaitOnTask(TaskId inTask) {
	Task* task = gGetTaskPtr(inTask);
	if (task) {
		while (m_IsActive && task->OpenTasks > 0) {
			// main thread can work on another task while waiting on this one
			Task* next_task = getNextAvailableTask(0);
			if (next_task) workOnTask(next_task, 0);
		}
	}
}

void TaskManager::WaitOnSemaphore(CounterSemaphore* const inSemaphore) {
	gAssert(inSemaphore != nullptr, "Expected a valid CounterSemaphore instance");
	inSemaphore->Wait();
}

size_t TaskManager::getQueueLength(uint32_t inThreadIndex) {
#ifdef WORK_STEALING
	WorkQueue& this_queue = m_Queues[inThreadIndex];
	return this_queue.GetTaskCount();
#else
	return m_Tasks.size();
#endif
}

Task* TaskManager::getNextAvailableTask(uint32_t inThreadIndex) {
#ifdef WORK_STEALING
	WorkQueue& this_queue = m_Queues[inThreadIndex];

	for (int i = 0; i < m_ThreadCount; i++) {
		WorkQueue& temp = m_Queues[i];
		long t = temp.GetTaskCount();

		int j = 0;
	}

	Task* next_task = this_queue.Pop();
	if (!next_task) {

		uint32_t steal_queue_index = 0;

		if (GetWorkQueueStealMode() == EWorkQueueStealMode::MOST_BUSY) {
			// not valid job, we need to pick a worker to steal a task from
			// pick the one with the most tasks in queue
			size_t last_count = 0;

			for (uint32_t i = 0; i < m_ThreadCount; ++i) {
				if (inThreadIndex == i) continue;

				size_t count = m_Queues[i].GetTaskCount();
				if (count > last_count) {
					last_count = count;
					steal_queue_index = i;
				}
			}
		}
		else if (GetWorkQueueStealMode() == EWorkQueueStealMode::RANDOM) {
			// steal from a random queue
			std::uniform_int_distribution<uint32_t> uniform(0, m_ThreadCount-1);
			steal_queue_index = uniform(gRnd);
		}

		WorkQueue& steal_queue = m_Queues[steal_queue_index];
		if (&steal_queue == &this_queue) {
			yield();
			return nullptr;
		}

		// try to steal a task from the selected worker queue
		Task* stolen_next_task = steal_queue.Steal();
		if (!stolen_next_task) {
			yield();
			return nullptr;
		}
		return stolen_next_task;
	}
	return next_task;

#else
	ScopedLock<BasicMutex> lock(&m_Mutex);
	Task* task = nullptr;

	if (m_Tasks.size() > 0) {
		task = m_Tasks.front();
		m_Tasks.pop();
	}
	return task;
#endif
}

void TaskManager::yield() {
	timeBeginPeriod(1);
	Sleep(1);
}

void TaskManager::finishTask(Task* inTask) {
	const uint32_t open_tasks = (--inTask->OpenTasks);

	if (open_tasks == 0) {
		
		if (inTask->Parent != sTaskNoParent) {
			finishTask(gGetTaskPtr(inTask->Parent));
		}
	}
}

void TaskManager::doWork(uint32_t inThreadIndex) {
	while (true) {

		while (m_IsActive && getQueueLength(inThreadIndex) == 0) {
			m_WorkSemaphore.Wait();
		}

		if (!m_IsActive) break;
		
		Task* next_task = getNextAvailableTask(inThreadIndex);
		workOnTask(next_task, inThreadIndex);

		yield();
	}
}

void TaskManager::workOnTask(Task* inTask, uint32_t inThreadIndex) {
	if (inTask) {
		// Block until dependencies have been resolved
		// Work on other items
		while (!executeTask(inTask)) {
			// TODO: In work stealing mode, this gets stuck, nextAvailableTask returns null.
			// There must be an issue with the queue.
			// happens when OpenTasks > 1
			Task* next_task = getNextAvailableTask(inThreadIndex);
			if (next_task) {
				workOnTask(next_task, inThreadIndex);
			}
			else{
				yield();
			}
		}
	}
}

bool TaskManager::executeTask(Task* inTask) {
	if (inTask->OpenTasks == 1) {
		if (inTask->EntryPoint) {
			inTask->EntryPoint(inTask, inTask->TaskData);
		}
		finishTask(inTask);

		return true;
	}

	return false;
}