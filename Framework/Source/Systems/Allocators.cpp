#include "Allocators.hpp"

namespace Magma {
	ScratchAllocator* gScratchAlloc = nullptr;

	void gInitialiseAllocators() {
		gScratchAlloc = new ScratchAllocator(SCRATCH_MEMORY_SIZE);
	}

	void gFreeAllocators() {
		delete gScratchAlloc;
	}

	void gGatherMemoryStats(AllocatorMemoryStatistics& outStats) {
		outStats.Scratch.Total = gScratchAlloc->GetTotalMemoryKB();
		outStats.Scratch.Used = gScratchAlloc->GetUsedMemoryKB();
		outStats.Scratch.Free = gScratchAlloc->GetFreeMemoryKB();
	}
}