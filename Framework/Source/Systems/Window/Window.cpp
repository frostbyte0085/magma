#include "Systems/Window/Window.hpp"

using namespace Magma;

LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam)
{
	switch (umessage)
	{
		// Check if the window is being destroyed.
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		return 0;
	}

	// Check if the window is being closed.
	case WM_CLOSE:
	{
		PostQuitMessage(0);
		return 0;
	}

	// All other messages pass to the message handler in the system class.
	default:
	{
		//return ApplicationHandle->MessageHandler(hwnd, umessage, wparam, lparam);
		return DefWindowProc(hwnd, umessage, wparam, lparam);
	}
	}

	return 0;
}


Window::Window(): m_WindowHandle(nullptr), m_Instance(nullptr) {
	m_Title = L"RenderWindow";
}

Window::~Window() {
	if (m_IsFullscreen) {
		ChangeDisplaySettings(nullptr, 0);
	}

	if (m_WindowHandle)	DestroyWindow(m_WindowHandle);
	m_WindowHandle = nullptr;

	std::wstring wtitle(m_Title.begin(), m_Title.end());
	if (m_Instance) UnregisterClass(wtitle.c_str(), m_Instance);
	m_Instance = nullptr;
}

void Window::SetTitle(const std::wstring& title) {
	m_Title = title;
	SetWindowText(m_WindowHandle, title.c_str());
}

bool Window::SetSize(uint32_t width, uint32_t height, bool isFullscreen) {
	
	bool success = false;

	if (isFullscreen) {
		SetWindowRgn(GetHandle(), 0, false);

		DEVMODE dmScreenSettings;
		ZeroMemory(&dmScreenSettings, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth = (unsigned long)width;
		dmScreenSettings.dmPelsHeight = (unsigned long)height;
		dmScreenSettings.dmBitsPerPel = 32;
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen.
		long result = ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);
		if (result == DISP_CHANGE_SUCCESSFUL) {
			DWORD style = GetWindowLong(GetHandle(), GWL_STYLE);
			style &= ~WS_OVERLAPPEDWINDOW;
			style |= WS_POPUP;
			SetWindowLong(GetHandle(), GWL_STYLE, style);

			if (SetWindowPos(GetHandle(), 0, 0, 0, width, height, 0)) {
				if (InvalidateRect(GetHandle(), nullptr, true)) {
					success = true;
				}
			}
		}
	}
	else {
		long result = ChangeDisplaySettings(nullptr, 0);
		if (result == DISP_CHANGE_SUCCESSFUL) {
			int x = (GetSystemMetrics(SM_CXSCREEN) - width) / 2;
			int y = (GetSystemMetrics(SM_CYSCREEN) - height) / 2;

			DWORD style = GetWindowLong(GetHandle(), GWL_STYLE);
			style &= ~WS_POPUP;
			style |= WS_OVERLAPPEDWINDOW;
			SetWindowLong(GetHandle(), GWL_STYLE, style);

			if (SetWindowPos(GetHandle(), 0, x, y, width, height, 0)) {
				if (InvalidateRect(GetHandle(), nullptr, true)) {
					success = true;
				}
			}
		}
	}

	if (success) {
		m_IsFullscreen = isFullscreen;
		m_Width = width;
		m_Height = height;
	}

	return success;
}

bool Window::IsAlive() {
	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return msg.message != WM_QUIT;
}

void Window::Initialise() {

	WNDCLASSEX wc;

	m_IsFullscreen = false;
	m_Width = kDefaultWidth;
	m_Height = kDefaultHeight;

	// Get the instance of this application.
	m_Instance = GetModuleHandle(nullptr);
	if (m_Instance == nullptr) {
		throw std::exception("Cannot create window class instance!");
	}

	// Setup the windows class with default settings.
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = m_Instance;
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	wc.hIconSm = wc.hIcon;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = m_Title.c_str();
	wc.cbSize = sizeof(WNDCLASSEX);

	// Register the window class.
	RegisterClassEx(&wc);
	
	// Place the window in the middle of the screen.
	int posX = (GetSystemMetrics(SM_CXSCREEN) - m_Width) / 2;
	int posY = (GetSystemMetrics(SM_CYSCREEN) - m_Height) / 2;
	
	// Create the window with the screen settings and get the handle to it.
	m_WindowHandle = CreateWindowEx(WS_EX_APPWINDOW, m_Title.c_str(), m_Title.c_str(),
		WS_OVERLAPPEDWINDOW,
		posX, posY, m_Width, m_Height, NULL, NULL, m_Instance, NULL);

	if (m_WindowHandle == nullptr) {
		throw std::exception("Cannot create window!");
	}

	// Bring the window up on the screen and set it as main focus.
	ShowWindow(m_WindowHandle, SW_SHOW);
	SetForegroundWindow(m_WindowHandle);
	SetFocus(m_WindowHandle);
}