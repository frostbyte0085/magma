#pragma once

#include "../System.hpp"

namespace Magma {
	class Window: public System {
	public:
		Window();
		~Window();

		bool IsAlive();

		HWND GetHandle() const { return m_WindowHandle; }
		HINSTANCE GetInstance() const { return m_Instance; }

		bool SetSize(uint32_t width, uint32_t height, bool isFullscreen);
		void SetTitle(const std::wstring& title);

		virtual void Begin() override {}
		virtual void End() override {}
		virtual void Update(float dt) override {}
		virtual void Initialise() override;

		inline uint32_t GetWidth() const { return m_Width; }
		inline uint32_t GetHeight() const { return m_Height; }

	private:
		HWND m_WindowHandle;
		HINSTANCE m_Instance;
		std::wstring m_Title;

		const uint32_t kDefaultWidth = 1280;
		const uint32_t kDefaultHeight = 720;

		uint32_t m_Width;
		uint32_t m_Height;
		bool m_IsFullscreen;
	};
}