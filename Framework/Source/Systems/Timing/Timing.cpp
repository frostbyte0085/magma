//
//  Timing.cpp
//  TerrainEngine
//
//  Created by Pantelis Lekakis on 07/02/2016.
//  Copyright © 2016 Pantelis Lekakis. All rights reserved.
//

#include "Systems/Timing/Timing.hpp"
#include "Systems/Timing/Timer.hpp"

using namespace Magma;

Time::Time() : m_ElapsedMs(0.0f), m_DeltaMs(0.0f), m_Frames(0), m_FramesPerSecond(0.0f), m_AccumulatedTime(0.0f) {
    
}

Time::~Time() {
    
}

void Time::Initialise() {
    m_Timer = std::unique_ptr<Timer>(new Timer());
    m_Timer->Reset();
}

void Time::Begin() {
}

void Time::End() {
    m_DeltaMs = m_Timer->GetElapsedMs();
}

void Time::Update(float dt) {
    m_ElapsedMs += m_DeltaMs;
    
    m_AccumulatedTime += (m_DeltaMs / 1000.0f);
    ++m_Frames;
    
    if (m_AccumulatedTime > kFPSUpdateInterval) {
        m_FramesPerSecond =  m_Frames / m_AccumulatedTime;
        m_AccumulatedTime = 0.0f;
        m_Frames = 0;
    }
    
}
