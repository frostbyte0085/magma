//
//  Timer.cpp
//  TerrainEngine
//
//  Created by Pantelis Lekakis on 07/02/2016.
//  Copyright © 2016 Pantelis Lekakis. All rights reserved.
//

#include "Systems/Timing/Timer.hpp"

using namespace std::chrono;
using namespace Magma;

Timer::Timer() {
    Reset();
}

Timer::~Timer() {
    
}

void Timer::Reset() {
    m_TimeEnd = m_LastTimeEnd = high_resolution_clock::now();
}

float Timer::GetElapsedMs() {
    m_TimeEnd = high_resolution_clock::now();
    auto frameTime = m_TimeEnd - m_LastTimeEnd;
    m_LastTimeEnd = m_TimeEnd;
    
    return duration_cast<duration<float, std::milli>>(frameTime).count();
}

float Timer::GetElapsedSeconds() {
    return GetElapsedMs() / 1000.0f;
}