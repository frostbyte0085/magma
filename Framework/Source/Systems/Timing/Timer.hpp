//
//  Timer.hpp
//  TerrainEngine
//
//  Created by Pantelis Lekakis on 07/02/2016.
//  Copyright © 2016 Pantelis Lekakis. All rights reserved.
//

#ifndef Timer_hpp
#define Timer_hpp

#include "../../Framework.h"

namespace Magma {
	class Timer {
	public:
		Timer();
		~Timer();

		void Reset();

		float GetElapsedMs();
		float GetElapsedSeconds();

	private:
		std::chrono::time_point<std::chrono::high_resolution_clock> m_LastTimeEnd, m_TimeEnd;
		std::chrono::duration<float, std::milli> m_ElapsedMs;
	};
}

#endif /* Timer_hpp */
