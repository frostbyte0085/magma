//
//  Timing.hpp
//  TerrainEngine
//
//  Created by Pantelis Lekakis on 07/02/2016.
//  Copyright © 2016 Pantelis Lekakis. All rights reserved.
//

#ifndef Timing_hpp
#define Timing_hpp

#include "../System.hpp"

namespace Magma {
	class Timer;

	// this is the main time component that the engine uses to track delta time and framerate.
	class Time : public System {
	public:
		Time();
		~Time();

		virtual void Begin() override;
		virtual void End() override;
		virtual void Update(float dt) override;
		virtual void Initialise() override;

		float GetDeltaMs() const { return m_DeltaMs; }
		float GetDeltaSeconds() const { return m_DeltaMs / 1000.0f; }
		float GetElapsedMs() const { return m_ElapsedMs; }
		float GetElapsedSeconds() const { return m_ElapsedMs / 1000.0f; }
		float GetFramesPerSecond() const { return m_FramesPerSecond; }
	private:
		std::unique_ptr<Timer> m_Timer;

		float m_DeltaMs;
		float m_ElapsedMs;

		uint32_t m_Frames;
		float m_FramesPerSecond;
		float m_AccumulatedTime;

		const float kFPSUpdateInterval = 0.25f;
	};
}
#endif /* Timing_hpp */
