//
//  System.hpp
//  TerrainEngine
//
//  Created by Pantelis Lekakis on 07/02/2016.
//  Copyright © 2016 Pantelis Lekakis. All rights reserved.
//

#ifndef System_hpp
#define System_hpp

#include "../Framework.h"

namespace Magma {
	// Base class for all subsystems.
	class System {
	public:

		virtual void Begin() = 0;
		virtual void End() = 0;
		virtual void Update(float dt) = 0;

		virtual void Initialise() = 0;

		virtual ~System() {}
	};
}
#endif /* System_hpp */
