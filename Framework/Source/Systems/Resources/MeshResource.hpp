#pragma once

#include "ResourceBase.hpp"

namespace Magma {
	class Mesh;

	class MeshResource : public ResourceBase {
	public:
		MeshResource(const std::string& filename) : ResourceBase(filename) { }
		~MeshResource();

		virtual void Load() override;
		virtual void Unload() override;

		Mesh* const GetMesh() const { assert(m_Mesh); return m_Mesh; }

	private:
		Mesh* m_Mesh = nullptr;
	};
}