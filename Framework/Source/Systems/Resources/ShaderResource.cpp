#include "ShaderResource.hpp"
#include "../Graphics/Shader.hpp"
#include <Application/Application.hpp>

using namespace Magma;

ShaderResource::~ShaderResource() {

}

void ShaderResource::Load() {
	std::wstring filename;
	filename.assign(GetResourceFileName().begin(), GetResourceFileName().end());

	gGfx->CreateShader(filename, GetTechnique(), &m_Shader);
}

void ShaderResource::Unload() {
	
}