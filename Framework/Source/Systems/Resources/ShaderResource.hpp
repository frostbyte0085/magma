#pragma once

#include "ResourceBase.hpp"
#include "../Graphics/Enumerations.hpp"

namespace Magma {
	
	// a shader resource
	class ShaderResource : public ResourceBase {
	public:
		ShaderResource(const std::string& filename, ERenderTechnique technique) : ResourceBase(filename) {
			m_Technique = technique;
		}

		~ShaderResource();

		virtual void Load() override;
		virtual void Unload() override;

		ERenderTechnique GetTechnique() const { return m_Technique; }
		Shader* GetShader() const { return m_Shader; }

	private:
		ERenderTechnique m_Technique;
		Shader* m_Shader = nullptr;
	};
}