#include "Resources.hpp"

using namespace Magma;

Resources::Resources() {

}

Resources::~Resources() {

}

void Resources::Begin() {

}

void Resources::End() {

}

void Resources::Update(float dt) {

}

void Resources::Initialise() {

	// text resource
	{
		m_TextResourceManager.Initialise("DefaultTextResourceManager");
		m_TextResourceMap.Initialise("DefaultTextResourceMap", &m_TextResourceManager);
	}

	// mesh resource
	{
		m_MeshResourceManager.Initialise("DefaultMeshResourceManager");
		m_MeshResourceMap.Initialise("DefaultMeshResourceMap", &m_MeshResourceManager);
	}

	// shader resource
	{
		m_ShaderResourceManager.Initialise("DefaultShaderResourceManager");
		m_ShaderResourceMap.Initialise("DefaultShaderResourceMap", &m_ShaderResourceManager);
	}
}