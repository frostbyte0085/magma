#pragma once

namespace Magma {
	class MeshResource;
	class ShaderResource;
	class TextResource;
	class Resources;
}