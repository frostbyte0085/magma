#pragma once

#include <Framework.h>

namespace Magma {

	// Based on Davide Cleopesce's implementation on gamedev.net
	// http://www.gamedev.net/page/resources/_/technical/game-programming/a-resource-manager-for-game-assets-r3807
	//
	class ResourceBase : public NonCopyable
	{
		template <class T> friend class ResourceManager;

	public:
		ResourceBase(const std::string& filename)
		{
			// exit with an error if filename is empty
			assert(!filename.empty());

			m_ResourceFileName = std::move(filename);
			gTrimString(m_ResourceFileName);
			gLowerStringASCII(m_ResourceFileName);
		}

		virtual ~ResourceBase()
		{
		}

		// called when ref count goes from 0 -> 1
		virtual void Load() = 0;

		// caled when ref count goes down to 0
		virtual void Unload() = 0;

		inline const std::string &GetResourceFileName() const
		{
			return m_ResourceFileName;
		}

		inline const int32_t GetReferencesCount() const
		{
			return m_References;
		}

	private:		
		int32_t m_References = 0;

		inline int32_t incReferences() { return ++m_References; }
		inline int32_t decReferences() { return --m_References; }

	protected:
		// resource filename
		std::string m_ResourceFileName;
	};
}