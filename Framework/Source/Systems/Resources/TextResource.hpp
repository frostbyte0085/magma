#pragma once

#include "ResourceBase.hpp"

namespace Magma {
	// a read-only text resource
	class TextResource : public ResourceBase {
	public:
		TextResource(const std::string& filename) : ResourceBase(filename) { }
		~TextResource();

		virtual void Load() override;
		virtual void Unload() override;

		const std::string& GetContents() const { return m_Contents; }
	private:
		std::string m_Contents;
	};
}