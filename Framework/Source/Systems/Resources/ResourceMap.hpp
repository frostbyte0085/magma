#pragma once

#include "ResourceManager.hpp"

namespace Magma {
	// Based on Davide Cleopesce's implementation on gamedev.net
	// http://www.gamedev.net/page/resources/_/technical/game-programming/a-resource-manager-for-game-assets-r3807
	//
	template < class T>
	class ResourceMap : public NonCopyable
	{
	public:
		ResourceMap() { }

		~ResourceMap() {
			Clear();			// remove elements if unique
		}

		//////////////////////////////////////////////////////////////////////////////////////
		// adds a new element

		template<class... Args>
		T *Add(const std::string &resourcename, const std::string &filename, Args... args)
		{
			assert(m_AttachedResourceManager);
			assert(!filename.empty());
			assert(!resourcename.empty());

			std::string res_name = std::move(resourcename);
			gTrimString(res_name);
			gLowerStringASCII(res_name);

			// looks in the hashmap to see if the
			// resource is already loaded
			std::unordered_map< std::string, T* >::iterator it = m_Map.find(res_name);

			if (it == m_Map.end())
			{
				std::string file_name = std::move(filename);
				gTrimString(file_name);
				gLowerStringASCII(file_name);

				// if duplicates flag is set to true , duplicated mapped values
				// are allowed, if duplicates flas is set to false, duplicates won't be allowed
				assert(IsValNonUnique(file_name));
				
				T *resource = m_AttachedResourceManager->Load(file_name, args...);
				
				// allocate new resource using the raii paradigm
				m_Map.insert(std::pair< std::string, T* >(res_name, resource));
				return resource;
			}

			return nullptr;
		}

		/////////////////////////////////////////////////////////
		// delete element using resourcename
		bool Remove(const std::string &resourcename)
		{
			assert(m_AttachedResourceManager);
			assert(!resourcename.empty());

			std::string res_name = std::move(resourcename);
			gTrimString(res_name);
			gLowerStringASCII(res_name);

			// do we have this item ?
			std::unordered_map< std::string, T* >::iterator it = Map.find(ResourceName);

			// yes, delete element, since it is a reference counted pointer, 
			// the reference count will be decreased
			if (it != m_Map.end())
			{
				// save resource name
				const std::string& filename = (*it).second->GetResourceFileName();

				// erase from this map
				m_Map.erase(it);

				// check if it is unique and erase it eventually
				m_AttachedResourceManager->Unload(filename);
				return true;
			}

			// if we get here , node couldn't be found
			// so , exit with an error
			return false;
		}

		//////////////////////////////////////////////////////////
		// clear all elements from map
		void Clear()
		{
			std::unordered_map< std::string, T* >::iterator it = m_Map.begin();

			// walk trhough all the map
			while (it != m_Map.end())
			{
				// save resource name 
				const std::string& filename = (*it).second->GetResourceFileName();

				// clear from this map
				it = m_Map.erase(it);

				// check if it is unique and erase it eventually
				m_AttachedResourceManager->Unload(filename);
			}
		}

		//////////////////////////////////////////////////////////
		// dummps database content to a string
		std::string Dump()
		{
			assert(m_AttachedResourceManager);

			std::string str = CStringFormatter::Format("\nDumping database %s\n\n", m_Name.c_str());

			for (std::unordered_map< std::string, T* >::iterator it = Map.begin(); it != m_Map.end(); ++it)
			{
				str += CStringFormatter::Format("resourcename : %s , %s\n",
					(*it).first.c_str(),
					(*it).second->GetResourceFileName().c_str());
			}

			return str;
		}

		/////////////////////////////////////////////////////////
		// getters

		/////////////////////////////////////////////////////////
		// gets arrays name

		inline const std::string &GetName() const { return m_Name; }
		inline const int Size() const { return m_Map.size(); }

		//////////////////////////////////////////////////////////
		// gets const reference to resource manager

		const ResourceManager<T> *GetResourceManager() { return m_AttachedResourceManager; }

		/////////////////////////////////////////////////////////
		// gets element using resourcename, you should use this
		// as a debug feature or to get shared pointer and later
		// use it , using it in a section where performance is
		// needed might slow down things a bit

		T *Get(const std::string &resourcename)
		{
			assert(m_AttachedResourceManager);
			assert(!resourcename.empty());

			std::string res_name = std::move(resourcename);
			gTrimString(res_name);
			gLowerStringASCII(res_name);

			std::unordered_map< std::string, T* >::iterator it;

			// do we have this item ?
			it = m_Map.find(res_name);

			// yes, return pointer to element
			if (it != m_Map.end()) return it->second;

			return nullptr;
		}

		/////////////////////////////////////////////////////////
		// setters

		inline void SetAllowDuplicates(bool allow) { m_AllowDuplicates = allow; }
		inline bool GetAllowDuplicates() const { return m_AllowDuplicates; }

		////////////////////////////////////////////////////////////
		// initialise resource mapper
		void Initialise(const std::string &name, ResourceManager<T> *resourcemanager, bool duplicates=true)
		{
			assert(resourcemanager);
			assert(!name.empty());

			m_Name = std::move(name);
			gTrimString(m_Name);
			gLowerStringASCII(m_Name);

			m_AttachedResourceManager = resourcemanager;

			SetAllowDuplicates(duplicates);			
		}
		
		private:
			/////////////////////////////////////////////////////////
			// find in all the map the value requested
			bool IsValNonUnique(const std::string &filename)
			{
				// if duplicates are allowed , then return always true
				if (GetAllowDuplicates()) return true;

				// else , check if element by value is already present
				// if it is found, then rturn treu, else exit with false
				std::unordered_map< std::string, T* >::iterator it = m_Map.begin();

				while (it != m_Map.end())
				{
					if ((it->second->GetResourceFileName() == filename)) return false;
					++it;
				}

				return true;
			}

			//////////////////////////////////////////////////////////////////////////////
			// private data

			std::string m_Name;							// name for this resource mapper
			bool m_AllowDuplicates;							// allows or disallwos duplicated filenames for resources
			ResourceManager<T> *m_AttachedResourceManager;		// attached resource manager
			std::unordered_map< std::string, T* > m_Map;	// resource mapper
	};
}