#pragma once

#include "ResourceBase.hpp"

namespace Magma {
	// Based on Davide Cleopesce's implementation on gamedev.net
	// http://www.gamedev.net/page/resources/_/technical/game-programming/a-resource-manager-for-game-assets-r3807
	//
	template < class T>
	class ResourceManager : public NonCopyable
	{
	public:
		ResourceManager()
		{

		}

		~ResourceManager()
		{
			releaseAll();
		}

		///////////////////////////////////////////////////
		// add an asset to the database
		template <class... Args>
		T *Load(const std::string &filename, Args... args)
		{
			// check if filename is not empty
			assert(!filename.empty());

			std::string file_name = std::move(filename);
			gTrimString(file_name);
			gLowerStringASCII(file_name);

			// looks in the map to see if the
			// resource is already loaded

			std::unordered_map< std::string, T* >::iterator it = m_Map.find(file_name);

			if (it != m_Map.end())
			{
				(*it).second->incReferences();
				return (*it).second;
			}

			// if we get here the resource must be loaded
			// allocate new resource using the raii paradigm
			// you must supply the class with a proper constructor
			// see header for details
			T *resource = new T(file_name, args...);
			resource->Load();

			// increase references , this sets the references count to 1
			resource->incReferences();

			// insert into the map
			m_Map.insert(std::pair< std::string, T* >(file_name, resource));
			return resource;
		}

		///////////////////////////////////////////////////////////
		// deleting an item
		bool Unload(const std::string &filename)
		{
			// check if filename is not empty
			assert (!filename.empty());

			std::string file_name = std::move(filename);

			// find the item to delete
			std::unordered_map< std::string, T* >::iterator it = m_Map.find(file_name);

			if (it != m_Map.end())
			{
				// decrease references
				(*it).second->decReferences();

				// if item has 0 references, means
				// the item isn't more used so , 
				// delete from main  database
				if ((*it).second->GetReferencesCount() == 0)
				{
					(*it).second->Unload();

					// call the destructor 
					SDELETE((*it).second);
					m_Map.erase(it);
				}

				return true;
			}

			return false;
		}


		//////////////////////////////////////////////////////////////////////
		// initialise
		void Initialise(const std::string &name)
		{
			// check if name is not empty
			assert(!name.empty());

			m_Name = std::move(name);
			gTrimString(m_Name);
			gLowerStringASCII(m_Name);
		}

		////////////////////////////////////////////////
		// get name for database
		inline const std::string &GetName() const { return m_Name; }
		inline const size_t Size() const { return m_Map.size(); }
		
		private:
			// data members
			std::unordered_map< std::string, T* > m_Map;
			std::string	m_Name;

			// force removal for each node
			void releaseAll()
			{
				std::unordered_map< std::string, T* >::iterator it = m_Map.begin();

				while (it != m_Map.end())
				{
					SDELETE((*it).second);
					it = m_Map.erase(it);
				}
			}
	};
}