#include "MeshResource.hpp"
#include <Systems/Graphics/HighLevel/Mesh.hpp>
#include <Systems/Graphics/VertexFormats.hpp>

#define TINYOBJLOADER_IMPLEMENTATION
#include <../Include/tinyobjloader/tiny_obj_loader.h>

using namespace Magma;

MeshResource::~MeshResource() {
	SDELETE(m_Mesh);
}

template<class VertexType, class IndexType>
void gPrepareMesh(Mesh* inMesh, const std::vector<VertexType>& inVertices, const std::vector<IndexType>& inIndices, const std::vector<Vector3>& inPositions) {
	gAssert(inMesh != nullptr, "Expected a valid Mesh instance");
	inMesh->PrepareMeshData(&inVertices[0], (uint32_t)inVertices.size(), &inIndices[0], (uint32_t)inIndices.size(), EMeshFlags::NONE, inPositions);	
}

void MeshResource::Load() {
	tinyobj::attrib_t attrib;
	std::string error;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &error, m_ResourceFileName.c_str())) {
		throw IOException("Cannot load mesh, file not found!");
	}

	for (const auto& m : materials) {
		
	}

	std::unordered_map<VertexFormats::VertexP0N0UV0, uint32_t> uniqueVertices = {};

	bool hasNormals = attrib.normals.size() > 0;
	bool hasUVs = attrib.texcoords.size() > 0;
	
	m_Mesh = new Mesh(m_ResourceFileName);
	
	uint32_t shape_idx = 0;
	for (const auto& s : shapes) {
		
		std::vector<uint16_t> indices;
		std::vector<VertexFormats::VertexP0N0UV0> vertices;
		std::vector<Vector3> positions;

		for (const auto& idx : s.mesh.indices) {
			VertexFormats::VertexP0N0UV0 vtx;

			vtx.Position[0] = attrib.vertices[3 * idx.vertex_index + 0];
			vtx.Position[1] = attrib.vertices[3 * idx.vertex_index + 1];
			vtx.Position[2] = attrib.vertices[3 * idx.vertex_index + 2];

			positions.emplace_back(vtx.Position[0], vtx.Position[1], vtx.Position[2]);

			if (hasNormals) {
				vtx.Normal[0] = attrib.normals[3 * idx.normal_index + 0];
				vtx.Normal[1] = attrib.normals[3 * idx.normal_index + 1];
				vtx.Normal[2] = attrib.normals[3 * idx.normal_index + 2];
			}

			if (hasUVs) {
				vtx.UV[0] = attrib.texcoords[2 * idx.texcoord_index + 0];
				vtx.UV[1] = attrib.texcoords[2 * idx.texcoord_index + 1];
			}

			if (uniqueVertices.count(vtx) == 0) {
				uniqueVertices[vtx] = (uint32_t)vertices.size();
				vertices.emplace_back(vtx);
			}

			indices.emplace_back(uniqueVertices[vtx]);
		}

		if (shape_idx == 0) {
			gPrepareMesh(m_Mesh, vertices, indices, positions);
		}
		else {
			Mesh* submesh = m_Mesh->AddSubMesh(s.name);
			gPrepareMesh(submesh, vertices, indices, positions);
		}

		++shape_idx;
	}
}

void MeshResource::Unload() {
	SDELETE(m_Mesh);
}