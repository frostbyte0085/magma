#include "TextResource.hpp"

using namespace Magma;

TextResource::~TextResource() {

}

void TextResource::Load() {
	std::ifstream file(GetResourceFileName());
	assert(file.is_open());

	std::stringstream ss;
	ss << file.rdbuf();

	m_Contents = ss.str();

	file.close();
}

void TextResource::Unload() {
	m_Contents = "";
}