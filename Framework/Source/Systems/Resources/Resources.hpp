#pragma once

#include "../System.hpp"
#include "ResourceMap.hpp"
#include "Enumerations.hpp"

#include "TextResource.hpp"
#include "MeshResource.hpp"
#include "ShaderResource.hpp"

namespace Magma {
	class Resources : public System {
	public:
		Resources();
		~Resources();

		virtual void Begin() override;
		virtual void End() override;
		virtual void Update(float dt) override;

		virtual void Initialise() override;
		
		inline ResourceManager<TextResource>& GetDefaultTextResourceManager() { return m_TextResourceManager; }
		inline ResourceMap<TextResource>& GetDefaultTextResourceMap() { return m_TextResourceMap; }

		inline ResourceManager<MeshResource>& GetDefaultMeshResourceManager() { return m_MeshResourceManager; }
		inline ResourceMap<MeshResource>& GetDefaultMeshResourceMap() { return m_MeshResourceMap; }

		inline ResourceManager<ShaderResource>& GetDefaultShaderResourceManager() { return m_ShaderResourceManager; }
		inline ResourceMap<ShaderResource>& GetDefaultShaderResourceMap() { return m_ShaderResourceMap; }

	private:
		// text
		ResourceManager<TextResource> m_TextResourceManager;
		ResourceMap<TextResource> m_TextResourceMap;
		// mesh
		ResourceManager<MeshResource> m_MeshResourceManager;
		ResourceMap<MeshResource> m_MeshResourceMap;
		// shaders
		ResourceManager<ShaderResource> m_ShaderResourceManager;
		ResourceMap<ShaderResource> m_ShaderResourceMap;
	};
}