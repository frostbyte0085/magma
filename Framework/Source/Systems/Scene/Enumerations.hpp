#pragma once

namespace Magma {
	enum class ESpace {
		WORLD,
		LOCAL
	};

	class Scene;
	class SceneNode;
	class Component;
	class Transform;
	class CameraProvider;
	class RenderProvider;
	class Dummy;
}