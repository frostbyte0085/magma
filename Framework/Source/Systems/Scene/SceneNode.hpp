//
//  SceneNode.hpp
//  TerrainEngine
//
//  Created by Pantelis Lekakis on 07/02/2016.
//  Copyright © 2016 Pantelis Lekakis. All rights reserved.
//

#ifndef SceneNode_hpp
#define SceneNode_hpp

#include "../../Framework.h"
#include "Enumerations.hpp"

namespace Magma {
	class SceneNode {
	public:
		SceneNode(const std::string& name);
		virtual ~SceneNode();

		inline SceneNode* GetParent() const { return m_Parent; }
		void SetParent(SceneNode* otherNode);

		SceneNode* GetChild(uint32_t index) const;
		SceneNode* GetChild(const std::string& name) const;
		uint32_t GetChildCount() const { return (uint32_t)m_Children.size(); }

		const std::string& GetName() const { return m_Name; }

		// Adds the component and returns its instance.
		template <class T, class... Args>
		T* AddComponent(Args... args) {
			T* instance = new T(args...);
			Component* asComponent = dynamic_cast<Component*>(instance);
			assert(asComponent != nullptr);

			initialiseComponent(asComponent);
			return instance;
		}

		// Returns the first found component of this type on this SceneNode.
		template <class T>
		T* GetComponent() {
			for (auto c : m_Components) {
				T* asT = dynamic_cast<T*>(c);
				if (asT != nullptr) {
					return asT;
				}
			}

			return nullptr;
		}

		// Returns all the found components of this type on this SceneNode.
		template <class T>
		std::vector<T*> GetComponents() {
			std::vector<T*> foundComponents;

			for (auto c : m_Components) {
				T* asT = dynamic_cast<T*>(c);
				if (asT != nullptr) {
					foundComponents.push_back(asT);
				}
			}

			return std::move(foundComponents);
		}

		// Returns the first found component of this type, searches all the children and this SceneNode.
		template <class T>
		T* GetComponentInChildren() {
			auto allComponents = GetComponentsInChildren<T>();
			for (auto& c : allComponents) {
				T* asT = dynamic_cast<T*>(c);
				if (asT != nullptr) {
					return c;
				}
			}

			return nullptr;
		}

		// Returns all the found components of this type, searches all the children and this SceneNode.
		template <class T>
		std::vector<T*> GetComponentsInChildren() {
			std::vector<T*> foundComponents = GetComponents<T>();

			for (auto& child : m_Children) {
				auto childComponents = child->GetComponentsInChildren<T>();
				foundComponents.insert(foundComponents.end(), childComponents.begin(), childComponents.end());
			}

			return std::move(foundComponents);
		}

		// Returns the first found component of this type, searches all the parents and this SceneNode.
		template <class T>
		T* GetComponentInParent() {
			return nullptr;
		}

		// Returns all the found components of this type, searches all the parents and this SceneNode.
		template <class T>
		std::vector<T*> GetComponentsInParent() {
			std::vector<T*> foundComponents;

			return std::move(foundComponents);
		}

	private:
		void initialiseComponent(Component* component);

		std::string m_Name;
		std::vector<Component*> m_Components;

		SceneNode* m_Parent = nullptr;
		std::vector<SceneNode*> m_Children;
	};
}
#endif /* SceneNode_hpp */
