#include "MeshRenderProvider.hpp"
#include <Systems/Graphics/RenderPipelineSetup.hpp>
#include <Systems/Graphics/Shader.hpp>
#include <Systems/Graphics/HighLevel/Mesh.hpp>
#include <Systems/Graphics/HighLevel/DrawableSetupContext.hpp>
#include <Systems/Graphics/HighLevel/Renderer.hpp>
#include <Systems/Graphics/CommandList.hpp>
#include <Systems/Graphics/Buffer.hpp>
#include <Systems/Graphics/HighLevel/DrawableSetup.hpp>
#include <Application/Application.hpp>
#include "Transform.hpp"

using namespace Magma;

MeshRenderProvider::MeshRenderProvider() {

}

MeshRenderProvider::~MeshRenderProvider() {	
	SDELETE_ARRAY(m_Setups);
}

void MeshRenderProvider::rebuildDrawableArray() {
	SDELETE_ARRAY(m_Setups);
	m_SetupCount = 0;

	countDrawable(GetMesh());

	if (m_SetupCount > 0) {
		m_Setups = new DrawableSetup[m_SetupCount];
	}
}

void MeshRenderProvider::countDrawable(Mesh* const inMesh) {
	if (inMesh) {
		m_SetupCount++;

		if (inMesh->GetSubMeshCount() > 0) {
			for (uint32_t i = 0; i < inMesh->GetSubMeshCount(); ++i) {
				countDrawable(inMesh->GetSubMesh(i));
			}
		}
	}
}

void MeshRenderProvider::Initialise(SceneNode* inOwnerSceneNode) {
	RenderProvider::Initialise(inOwnerSceneNode);
		
}

void MeshRenderProvider::SetMesh(Mesh* const inMesh) {
	gAssert(inMesh != nullptr, "Expected a valid Mesh instance");
	m_Mesh = inMesh;

	rebuildDrawableArray();
}

void MeshRenderProvider::BuildContexts(const SceneView& inView) {
	ScopedLock<RecursiveMutex> lock(&m_Mutex);

	// cbuffer update
	Transform *t = GetComponent<Transform>();

	m_PerObjectData.WorldMatrix = t->GetWorldMatrix();

	uint32_t current_index = 0;
	buildDrawableSetupsForMesh(inView, gGetAvailableDrawableSetupContext(), current_index, GetMesh());
}

void MeshRenderProvider::Cull(const SceneView& inView) {
	ScopedLock<RecursiveMutex> lock(&m_Mutex);

	uint32_t current_index = 0;
	prepareAndCullDrawableSetupsForMesh(inView, current_index, GetMesh());
}

void MeshRenderProvider::prepareAndCullDrawableSetupsForMesh(const SceneView& inView, uint32_t& ioCurrentDrawableSetupIndex, Mesh* inMesh) {
	ScopedLock<RecursiveMutex> lock(&m_Mutex);

	if (inMesh) {
		DrawableSetup& setup = m_Setups[ioCurrentDrawableSetupIndex];

		// transform bounding volumes and store
		const Sphere& local_bounds = inMesh->GetLocalBoundingSphere();
		Sphere transformed_bounds = local_bounds.GetTransformed(m_PerObjectData.WorldMatrix);

		setup.SetLocalBoundingSphere(local_bounds);
		setup.SetWorldBoundingSphere(transformed_bounds);
		
		// test & cull
		if (!gIntersects(transformed_bounds, inView.GetFrustum(), nullptr)) {
			setup.SetCulled();
		}

		for (size_t i = 0; i < inMesh->GetSubMeshCount(); i++) {
			prepareAndCullDrawableSetupsForMesh(inView, ++ioCurrentDrawableSetupIndex, inMesh->GetSubMesh(i));
		}
	}
}

void MeshRenderProvider::buildDrawableSetupsForMesh(const SceneView& inView, DrawableSetupContext& ioContext, uint32_t& ioCurrentDrawableSetupIndex, Mesh* inMesh) {
	ScopedLock<RecursiveMutex> lock(&m_Mutex);

	if (inMesh) {
		
		DrawableSetup& setup = m_Setups[ioCurrentDrawableSetupIndex];
		if (!setup.IsCulled()) {
			setup.ResourceBindings->CB.clear();
			setup.ResourceBindings->Samplers.clear();
			setup.ResourceBindings->SRV.clear();
			setup.ResourceBindings->VB.clear();

			// shader & material properties
			setup.SetShader(GetShader());

			// update sort key		
			float distance_to_camera = Vector3::Distance(inView.GetCameraTransform()->GetWorldPosition(), setup.GetWorldBoundingSphere().GetCenter());

			setup.SetSortKey((uint32_t)distance_to_camera);

			// resources
			setup.SetPerObjectBufferData(&m_PerObjectData);

			setup.ResourceBindings->VB.emplace_back(inMesh->GetVertexBuffer(), inMesh->GetVertexDataStructureSize());
			setup.ResourceBindings->IB = IndexBufferBinding(inMesh->GetIndexBuffer());

			// draw topology
			setup.IndexedDraw = IndexedDrawDesc(inMesh->GetIndexCount(), 0, 0);
			
			ioContext.Add(&setup);
		}
		for (size_t i = 0; i < inMesh->GetSubMeshCount(); i++) {
			buildDrawableSetupsForMesh(inView, gGetAvailableDrawableSetupContext(), ++ioCurrentDrawableSetupIndex, inMesh->GetSubMesh(i));
		}
	}
}