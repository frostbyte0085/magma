//
//  Component.hpp
//  TerrainEngine
//
//  Created by Pantelis Lekakis on 11/02/2016.
//  Copyright © 2016 Pantelis Lekakis. All rights reserved.
//

#ifndef Component_hpp
#define Component_hpp

#include "../SceneNode.hpp"

namespace Magma {
	class Component {
		friend class SceneNode;
	public:
		Component(const Component& other) = delete;

		Component();
		virtual ~Component();

		void OnParentChanged(const SceneNode* oldParent, const SceneNode* newParent) {}

		template <class T> T* GetComponent() {	return m_OwnerSceneNode->GetComponent<T>();	}
		template <class T> std::vector<T*> GetComponents() { return std::move(m_OwnerSceneNode->GetComponents<T>()); }
		template <class T> T* GetComponentInChildren() { return m_OwnerSceneNode->GetComponentInChildren<T>(); }
		template <class T> std::vector<T*> GetComponentsInChildren() { return std::move(m_OwnerSceneNode->GetComponentsInChildren<T>()); }
		template <class T> T* GetComponentInParent() { return m_OwnerSceneNode->GetComponentInParent<T>(); }
		template <class T> std::vector<T*> GetComponentsInParent() { return std::move(m_OwnerSceneNode->GetComponentsInParent<T>()); }

	protected:
		virtual void Initialise(SceneNode* ownerSceneNode);
		SceneNode* m_OwnerSceneNode = nullptr;

	private:
	};
}

#endif /* Component_hpp */
