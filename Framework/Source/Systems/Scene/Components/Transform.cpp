//
//  Transform.cpp
//  TerrainEngine
//
//  Created by Pantelis Lekakis on 11/02/2016.
//  Copyright © 2016 Pantelis Lekakis. All rights reserved.
//

#include "Systems/Scene/Components/Transform.hpp"
#include "Systems/Scene/SceneNode.hpp"

using namespace Magma;

Transform::Transform() : m_IsWorldMatrixDirty(false), m_IsTranslationDirty(false), m_IsRotationDirty(false), m_IsScalingDirty(false), m_LocalScaling(Vector3(1,1,1)) {
    
}

Transform::~Transform() {
    
}

void Transform::SetPosition(const Vector3 &p, ESpace space) {

    if (space == ESpace::LOCAL) {
        m_LocalPosition = p;
    }
    else if (space == ESpace::WORLD) {
        auto parent = m_OwnerSceneNode->GetParent();
        if (parent != nullptr) {
            auto parentTransform = parent->GetComponent<Transform>();
            assert (parentTransform != nullptr);
            
			m_LocalPosition = WorldToLocalPosition(p);
        }
        else {
            m_LocalPosition = p;
        }
    }

	markDirtyTranslation();
}

void Transform::AddPosition(const Vector3 &p, ESpace space) {
    if (space == ESpace::LOCAL) {
        m_LocalPosition += (p * GetLocalRotation());
    }
    else if (space == ESpace::WORLD) {
        auto parent = m_OwnerSceneNode->GetParent();
        if (parent != nullptr) {
            auto parentTransform = parent->GetComponent<Transform>();
            assert (parentTransform != nullptr);
            
            m_LocalPosition += (p * parentTransform->GetWorldRotation().GetInverse()) / parentTransform->GetWorldScaling();
        }
        else {
            m_LocalPosition += p;
        }
    }
    
	markDirtyTranslation();
}

Vector3 Transform::GetWorldPosition() const {
        
    auto parent = m_OwnerSceneNode->GetParent();
    if (parent != nullptr) {
        auto parentTransform = parent->GetComponent<Transform>();
        assert (parentTransform != nullptr);
        
		return m_LocalPosition * parentTransform->GetWorldMatrix();
    }
    
    return m_LocalPosition;
}

Vector3 Transform::GetLocalPosition() const {
    
    auto parent = m_OwnerSceneNode->GetParent();
    if (parent != nullptr) {
        auto parentTransform = parent->GetComponent<Transform>();
        assert (parentTransform != nullptr);
        
		return m_LocalPosition * parentTransform->GetWorldMatrix().GetInverse();
    }
    
    return m_LocalPosition;
}

void Transform::SetRotation(const Quaternion &r, ESpace space) {
    
    if (space == ESpace::LOCAL) {
        m_LocalRotation = r;
    }
    else if (space == ESpace::WORLD) {
        auto parent = m_OwnerSceneNode->GetParent();
        if (parent != nullptr) {
            auto parentTransform = parent->GetComponent<Transform>();
            assert (parentTransform != nullptr);
            
            m_LocalRotation = WorldToLocalRotation(r);
        }
		else {
			m_LocalRotation = r;
		}
    }
    
	markDirtyRotation();
}

void Transform::AddRotation(const Quaternion &r, ESpace space) {
    Quaternion q = r.GetNormalized();
    
    if (space == ESpace::LOCAL) {
        m_LocalRotation = m_LocalRotation * q;
    }
    else if (space == ESpace::WORLD) {
		m_LocalRotation = m_LocalRotation * (GetWorldRotation().GetInverse()) * q * GetWorldRotation();
    }
    
	markDirtyRotation();
}

Quaternion Transform::GetWorldRotation() const {
	Quaternion worldRotation = m_LocalRotation;
    
    auto parent = m_OwnerSceneNode->GetParent();
    if (parent != nullptr) {
        auto parentTransform = parent->GetComponent<Transform>();
        assert (parentTransform != nullptr);
        worldRotation = parentTransform->GetWorldRotation() * worldRotation;
    }
    
    return worldRotation;
}

Quaternion Transform::GetLocalRotation() const {
    Quaternion localRotation = GetWorldRotation();
    
    auto parent = m_OwnerSceneNode->GetParent();
    if (parent != nullptr) {
        auto parentTransform = parent->GetComponent<Transform>();
        assert (parentTransform != nullptr);
        localRotation = parentTransform->GetWorldRotation().GetInverse() * localRotation;
    }
    
    return localRotation;
}

void Transform::SetScaling(const Vector3 &s) {
    
    m_LocalScaling = s;
    
	markDirtyScaling();
}

void Transform::AddScaling(const Vector3 &s) {
    m_LocalScaling += s;
    
	markDirtyScaling();
}

Vector3 Transform::GetWorldScaling() const {
    
    Vector3 worldScaling = m_LocalScaling;
    
    auto parent = m_OwnerSceneNode->GetParent();
    if (parent != nullptr) {
        auto parentTransform = parent->GetComponent<Transform>();
        assert (parentTransform != nullptr);
        worldScaling *= parentTransform->GetWorldScaling();
    }
    
    return worldScaling;
}

Vector3 Transform::GetLocalScaling() const {
    Vector3 localScaling = GetWorldScaling();
    
    auto parent = m_OwnerSceneNode->GetParent();
    if (parent != nullptr) {
        auto parentTransform = parent->GetComponent<Transform>();
        assert (parentTransform != nullptr);
        localScaling *=  (Vector3::one / parentTransform->GetWorldScaling());
    }
    
    return localScaling;
}

bool Transform::IsWorldDirty() const {
	auto parent = m_OwnerSceneNode->GetParent();
	if (parent != nullptr) {
		auto parentTransform = parent->GetComponent<Transform>();
		assert(parentTransform != nullptr);
		m_IsWorldMatrixDirty = m_IsWorldMatrixDirty || parentTransform->IsWorldDirty();
	}

	return m_IsWorldMatrixDirty;
}

bool Transform::IsTranslationDirty() const {
    
    auto parent = m_OwnerSceneNode->GetParent();
    if (parent != nullptr) {
        auto parentTransform = parent->GetComponent<Transform>();
        assert (parentTransform != nullptr);
        m_IsTranslationDirty = m_IsTranslationDirty || parentTransform->IsTranslationDirty();
    }
    
    return m_IsTranslationDirty;
}

bool Transform::IsRotationDirty() const {
    
    auto parent = m_OwnerSceneNode->GetParent();
    if (parent != nullptr) {
        auto parentTransform = parent->GetComponent<Transform>();
        assert (parentTransform != nullptr);
        m_IsRotationDirty = m_IsRotationDirty || parentTransform->IsRotationDirty();
    }
    
    return m_IsRotationDirty;
}

bool Transform::IsScalingDirty() const {
    
    auto parent = m_OwnerSceneNode->GetParent();
    if (parent != nullptr) {
        auto parentTransform = parent->GetComponent<Transform>();
        assert (parentTransform != nullptr);
        m_IsScalingDirty = m_IsScalingDirty || parentTransform->IsScalingDirty();
    }
    
    return m_IsScalingDirty;
}

Vector3 Transform::WorldToLocalPosition(const Vector3 &worldPosition) {
	auto parent = m_OwnerSceneNode->GetParent();
	if (parent != nullptr) {
		auto parentTransform = parent->GetComponent<Transform>();
		assert(parentTransform != nullptr);

		return (worldPosition * parentTransform->GetWorldMatrix().GetInverse()) * GetWorldScaling();
	}

	return worldPosition;
}

Vector3 Transform::LocalToWorldPosition(const Vector3 &localPosition) {
	auto parent = m_OwnerSceneNode->GetParent();
	if (parent != nullptr) {
		auto parentTransform = parent->GetComponent<Transform>();
		assert(parentTransform != nullptr);

		return (localPosition * parentTransform->GetWorldMatrix()) / GetWorldScaling();
	}

	return localPosition;
}

Quaternion Transform::WorldToLocalRotation(const Quaternion &worldRotation) {
	return (GetWorldRotation() * worldRotation).GetInverse();
}

Quaternion Transform::LocalToWorldRotation(const Quaternion &localRotation) {
    return GetWorldRotation() * localRotation;
}

Matrix4x4 Transform::GetWorldMatrix() {
    
    if (IsWorldDirty()) {
		m_WorldMatrix = GetScalingMatrix() * GetRotationMatrix() * GetTranslationMatrix();
		cleanWorldMatrix();
    }
    
    return m_WorldMatrix;
}

const Matrix4x4& Transform::GetTranslationMatrix() {
    if (IsTranslationDirty()) {
        m_TranslationMatrix = Matrix4x4::Translation(GetWorldPosition());
		markDirtyTranslation(true); // inform children that this matrix has changed
		cleanTranslationMatrix();
    }
    
    return m_TranslationMatrix;
}

const Matrix4x4& Transform::GetRotationMatrix() {
    if (IsRotationDirty()) {
		m_RotationMatrix = Matrix4x4::Rotation(GetWorldRotation());
		markDirtyRotation(true); // inform children that this matrix has changed
		cleanRotationMatrix();
    }
    
    return m_RotationMatrix;
}

const Matrix4x4& Transform::GetScalingMatrix() {
    if (IsScalingDirty()) {
        m_ScalingMatrix = Matrix4x4::Scale(GetWorldScaling());
		markDirtyScaling(true); // inform children that this matrix has changed
		cleanScalingMatrix();
    }
    
    return m_ScalingMatrix;
}

Vector3 Transform::GetWorldForward() const {
    return Vector3(0, 0, 1) * GetWorldRotation();
}

Vector3 Transform::GetWorldRight() const {
	return Vector3(1, 0, 0) * GetWorldRotation();
}

Vector3 Transform::GetWorldUp() const {
    return Vector3::Cross(GetWorldForward(), GetWorldRight());
}

void Transform::markDirtyRotation(bool childrenOnly) {
	for (uint32_t i = 0; i < m_OwnerSceneNode->GetChildCount(); i++) {
		auto t = m_OwnerSceneNode->GetChild(i)->GetComponent<Transform>();
		assert(t != nullptr);

		t->markDirtyRotation();
	}

	if (!childrenOnly) {
		m_IsRotationDirty = true;
		m_IsWorldMatrixDirty = true;
	}
}

void Transform::markDirtyTranslation(bool childrenOnly) {
	for (uint32_t i = 0; i < m_OwnerSceneNode->GetChildCount(); i++) {
		auto t = m_OwnerSceneNode->GetChild(i)->GetComponent<Transform>();
		assert(t != nullptr);

		t->markDirtyTranslation();
	}

	if (!childrenOnly) {
		m_IsTranslationDirty = true;
		m_IsWorldMatrixDirty = true;
	}
}

void Transform::markDirtyScaling(bool childrenOnly) {
	for (uint32_t i = 0; i < m_OwnerSceneNode->GetChildCount(); i++) {
		auto t = m_OwnerSceneNode->GetChild(i)->GetComponent<Transform>();
		assert(t != nullptr);

		t->markDirtyScaling();
	}

	if (!childrenOnly) {
		m_IsScalingDirty = true;
		m_IsWorldMatrixDirty = true;
	}
}