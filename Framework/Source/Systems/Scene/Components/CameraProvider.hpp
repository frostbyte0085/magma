#pragma once

#include "Component.hpp"
#include "../Enumerations.hpp"

namespace Magma {
	enum class ECameraProjection {
		Perspective,
		Orthographic
	};

	class CameraProvider : public Component {
	public:
		CameraProvider();
		CameraProvider(float fov, float aspectRatio, float nearClipPlane, float farClipPlane);
		~CameraProvider();

		void SetProjection(ECameraProjection projection) { m_Projection = projection; }
		ECameraProjection GetProjection() const;
		
		void SetHorizontalFOV(float fov) { m_hFOV = fov; }
		float GetHorizontalFOV() const { return m_hFOV; }

		void SetNearPlane(float nearPlane) { m_Near = nearPlane; }
		float GetNearPlane() const { return m_Near; }

		void SetFarPlane(float farPlane) { m_Far = farPlane; }
		float GetFarPlane() const { return m_Far; }
		
		void SetAspectRatio(float aspectRatio) { m_AspectRatio = aspectRatio; }
		float GetAspectRatio() const { return m_AspectRatio; }

	private:
		ECameraProjection m_Projection;
		float m_hFOV;
		float m_Near;
		float m_Far;
		float m_AspectRatio;
	};
}