#include "CameraProvider.hpp"

using namespace Magma;

CameraProvider::CameraProvider() : m_hFOV(60.0f), m_Projection(ECameraProjection::Perspective) {

}

CameraProvider::CameraProvider(float fov, float aspectRatio, float nearClipPlane, float farClipPlane) : m_hFOV(fov), m_AspectRatio(aspectRatio), m_Near(nearClipPlane), m_Far(farClipPlane) {

}

CameraProvider::~CameraProvider() {

}