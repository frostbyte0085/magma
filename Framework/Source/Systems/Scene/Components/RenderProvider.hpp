//
//  Renderer.hpp
//  TerrainEngine
//
//  Created by Pantelis Lekakis on 30/03/2016.
//  Copyright © 2016 Pantelis Lekakis. All rights reserved.
//

#ifndef Renderer_hpp
#define Renderer_hpp

#include "Component.hpp"
#include <Systems/Graphics/CommandList.hpp>

namespace Magma {
	class DrawableSetup;
	class DrawableSetupContext;

	class RenderProvider : public Component {
	public:
		RenderProvider();
		virtual ~RenderProvider();

		virtual void BuildContexts(const SceneView& inView) = 0;
		virtual void Cull(const SceneView& inView) = 0;

		DrawableSetup* const GetDrawableSetups() const { return m_Setups; }
		uint32_t GetDrawableSetupCount() const { return m_SetupCount; }

	protected:
		DrawableSetup* m_Setups = nullptr;
		uint32_t m_SetupCount = 0;

		RecursiveMutex m_Mutex;
	};
}
#endif /* Renderer_hpp */
