//
//  Component.cpp
//  TerrainEngine
//
//  Created by Pantelis Lekakis on 11/02/2016.
//  Copyright © 2016 Pantelis Lekakis. All rights reserved.
//

#include "Systems/Scene/Components/Component.hpp"

using namespace Magma;

Component::Component() {
    
}

Component::~Component() {
    
}

void Component::Initialise(SceneNode* ownerSceneNode) {
    m_OwnerSceneNode = ownerSceneNode;
}