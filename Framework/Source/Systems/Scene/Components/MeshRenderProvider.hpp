#pragma once

#include "RenderProvider.hpp"

namespace Magma {
	class Mesh;

	class MeshRenderProvider : public RenderProvider {
	public:
		MeshRenderProvider();
		~MeshRenderProvider();
		
		inline void SetShader(Shader* const inShader) { m_Shader = inShader; }
		inline Shader* const GetShader() const { return m_Shader; }

		void SetMesh(Mesh* const inMesh);
		inline Mesh* const GetMesh() const { return m_Mesh; }

		virtual void BuildContexts(const SceneView& inView);
		virtual void Cull(const SceneView& inView);

	protected:
		void prepareAndCullDrawableSetupsForMesh(const SceneView& inView, uint32_t& ioCurrentDrawableSetupIndex, Mesh* mesh);
		void buildDrawableSetupsForMesh(const SceneView& inView, class DrawableSetupContext& ioContext, uint32_t& ioCurrentDrawableSetupIndex, Mesh* mesh);

		virtual void Initialise(SceneNode* ownerSceneNode) override;

	private:
		void rebuildDrawableArray();
		void countDrawable(Mesh* const inMesh);

		PerObjectBuffer m_PerObjectData;
		Shader* m_Shader = nullptr;
		Mesh* m_Mesh = nullptr;
	};
}