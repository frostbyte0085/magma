//
//  Transform.hpp
//  TerrainEngine
//
//  Created by Pantelis Lekakis on 11/02/2016.
//  Copyright © 2016 Pantelis Lekakis. All rights reserved.
//

#ifndef Transform_hpp
#define Transform_hpp

#include "Component.hpp"
#include <Math/MathInclude.h>
#include "../Enumerations.hpp"

namespace Magma {
	
	class Transform : public Component {
	public:
		Transform();
		~Transform();

		void AddPosition(const Vector3& p, ESpace space);
		void SetPosition(const Vector3& p, ESpace space);
		Vector3 GetWorldPosition() const;
		Vector3 GetLocalPosition() const;

		void SetRotation(const Quaternion& r, ESpace space);
		void AddRotation(const Quaternion& r, ESpace space);
		Quaternion GetWorldRotation() const;
		Quaternion GetLocalRotation() const;

		void SetScaling(const Vector3& s);
		void AddScaling(const Vector3& s);
		Vector3 GetWorldScaling() const;
		Vector3 GetLocalScaling() const;

		const Matrix4x4& GetTranslationMatrix();
		const Matrix4x4& GetRotationMatrix();
		const Matrix4x4& GetScalingMatrix();
		Matrix4x4 GetWorldMatrix();

		Vector3 WorldToLocalPosition(const Vector3& worldPosition);
		Vector3 LocalToWorldPosition(const Vector3& localPosition);
		Quaternion WorldToLocalRotation(const Quaternion& worldRotation);
		Quaternion LocalToWorldRotation(const Quaternion& localRotation);

		Vector3 GetWorldForward() const;
		Vector3 GetWorldUp() const;
		Vector3 GetWorldRight() const;

	private:
		mutable bool m_IsTranslationDirty;
		mutable bool m_IsRotationDirty;
		mutable bool m_IsScalingDirty;

		mutable bool m_IsWorldMatrixDirty;

		bool IsTranslationDirty() const;
		bool IsRotationDirty() const;
		bool IsScalingDirty() const;
		bool IsWorldDirty() const;

		Vector3 m_LocalPosition;
		Quaternion m_LocalRotation;
		Vector3 m_LocalScaling;

		Matrix4x4 m_TranslationMatrix;
		Matrix4x4 m_RotationMatrix;
		Matrix4x4 m_ScalingMatrix;
		Matrix4x4 m_WorldMatrix;

		void markDirtyTranslation(bool childrenOnly = false);
		void markDirtyRotation(bool childrenOnly = false);
		void markDirtyScaling(bool childrenOnly = false);

		inline void cleanTranslationMatrix() { m_IsTranslationDirty = false; }
		inline void cleanRotationMatrix() { m_IsRotationDirty = false; }
		inline void cleanScalingMatrix() { m_IsScalingDirty = false; }
		inline void cleanWorldMatrix() { m_IsWorldMatrixDirty = false; }
	};
}
#endif /* Transform_hpp */
