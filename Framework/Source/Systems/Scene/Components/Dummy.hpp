//
//  Dummy.hpp
//  TerrainEngine
//
//  Created by Pantelis Lekakis on 11/02/2016.
//  Copyright © 2016 Pantelis Lekakis. All rights reserved.
//

#ifndef Dummy_hpp
#define Dummy_hpp

#include "Component.hpp"

namespace Magma {
	class Dummy : public Component {
	public:
		Dummy();
		~Dummy();

	private:

	};
}
#endif /* Dummy_hpp */
