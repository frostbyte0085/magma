//
//  Scene.cpp
//  TerrainEngine
//
//  Created by Pantelis Lekakis on 07/02/2016.
//  Copyright © 2016 Pantelis Lekakis. All rights reserved.
//

#include "ErrorHandling/Exceptions.hpp"
#include "Systems/Scene/Scene.hpp"
#include "Systems/Scene/SceneNode.hpp"
#include "Systems/Scene/Components/Transform.hpp"

using namespace Magma;

Scene::Scene() {
    
}

Scene::~Scene() {
    std::unordered_map<std::string, SceneNode*>::iterator it;
    
    for (it=m_Nodes.begin(); it!=m_Nodes.end(); it++) {
        SDELETE(it->second);
    }
    m_Nodes.clear();
}

void Scene::Initialise() {
    m_RootNode = new SceneNode("__engine_scene_root__");
    m_RootNode->AddComponent<Transform>();
}

void Scene::Begin() {
    
}

void Scene::End() {
    
}

void Scene::Update(float dt) {
    
}

SceneNode* Scene::AddSceneNode(const std::string &name, const std::string &parent) {
    if (GetSceneNode(name) != nullptr) {
        throw SceneException("SceneNode with name \"" + name + "\" already exists!");
    }
    
    SceneNode* node = new SceneNode(name);
    Transform* transform = node->AddComponent<Transform>();
    
    SceneNode* parentNode = GetSceneNode(parent);
    if (parentNode == nullptr) {
        node->SetParent(m_RootNode);
    }
    else {
        node->SetParent(parentNode);
    }
    
    m_Nodes[name] = node;
    return node;
}

void Scene::DeleteSceneNode(const std::string &name) {
    std::unordered_map<std::string, SceneNode*>::const_iterator it = m_Nodes.find(name);
	
	if (it != m_Nodes.end()) {
		deleteSceneNodeInternal(it->second);
		m_Nodes.erase(it);
	}    
}

void Scene::DeleteSceneNode(SceneNode* node) {
	assert(node);

	std::unordered_map<std::string, SceneNode*>::const_iterator it = m_Nodes.find(node->GetName());

	if (it != m_Nodes.end()) {
		deleteSceneNodeInternal(it->second);
		m_Nodes.erase(it);
	}
}

void Scene::deleteSceneNodeInternal(SceneNode* node) {
	assert(node && (node != m_RootNode));
	
	// go through the children and release them too
	if (node->GetChildCount() > 0) {
		for (uint32_t i = node->GetChildCount() - 1; i >= 0; --i) {
			DeleteSceneNode(node->GetChild(i));
		}
	}
	SDELETE(node);
}

SceneNode* Scene::GetSceneNode(const std::string &name) const {
    std::unordered_map<std::string, SceneNode*>::const_iterator it = m_Nodes.find(name);
    if (it != m_Nodes.end()) {
        return it->second;
    }
    return nullptr;
}