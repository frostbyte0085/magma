//
//  SceneNode.cpp
//  TerrainEngine
//
//  Created by Pantelis Lekakis on 07/02/2016.
//  Copyright © 2016 Pantelis Lekakis. All rights reserved.
//

#include "Systems/Scene/SceneNode.hpp"
#include "Systems/Scene/Components/Transform.hpp"

using namespace Magma;

SceneNode::SceneNode(const std::string& name) {
    m_Name = std::move(name);
}

SceneNode::~SceneNode() {
	m_Parent = nullptr;

	for (auto c : m_Components) {
		SDELETE(c);
	}
	m_Components.clear();

	m_Children.clear();
}

void SceneNode::initialiseComponent(Component* component) {
    
    Transform* asTransform = dynamic_cast<Transform*>(component);
    
    for (auto c: m_Components) {
        Transform* otherAsTransform = dynamic_cast<Transform*>(c);
        bool containsTransform = (asTransform != nullptr) && (otherAsTransform != nullptr);
        
        assert (!containsTransform);
    }
    
    component->Initialise(this);
    m_Components.emplace_back(component);
}

void SceneNode::SetParent(SceneNode* otherNode) {
    
    for (auto c: m_Components) {
        c->OnParentChanged(m_Parent, otherNode);
    }
    
    otherNode->m_Children.push_back(this);
    
    m_Parent = otherNode;
}

SceneNode* SceneNode::GetChild(uint32_t index) const {
	assert(index < (uint32_t)m_Children.size());
	return m_Children[index];
}

SceneNode* SceneNode::GetChild(const std::string& name) const {
	for (auto node : m_Children) {
		if (name == node->GetName()) return node;
	}

	return nullptr;
}