//
//  Scene.hpp
//  TerrainEngine
//
//  Created by Pantelis Lekakis on 07/02/2016.
//  Copyright © 2016 Pantelis Lekakis. All rights reserved.
//

#ifndef Scene_hpp
#define Scene_hpp

#include "../System.hpp"
#include "Enumerations.hpp"

namespace Magma {
	class Scene : public System {
	public:
		Scene();
		~Scene();

		virtual void Initialise() override;
		virtual void Update(float dt) override;
		virtual void Begin() override;
		virtual void End() override;

		// add a new SceneNode to this scene, if there is a parent provided and it exists it's parented under that, otherwise under root node.
		SceneNode* AddSceneNode(const std::string& name, const std::string& parent = "");

		// deletes a SceneNode.
		void DeleteSceneNode(const std::string& name);
		// deletes a SceneNode.
		void DeleteSceneNode(SceneNode* node);
		// returns a pointer to an existing SceneNode.
		SceneNode* GetSceneNode(const std::string& name) const;

		SceneNode* GetRootNode() const { return m_RootNode; }

		void SetMainCameraProvider(SceneNode* node) { assert(node); m_MainCameraProviderNode = node; }
		SceneNode* GetMainCameraProviderNode() const { return m_MainCameraProviderNode; }

	private:
		void deleteSceneNodeInternal(SceneNode* node);

		SceneNode* m_RootNode = nullptr;
		SceneNode* m_MainCameraProviderNode = nullptr;
		std::unordered_map<std::string, SceneNode*> m_Nodes;
	};
}
#endif /* Scene_hpp */
