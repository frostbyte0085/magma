#pragma once

#include <Utilities/ScratchAllocator.hpp>

#ifndef MAGMA_MEMORY_SIZES
#define MAGMA_MEMORY_SIZES

#define SCRATCH_MEMORY_SIZE 8192 * 1024

#endif

namespace Magma {
	enum EAllocationType {
		SCRATCH_MEMORY,
		LINEAR_MEMORY,
		OS_MANAGED
	};

	struct AllocatorMemoryStatistics {
		struct ScratchStatistics {
			float Total, Used, Free;
		}Scratch;		
	};

	extern void gInitialiseAllocators();
	extern void gFreeAllocators();

	extern ScratchAllocator* gScratchAlloc;

	extern void gGatherMemoryStats(AllocatorMemoryStatistics& outStats);
}