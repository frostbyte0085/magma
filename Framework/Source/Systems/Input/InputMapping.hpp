#pragma once

#include "Enumerations.hpp"

namespace Magma {

	struct InputData {
		uint8_t m_KeyboardState[256];
		DIMOUSESTATE2 m_MouseState;

		float m_MouseSensitivity = 0.2f;
	};

	class InputMapping {
	public:
		InputMapping(uint8_t keyCode, EInputDeviceType deviceType) : m_InputDeviceType(deviceType), m_KeyCode(keyCode) {}
		virtual ~InputMapping() {}

		virtual EInputMappingType GetInputMappingType() const = 0;
		virtual EInputEventType Update(InputData* const data) = 0;

		inline EInputDeviceType GetInputDeviceType() const { return m_InputDeviceType; }
		inline uint8_t GetKeyCode() const { return m_KeyCode; }

	protected:
		EInputDeviceType m_InputDeviceType;
		uint8_t m_KeyCode;
	};

	class KeyMapping : public InputMapping {
	public:
		KeyMapping(uint8_t keyCode, EInputDeviceType deviceType) : InputMapping(keyCode, deviceType) {}
		~KeyMapping() {}

		virtual EInputMappingType GetInputMappingType() const { return EInputMappingType::INPUT_TYPE_KEY; }
		virtual EInputEventType Update(InputData* const data) override {
			m_PreviousState = m_CurrentState;

			switch (GetInputDeviceType()) {
			case EInputDeviceType::INPUT_DEVICE_KEYBOARD:
				m_CurrentState = (uint32_t)(data->m_KeyboardState[m_KeyCode] & 0x80);
				break;

			case EInputDeviceType::INPUT_DEVICE_MOUSE:
				m_CurrentState = (uint32_t)(data->m_MouseState.rgbButtons[m_KeyCode-12] & 0x80);
				break;

			default:break;
			}

			uint32_t changes = m_CurrentState ^ m_PreviousState;
			uint32_t is_down = changes & m_CurrentState;
			uint32_t is_up = changes & (~m_CurrentState);

			if (is_down) {
				return EInputEventType::INPUT_EVENT_KEY_DOWN;
			}
			else if (is_up) {
				return EInputEventType::INPUT_EVENT_KEY_UP;
			}
			
			return EInputEventType::INPUT_EVENT_KEY_IDLE;
		}

	private:
		uint32_t m_CurrentState = 0, m_PreviousState = 0;
	};

	class AxisMapping : public InputMapping {
	public:
		AxisMapping(uint8_t keyCode, EInputDeviceType deviceType, float value) : InputMapping(keyCode, deviceType), m_TargetValue(value) {}
		~AxisMapping() {}

		inline virtual EInputMappingType GetInputMappingType() const { return EInputMappingType::INPUT_TYPE_AXIS; }
		virtual EInputEventType Update(InputData* const data) override {

			float v = 0.0f;

			switch (GetInputDeviceType()) {
			case EInputDeviceType::INPUT_DEVICE_KEYBOARD:
				v = (float)(((uint32_t)( (data->m_KeyboardState[m_KeyCode] & 0x80) > 0) ));
				break;

			case EInputDeviceType::INPUT_DEVICE_MOUSE:
				if (GetKeyCode() == (uint8_t)EInputAxisDirection::INPUT_AXIS_X) {
					v = (float)data->m_MouseState.lX;
				}
				else if (GetKeyCode() == (uint8_t)EInputAxisDirection::INPUT_AXIS_Y) {
					v = (float)data->m_MouseState.lY;
				}

				break;

			default:break;
			}

			m_CurrentValue = v * m_TargetValue * data->m_MouseSensitivity;
			return EInputEventType::INPUT_EVENT_KEY_IDLE;
		}

		inline float GetCurrentValue() const { return m_CurrentValue; }
		inline float GetTargetValue() const { return m_TargetValue; }

	private:
		float m_TargetValue;
		float m_CurrentValue = 0.0f;
	};
}