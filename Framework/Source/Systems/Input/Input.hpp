#pragma once

#include "../System.hpp"
#include "InputMapping.hpp"

namespace Magma {
	
	class Input : public System {
	public:
		Input();
		~Input();

		virtual void Begin() override;
		virtual void End() override;
		virtual void Update(float dt) override;

		virtual void Initialise() override;

		void Map(const std::string& name, InputMapping* mapping);
		void BindKey(const std::string& name, EInputEventType type, std::function<void()> f);
		void BindAxis(const std::string& name, std::function<void(float)> f);
		
		void TestEscapeDown();
		void TestEscapeUp();

		void TestAxis(float value);

		inline void SetMouseSensitivity(float v) { assert(v > 0.0f); m_InputData.m_MouseSensitivity = v; }
		inline float GetMouseSensitivity() const { return m_InputData.m_MouseSensitivity; }

	private:
		void setupDefaultMappings();
		void triggerKeyBinding(const std::string& name, EInputEventType type);
		void triggerAxisBinding(const std::string& name, float value);

		IDirectInput8* m_DirectInput = nullptr;
		IDirectInputDevice8* m_Keyboard = nullptr;
		IDirectInputDevice8* m_Mouse = nullptr;

		std::unordered_map<std::string, std::vector<KeyMapping*>> m_KeyMappings;
		std::unordered_map<std::string, std::vector<AxisMapping*>> m_AxisMappings;

		std::unordered_map<std::string, std::vector<std::function<void()>>> m_KeyBindings[(uint32_t)EInputEventType::INPUT_EVENT_MAX];
		std::unordered_map<std::string, std::vector<std::function<void(float)>>> m_AxisBindings;

		std::vector<std::string> m_ActiveKeys;

		InputData m_InputData;
	};
}