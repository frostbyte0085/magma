#pragma once

namespace Magma {
	enum class EInputMappingType {
		INPUT_TYPE_KEY,
		INPUT_TYPE_AXIS
	};

	enum class EInputDeviceType {
		INPUT_DEVICE_KEYBOARD,
		INPUT_DEVICE_MOUSE
	};

	enum class EInputEventType {
		INPUT_EVENT_KEY_UP = 0,
		INPUT_EVENT_KEY_DOWN = 1,
		INPUT_EVENT_MAX = 2,
		INPUT_EVENT_KEY_IDLE = 100
	};

	enum class EInputAxisDirection {
		INPUT_AXIS_X = 0,
		INPUT_AXIS_Y = 1
	};
}