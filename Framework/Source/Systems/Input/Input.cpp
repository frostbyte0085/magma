#include "Input.hpp"
#include <Systems/Window/Window.hpp>
#include <Application/Application.hpp>

using namespace Magma;

Input::Input() {
	ZeroMemory(&m_InputData, sizeof(InputData));
}

Input::~Input() {

	m_ActiveKeys.clear();

	for (uint32_t i = 0; i < (uint32_t)EInputEventType::INPUT_EVENT_MAX; ++i) {
		for (auto it : m_KeyBindings[i]) {
			it.second.clear();
		}
		m_KeyBindings[i].clear();
	}

	for (auto it : m_AxisBindings) {
		it.second.clear();
	}
	m_AxisBindings.clear();

	for (auto it : m_KeyMappings) {
		std::vector<KeyMapping*>& mapping_vector = it.second;
		for (auto v: mapping_vector) {
			SDELETE(v);
		}
		mapping_vector.clear();
	}
	m_KeyMappings.clear();

	for (auto it : m_AxisMappings) {
		std::vector<AxisMapping*>& mapping_vector = it.second;
		for (auto v : mapping_vector) {
			SDELETE(v);
		}
		mapping_vector.clear();
	}
	m_AxisMappings.clear();

	// Release the mouse.
	if (m_Mouse)
	{
		m_Mouse->Unacquire();
		m_Mouse->Release();		
	}
	m_Mouse = nullptr;

	// Release the keyboard.
	if (m_Keyboard)
	{
		m_Keyboard->Unacquire();
		m_Keyboard->Release();		
	}
	m_Keyboard = nullptr;

	// Release the main interface to direct input.
	SRELEASE(m_DirectInput);
}

void Input::Begin() {
	HRESULT hr;
	hr = m_Keyboard->GetDeviceState(sizeof(m_InputData.m_KeyboardState), (LPVOID)&(m_InputData.m_KeyboardState));
	if (FAILED(hr)) {
		// If the keyboard lost focus or was not acquired then try to get control back.
		if ((hr == DIERR_INPUTLOST) || (hr == DIERR_NOTACQUIRED))
		{
			m_Keyboard->Acquire();
		}
	}

	// Read the mouse device.
	hr = m_Mouse->GetDeviceState(sizeof(m_InputData.m_MouseState), (LPVOID)&(m_InputData.m_MouseState));
	if (FAILED(hr))
	{
		// If the mouse lost focus or was not acquired then try to get control back.
		if ((hr == DIERR_INPUTLOST) || (hr == DIERR_NOTACQUIRED))
		{
			m_Mouse->Acquire();
		}
	}
	
}

void Input::End() {

}

// updates the keys/axis in the mappings and fires the events
void Input::Update(float dt) {
	for (auto it : m_KeyMappings) {
		std::vector<KeyMapping*>& mapping_vector = it.second;

		EInputEventType last_type = EInputEventType::INPUT_EVENT_KEY_IDLE;
		for (auto v : mapping_vector) {
			assert(v);

			EInputEventType type = v->Update(&m_InputData);

			// duplicate detection - do not allow 2 keys of the same type to trigger the event in different frames
			// first check for key releases, in this case remove the key name from the active ones
			if (type == EInputEventType::INPUT_EVENT_KEY_UP) {
				std::vector<std::string>::iterator result = std::find(m_ActiveKeys.begin(), m_ActiveKeys.end(), it.first);
				if (result != m_ActiveKeys.end()) {
					m_ActiveKeys.erase(result);
				}
			}

			// now see if the event type would require an event binding trigger
			// do that only if there are no active keys in the list
			if ((type != EInputEventType::INPUT_EVENT_KEY_IDLE) && (last_type == EInputEventType::INPUT_EVENT_KEY_IDLE)) {

				std::vector<std::string>::iterator result = std::find(m_ActiveKeys.begin(), m_ActiveKeys.end(), it.first);
				if (result == m_ActiveKeys.end()) {
					last_type = type;
				}
			}

			// now see if the event type was "down", if so, add the key to the active list
			if (type == EInputEventType::INPUT_EVENT_KEY_DOWN) {
				m_ActiveKeys.push_back(it.first);
			}
		}

		// if we need to trigger an event binding, do it here
		if (last_type != EInputEventType::INPUT_EVENT_KEY_IDLE) {
			triggerKeyBinding(it.first, last_type);
		}
	}

	for (auto it : m_AxisMappings) {
		std::vector<AxisMapping*>& mapping_vector = it.second;

		float total_axis = 0.0f;

		for (auto v : mapping_vector) {
			assert(v);

			v->Update(&m_InputData);
			total_axis += v->GetCurrentValue();
		}

		triggerAxisBinding(it.first, total_axis);
	}
}

void Input::triggerKeyBinding(const std::string& name, EInputEventType type) {
	auto& bindings_map = m_KeyBindings[(uint32_t)type];
	auto it = bindings_map.find(name);
	if (it != bindings_map.end()) {
		for (auto f : it->second) {
			f();
		}
	}
}

void Input::triggerAxisBinding(const std::string& name, float value) {
	auto it = m_AxisBindings.find(name);
	if (it != m_AxisBindings.end()) {
		for (auto f : it->second) {
			f(value);
		}
	}
}

void Input::Initialise() {
	// initialise the devices
	if (FAILED(DirectInput8Create(gWnd->GetInstance(), DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_DirectInput, nullptr))) {
		throw InputException("Failed creating the main DirectInput object!");
	}

	const DWORD coop_mode = DISCL_FOREGROUND | DISCL_NONEXCLUSIVE;

	// KEYBOARD	
	if(FAILED(m_DirectInput->CreateDevice(GUID_SysKeyboard, &m_Keyboard, nullptr))) {
		throw InputException("Failed creating a keyboard device!");
	}

	// Set the data format.  In this case since it is a keyboard we can use the predefined data format.
	if (FAILED(m_Keyboard->SetDataFormat(&c_dfDIKeyboard))) {
		throw InputException("Failed setting the keyboard's data format!");
	}

	// Set the cooperative level of the keyboard to not share with other programs.
	// TODO: change this when fullscreen mode
	if (FAILED(m_Keyboard->SetCooperativeLevel(gWnd->GetHandle(), coop_mode))) {
		throw InputException("Failed setting the keyboard's cooperative level!");
	}

	if (FAILED(m_Keyboard->Acquire())) {
		throw InputException("Failed acquiring the keyboard!");
	}
	//

	// MOUSE
	// Initialize the direct input interface for the mouse.
	if (FAILED(m_DirectInput->CreateDevice(GUID_SysMouse, &m_Mouse, nullptr))) {
		throw InputException("Failed creating a mouse device!");
	}

	// Set the data format for the mouse using the pre-defined mouse data format.
	if (FAILED(m_Mouse->SetDataFormat(&c_dfDIMouse2))) {
		throw InputException("Failed setting the mouse's data format!");
	}

	if (FAILED(m_Mouse->SetCooperativeLevel(gWnd->GetHandle(), coop_mode))) {
		throw InputException("Failed setting the mouse's cooperative level!");
	}

	if (FAILED(m_Mouse->Acquire())) {
		throw InputException("Failed acquiring the mouse!");
	}

	// setup default key-mappings
	setupDefaultMappings();

	// and default settings
	SetMouseSensitivity(0.25f);
}

void Input::TestEscapeDown() {
	gTrace("escape down");
}

void Input::TestEscapeUp() {
	gTrace("escape up");
}

void Input::TestAxis(float value) {
	
}

void Input::setupDefaultMappings() {
	Map("quit", new KeyMapping(DIK_ESCAPE, EInputDeviceType::INPUT_DEVICE_KEYBOARD));
	Map("quit", new KeyMapping(DIMOFS_BUTTON0, EInputDeviceType::INPUT_DEVICE_MOUSE));

	Map("forward", new AxisMapping(DIK_W, EInputDeviceType::INPUT_DEVICE_KEYBOARD, 1.0f));
	Map("forward", new AxisMapping(DIK_S, EInputDeviceType::INPUT_DEVICE_KEYBOARD, -1.0f));
	Map("forward", new AxisMapping((uint8_t)EInputAxisDirection::INPUT_AXIS_X, EInputDeviceType::INPUT_DEVICE_MOUSE, 1.0f));

	BindKey("quit", EInputEventType::INPUT_EVENT_KEY_DOWN, std::bind(&Input::TestEscapeDown, this));
	BindKey("quit", EInputEventType::INPUT_EVENT_KEY_UP, std::bind(&Input::TestEscapeUp, this));

	BindAxis("forward", std::bind(&Input::TestAxis, this, std::placeholders::_1));
}

void Input::Map(const std::string& name, InputMapping* mapping) {
	assert(mapping);

	if (mapping->GetInputMappingType() == EInputMappingType::INPUT_TYPE_KEY) {
		auto it = m_KeyMappings.find(name);
		if (it != m_KeyMappings.end()) {
			std::vector<KeyMapping*>& mapping_vector = it->second;
			mapping_vector.push_back(dynamic_cast<KeyMapping*>(mapping));
		}
		else {
			m_KeyMappings[name].push_back(dynamic_cast<KeyMapping*>(mapping));
		}
	}
	else if (mapping->GetInputMappingType() == EInputMappingType::INPUT_TYPE_AXIS) {
		auto it = m_AxisMappings.find(name);
		if (it != m_AxisMappings.end()) {
			std::vector<AxisMapping*>& mapping_vector = it->second;
			mapping_vector.push_back(dynamic_cast<AxisMapping*>(mapping));
		}
		else {
			m_AxisMappings[name].push_back(dynamic_cast<AxisMapping*>(mapping));
		}
	}
}

void Input::BindKey(const std::string& name, EInputEventType type, std::function<void()> f) {
	// name must exist in the keymap!
	assert(m_KeyMappings.find(name) != m_KeyMappings.end());

	auto& bindings_map = m_KeyBindings[(uint32_t)type];

	auto it = bindings_map.find(name);
	if (it != bindings_map.end()) {
		std::vector<std::function<void()>>& binding_vector = it->second;
		binding_vector.push_back(f);
	}
	else {
		bindings_map[name].push_back(f);
	}
}

void Input::BindAxis(const std::string& name, std::function<void(float)> f) {
	// name must exist in the keymap!
	assert(m_AxisMappings.find(name) != m_AxisMappings.end());

	auto it = m_AxisBindings.find(name);
	if (it != m_AxisBindings.end()) {
		std::vector<std::function<void(float)>>& binding_vector = it->second;
		binding_vector.push_back(f);
	}
	else {
		m_AxisBindings[name].push_back(f);
	}
}