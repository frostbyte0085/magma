#include "Shader.hpp"
#include "../../Application/Application.hpp"

using namespace Magma;

const std::string Shader::sEntryPoint = "main";
const std::string Shader::sTargetProfile = "5_0";
const std::string Shader::sNames[(uint32_t)EShaderType::SHADER_NUM_TYPES] = { "vs", "ps", "gs", "ds", "hs", "cs" };

namespace Magma {
	uint32_t gGetShaderIndexForType(EShaderType type) {
		uint32_t idx = static_cast<uint32_t>(log2f((float)type));
		return idx;
	}
}

Shader::Shader() {

}

Shader::~Shader() {
	for (auto& s : m_Instances) {
		SRELEASE(s.Code);
		SRELEASE(s.Shader);
	}

	for (auto it : m_BindingHandles) {
		for (auto handle : it.second) {
			SDELETE(handle);
		}
		it.second.clear();
	}
	m_BindingHandles.clear();

	SRELEASE(m_Layout);
}

std::vector<D3D11_INPUT_ELEMENT_DESC> Shader::getInputLayoutDescription(ID3DBlob** vsBlobOut) {
	const ShaderInstance& vs_instance = GetShaderInstance(EShaderType::SHADER_TYPE_VERTEX);
	
	// Reflect shader info
	ID3D11ShaderReflection* vs_reflection = nullptr;
	if (FAILED(D3DReflect(vs_instance.Code->GetBufferPointer(), vs_instance.Code->GetBufferSize(), __uuidof(ID3D11ShaderReflection), (void**)&vs_reflection)))
	{
		throw GraphicsException("Cannot read vertex shader for input layout creation!");
	}

	// Get shader info
	D3D11_SHADER_DESC shader_desc;
	vs_reflection->GetDesc(&shader_desc);
	
	// Read input layout description from shader info
	std::vector<D3D11_INPUT_ELEMENT_DESC> il_desc;
	for (uint32_t i = 0; i< shader_desc.InputParameters; i++)
	{
		D3D11_SIGNATURE_PARAMETER_DESC param_desc;
		vs_reflection->GetInputParameterDesc(i, &param_desc);

		// fill out input element desc
		D3D11_INPUT_ELEMENT_DESC element_desc;
		element_desc.SemanticName = param_desc.SemanticName;
		element_desc.SemanticIndex = param_desc.SemanticIndex;
		element_desc.InputSlot = 0;
		element_desc.AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
		element_desc.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
		element_desc.InstanceDataStepRate = 0;

		// determine DXGI format
		if (param_desc.Mask == 1)
		{
			if (param_desc.ComponentType == D3D_REGISTER_COMPONENT_UINT32) element_desc.Format = DXGI_FORMAT_R32_UINT;
			else if (param_desc.ComponentType == D3D_REGISTER_COMPONENT_SINT32) element_desc.Format = DXGI_FORMAT_R32_SINT;
			else if (param_desc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32) element_desc.Format = DXGI_FORMAT_R32_FLOAT;
		}
		else if (param_desc.Mask <= 3)
		{
			if (param_desc.ComponentType == D3D_REGISTER_COMPONENT_UINT32) element_desc.Format = DXGI_FORMAT_R32G32_UINT;
			else if (param_desc.ComponentType == D3D_REGISTER_COMPONENT_SINT32) element_desc.Format = DXGI_FORMAT_R32G32_SINT;
			else if (param_desc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32) element_desc.Format = DXGI_FORMAT_R32G32_FLOAT;
		}
		else if (param_desc.Mask <= 7)
		{
			if (param_desc.ComponentType == D3D_REGISTER_COMPONENT_UINT32) element_desc.Format = DXGI_FORMAT_R32G32B32_UINT;
			else if (param_desc.ComponentType == D3D_REGISTER_COMPONENT_SINT32) element_desc.Format = DXGI_FORMAT_R32G32B32_SINT;
			else if (param_desc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32) element_desc.Format = DXGI_FORMAT_R32G32B32_FLOAT;
		}
		else if (param_desc.Mask <= 15)
		{
			if (param_desc.ComponentType == D3D_REGISTER_COMPONENT_UINT32) element_desc.Format = DXGI_FORMAT_R32G32B32A32_UINT;
			else if (param_desc.ComponentType == D3D_REGISTER_COMPONENT_SINT32) element_desc.Format = DXGI_FORMAT_R32G32B32A32_SINT;
			else if (param_desc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32) element_desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		}

		//save element desc
		il_desc.push_back(element_desc);
	}

	SRELEASE(vs_reflection);

	assert(il_desc.size() > 0);

	*vsBlobOut = vs_instance.Code;
		
	return std::move(il_desc);
}

void Shader::Reflect() {
	std::vector<ShaderBindingHandle*> empty_bindings;
	m_BindingHandles[D3D_SIT_TEXTURE] = empty_bindings;
	m_BindingHandles[D3D_SIT_SAMPLER] = empty_bindings;
	m_BindingHandles[D3D_SIT_CBUFFER] = empty_bindings;

	for (uint32_t i = 0; i < (uint32_t)EShaderType::SHADER_NUM_TYPES; i++) {
		EShaderType type = (EShaderType)(1 << i);
		
		const ShaderInstance& instance = GetShaderInstance(type);
		if (instance.Shader) {
			ID3D11ShaderReflection *reflection = nullptr;
			gAssertD3D(D3DReflect(instance.Code->GetBufferPointer(), instance.Code->GetBufferSize(), __uuidof(ID3D11ShaderReflection), (void**)&reflection), "Failed reflecting shader!");
			
			D3D11_SHADER_DESC shader_desc;
			reflection->GetDesc(&shader_desc);

			D3D11_SHADER_INPUT_BIND_DESC binding_desc;
			for (uint32_t res_index = 0; res_index < shader_desc.BoundResources; res_index++) {
				reflection->GetResourceBindingDesc(res_index, &binding_desc);

				std::vector<ShaderBindingHandle*>& bindings = m_BindingHandles[binding_desc.Type];

				ShaderBindingHandle* binding_handle = new ShaderBindingHandle();

				ShaderBindingHandleData& binding_data = binding_handle->m_Data[i];
				binding_data.Slot = binding_desc.BindPoint;
				binding_data.SlotCount = binding_desc.BindCount;
				binding_data.Dimension = binding_desc.Dimension;
				binding_data.IsValid = true;

				binding_handle->m_Name = binding_desc.Name;

				bindings.push_back(binding_handle);				
			}

			SRELEASE(reflection);
		}
	}
}

ShaderBindingHandle* const Shader::GetBindingHandle(const std::string& inName, D3D_SHADER_INPUT_TYPE inType) const {	
	auto it = m_BindingHandles.find(inType);

	gAssert(it != m_BindingHandles.end(), "Invalid shader input type!");

	for (const auto binding : it->second) {
		if (binding->GetName() == inName) {
			return binding;
		}
	}

	return nullptr;
}