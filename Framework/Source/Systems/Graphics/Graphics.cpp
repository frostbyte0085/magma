//
//  Graphics.cpp
//  TerrainEngine
//
//  Created by Pantelis Lekakis on 07/02/2016.
//  Copyright © 2016 Pantelis Lekakis. All rights reserved.
//

#include <Systems/Resources/Resources.hpp>
#include <Systems/Resources/ShaderResource.hpp>
#include <Application/Application.hpp>

#include "Graphics.hpp"
#include "Texture.hpp"
#include "RenderPipelineSetup.hpp"
#include "SamplerState.hpp"
#include "RenderTargetView.hpp"
#include "DepthStencilView.hpp"
#include "ShaderResourceView.hpp"
#include "UnorderedAccessView.hpp"
#include "Viewport.hpp"
#include "Buffer.hpp"
#include "Shader.hpp"
#include "../../Math/Color.h"
#include "DefaultResources.hpp"
#include "ResourceContainer.hpp"
#include "Bindings.hpp"
#include "HighLevel/Renderer.hpp"
#include "CommandList.hpp"

using namespace Magma;

Graphics::Graphics() {

}

Graphics::~Graphics() {	
	gDestroyDefaultGfxResources();
	gFreeRenderPipelineSetups();

	SDELETE(gRenderer);

	m_Swapchain->SetFullscreenState(FALSE, nullptr);

	SDELETE(m_Viewport);
	SDELETE(m_BackbufferTarget);
	SRELEASE(m_Swapchain);

	SRELEASE(m_Context);
	SRELEASE(m_Device);
}

void Graphics::Initialise() {
	createDevice();

	gRenderer = new Renderer;
	gRenderer->Initialise();

	postInitialize();
}

void Graphics::Update(float dt) {
	
}

void Graphics::Begin() {
	assert(m_Context);

	updateViewport();

	SetMRT(m_BackbufferTarget);

	RenderTargetClear backbuffer_clear;
	backbuffer_clear.ClearColor[0] = Color(0.5f, 0.5f, 0.5f, 1.0f);
	ClearMRT(EClearFlag::CLEAR_COLOR | EClearFlag::CLEAR_DEPTH | EClearFlag::CLEAR_STENCIL, backbuffer_clear, m_BackbufferTarget);
	
	gRenderer->Begin();
}

void Graphics::End() {
	assert(m_Swapchain);

	gRenderer->End();
	m_Swapchain->Present(0, 0);
}

void Graphics::postInitialize() {
	// load the screenwipe shaders used by the renderer for the debug view
	ShaderResource* albedo = gResources->GetDefaultShaderResourceMap().Add("ScreenWipe_Albedo", "Assets\\Shaders\\ScreenWipe_Albedo.hlsl", ERenderTechnique::TECHNIQUE_FULLSCREEN_QUAD);
	ShaderResource* metalness = gResources->GetDefaultShaderResourceMap().Add("ScreenWipe_Metalness", "Assets\\Shaders\\ScreenWipe_Metalness.hlsl", ERenderTechnique::TECHNIQUE_FULLSCREEN_QUAD);
	ShaderResource* roughness = gResources->GetDefaultShaderResourceMap().Add("ScreenWipe_Roughness", "Assets\\Shaders\\ScreenWipe_Roughness.hlsl", ERenderTechnique::TECHNIQUE_FULLSCREEN_QUAD);
	ShaderResource* normals = gResources->GetDefaultShaderResourceMap().Add("ScreenWipe_Normals", "Assets\\Shaders\\ScreenWipe_Normals.hlsl", ERenderTechnique::TECHNIQUE_FULLSCREEN_QUAD);
	
	std::vector<Shader*> screenwipe_shaders;
	screenwipe_shaders.push_back(albedo->GetShader());
	screenwipe_shaders.push_back(metalness->GetShader());
	screenwipe_shaders.push_back(roughness->GetShader());
	screenwipe_shaders.push_back(normals->GetShader());

	gRenderer->UpdateScreenWipeShaders(&screenwipe_shaders[0]);
}

void Graphics::getClientSize() {

}

void Graphics::createDevice() {
	DXGI_SWAP_CHAIN_DESC desc;
	ZeroMemory(&desc, sizeof(desc));

	desc.BufferCount = kSwapChainBufferCount;
	desc.BufferDesc.Format = kBackbufferFormat;
	desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	desc.OutputWindow = GetActiveWindow();
	desc.SampleDesc.Count = 1;
	desc.Windowed = TRUE;

	const D3D_FEATURE_LEVEL levels[] = { D3D_FEATURE_LEVEL_11_1, D3D_FEATURE_LEVEL_11_0 };

	UINT creation_flags = 0;
#if defined(_DEBUG)
	// If the project is in a debug build, enable the debug layer.
	creation_flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	if (FAILED(D3D11CreateDeviceAndSwapChain(NULL,
		D3D_DRIVER_TYPE_HARDWARE,
		NULL,
		creation_flags,
		levels,
		2,
		D3D11_SDK_VERSION,
		&desc,
		&m_Swapchain,
		&m_Device,
		NULL,
		&m_Context))) {

		throw GraphicsException("Failed creating a device and swapchain!");
	}
	m_Swapchain->GetDesc(&desc);

	CreateViewport(0, 0, (float)desc.BufferDesc.Width, (float)desc.BufferDesc.Height, 0.0f, 1.0f, &m_Viewport);

	gCreateDefaultGfxResources();

	Texture2D *backbuffer_texture = nullptr;
	CreateTexture2DFromCurrentSwapChain(0, &backbuffer_texture);
	
	m_BackbufferTarget = new RenderTarget();
	m_BackbufferTarget->Initialise(1, desc.BufferDesc.Width, desc.BufferDesc.Height);
	m_BackbufferTarget->SetColorTarget(0, backbuffer_texture);
	m_BackbufferTarget->SetDepthTarget(DXGI_FORMAT_R24G8_TYPELESS);

	SDELETE(backbuffer_texture);
}

void Graphics::updateViewport() {
	DXGI_SWAP_CHAIN_DESC desc;
	m_Swapchain->GetDesc(&desc);
	
	m_Viewport->Set(0.0f, 0.0f, (float)desc.BufferDesc.Width, (float)desc.BufferDesc.Height, 0.0f, 1.0f);
	SetViewports(1, &m_Viewport);
}

//
// D3D11 Creation functions
//
void Graphics::CreateTexture1D(const D3D11_TEXTURE1D_DESC& desc, const D3D11_SUBRESOURCE_DATA* initialData, Texture1D** texOut) {
	assert(m_Device);

	ID3D11Texture1D* d3d_tex = nullptr;
	if (FAILED(m_Device->CreateTexture1D(&desc, initialData, &d3d_tex))) {
		throw GraphicsException("Failed creating a texture1d!");
	}

	*texOut = new Texture1D(d3d_tex);
}

void Graphics::CreateTexture2D(const D3D11_TEXTURE2D_DESC& desc, const D3D11_SUBRESOURCE_DATA* initialData, Texture2D** texOut) {
	assert(m_Device);

	ID3D11Texture2D* d3d_tex = nullptr;
	if (FAILED(m_Device->CreateTexture2D(&desc, initialData, &d3d_tex))) {
		throw GraphicsException("Failed creating a texture2d!");
	}

	*texOut = new Texture2D(d3d_tex);
}

void Graphics::CreateTexture2DFromCurrentSwapChain(uint32_t bufferIndex, Texture2D** texOut) {
	assert(m_Swapchain && (bufferIndex >= 0));
	ID3D11Texture2D* d3d_tex = nullptr;
	m_Swapchain->GetBuffer(bufferIndex, __uuidof(ID3D11Texture2D), (LPVOID*)&d3d_tex);

	*texOut = new Texture2D(d3d_tex);
}

void Graphics::CreateTexture3D(const D3D11_TEXTURE3D_DESC& desc, const D3D11_SUBRESOURCE_DATA* initialData, Texture3D** texOut) {
	assert(m_Device);

	ID3D11Texture3D* d3d_tex = nullptr;
	if (FAILED(m_Device->CreateTexture3D(&desc, initialData, &d3d_tex))) {
		throw GraphicsException("Failed creating a texture1d!");
	}

	*texOut = new Texture3D(d3d_tex);
}

void Graphics::CreateViewport(float x, float y, float width, float height, float minZ, float maxZ, Viewport** viewportOut) {
	*viewportOut = new Viewport;
	(*viewportOut)->Set(x, y, width, height, minZ, maxZ);
}

void Graphics::CreateBuffer(const D3D11_BUFFER_DESC& desc, const D3D11_SUBRESOURCE_DATA* initialData, Buffer** bufferOut) {
	assert(m_Device);

	ID3D11Buffer* d3d_buffer = nullptr;
	if (FAILED(m_Device->CreateBuffer(&desc, initialData, &d3d_buffer))) {
		throw GraphicsException("Failed creating a buffer!");
	}

	*bufferOut = new Buffer(d3d_buffer);
}

void Graphics::CreateSamplerState(const D3D11_SAMPLER_DESC& desc, SamplerState** samplerStateOut) {
	assert(m_Device);

	ID3D11SamplerState* d3d_state = nullptr;
	if (FAILED(m_Device->CreateSamplerState(&desc, &d3d_state))) {
		throw GraphicsException("Failed creating a sampler state!");
	}

	*samplerStateOut = new SamplerState(d3d_state);
}

void Graphics::CreateUnorderedAccessView(Buffer* buffer, uint32_t numElementsInBuffer, bool isAppendBuffer, const DXGI_FORMAT typedBufferFmt, UnorderedAccessView** viewOut) {
	assert(m_Device);
	assert(buffer && (numElementsInBuffer > 0));

	ID3D11Buffer* resource = buffer->m_Buffer;

	ID3D11UnorderedAccessView* d3d_view = nullptr;

	D3D11_UNORDERED_ACCESS_VIEW_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	
	uint32_t flags = 0;
	if (isAppendBuffer) {
		flags |= D3D11_BUFFER_UAV_FLAG_APPEND;
	}

	desc.Format = typedBufferFmt;
	desc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	desc.Buffer.FirstElement = 0;
	desc.Buffer.Flags = flags;
	desc.Buffer.NumElements = numElementsInBuffer;

	if (FAILED(m_Device->CreateUnorderedAccessView(resource, &desc, &d3d_view))) {
		throw GraphicsException("Failed creating an unordered access view!");
	}

	*viewOut = new UnorderedAccessView(d3d_view);
}

void Graphics::CreateUnorderedAccessView(Texture* texture, const DXGI_FORMAT fmt, UnorderedAccessView** viewOut) {
	assert(m_Device);
	assert(texture);

	ID3D11Resource* resource = texture->m_Resource;

	D3D11_UNORDERED_ACCESS_VIEW_DESC desc;
	ZeroMemory(&desc, sizeof(desc));

	if (texture->IsTypeless()) {
		D3D11_RESOURCE_DIMENSION dimension;
		resource->GetType(&dimension);
		assert(dimension != D3D11_RESOURCE_DIMENSION_UNKNOWN);

		DXGI_FORMAT viewFormat = fmt;

		if (dimension == D3D11_RESOURCE_DIMENSION_TEXTURE1D) {
			desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE1D;
			desc.Texture1D.MipSlice = 0;

			if (viewFormat == DXGI_FORMAT_UNKNOWN) {
				viewFormat = texture->GetFormat();
			}
		}
		else if (dimension == D3D11_RESOURCE_DIMENSION_TEXTURE2D) {
			desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
			desc.Texture2D.MipSlice = 0;

			if (viewFormat == DXGI_FORMAT_UNKNOWN) {
				viewFormat = texture->GetFormat();
			}
		}
		else if (dimension == D3D11_RESOURCE_DIMENSION_TEXTURE3D) {
			desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE3D;
			desc.Texture3D.MipSlice = 0;

			if (viewFormat == DXGI_FORMAT_UNKNOWN) {
				viewFormat = texture->GetFormat();
			}
		}
		// TODO: Handle cubemap

		desc.Format = viewFormat;
	}
	
	ID3D11UnorderedAccessView* d3d_view = nullptr;
	if (FAILED(m_Device->CreateUnorderedAccessView(resource, (texture->IsTypeless() ? &desc : nullptr), &d3d_view))) {
		throw GraphicsException("Failed creating an unordered access view!");
	}

	*viewOut = new UnorderedAccessView(d3d_view);
}

void Graphics::CreateShaderResourceView(Buffer* buffer, uint32_t numElementsInBuffer, ShaderResourceView** viewOut) {
	assert(m_Device);
	assert(buffer && (numElementsInBuffer > 0));

	ID3D11Buffer* resource = buffer->m_Buffer;

	ID3D11ShaderResourceView* d3d_view = nullptr;

	D3D11_SHADER_RESOURCE_VIEW_DESC desc;
	ZeroMemory(&desc, sizeof(desc));

	desc.Format = DXGI_FORMAT_UNKNOWN;
	desc.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
	desc.BufferEx.FirstElement = 0;
	desc.BufferEx.Flags = 0;
	desc.BufferEx.NumElements = numElementsInBuffer;

	gAssertD3D(m_Device->CreateShaderResourceView(resource, &desc, &d3d_view), "Failed creating a shader resource view!");

	*viewOut = new ShaderResourceView(d3d_view);
}

void Graphics::CreateShaderResourceView(Texture* texture, const DXGI_FORMAT fmt, ShaderResourceView** viewOut) {
	assert(m_Device);
	assert(texture);

	ID3D11Resource* resource = texture->m_Resource;

	D3D11_SHADER_RESOURCE_VIEW_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	
	if (texture->IsTypeless()) {
		D3D11_RESOURCE_DIMENSION dimension;
		resource->GetType(&dimension);
		assert(dimension != D3D11_RESOURCE_DIMENSION_UNKNOWN);
		
		DXGI_FORMAT view_format = fmt;

		bool set_mip_data = false;
		uint32_t most_detailed_mip = 0;
		int32_t mip_levels = 0;

		switch (view_format) {
		case DXGI_FORMAT_R24_UNORM_X8_TYPELESS:
			most_detailed_mip = 0;
			mip_levels = 1;
			set_mip_data = true;
			break;

		default: break;
		}

		if (dimension == D3D11_RESOURCE_DIMENSION_TEXTURE1D) {
			desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE1D;

			if (view_format == DXGI_FORMAT_UNKNOWN) {
				view_format = texture->GetFormat();
			}

			if (set_mip_data) {
				desc.Texture1D.MipLevels = mip_levels;
				desc.Texture1D.MostDetailedMip = most_detailed_mip;
			}
		}
		else if (dimension == D3D11_RESOURCE_DIMENSION_TEXTURE2D) {
			desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;

			if (view_format == DXGI_FORMAT_UNKNOWN) {
				view_format = texture->GetFormat();
			}
			
			if (set_mip_data) {
				desc.Texture2D.MipLevels = mip_levels;
				desc.Texture2D.MostDetailedMip = most_detailed_mip;
			}
		}
		else if (dimension == D3D11_RESOURCE_DIMENSION_TEXTURE3D) {
			desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE3D;

			if (view_format == DXGI_FORMAT_UNKNOWN) {
				view_format = texture->GetFormat();
			}

			if (set_mip_data) {
				desc.Texture3D.MipLevels = mip_levels;
				desc.Texture3D.MostDetailedMip = most_detailed_mip;
			}
		}

		// TODO: Handle cubemap
		
		desc.Format = view_format;
	}

	ID3D11ShaderResourceView* d3d_view = nullptr;
	gAssertD3D(m_Device->CreateShaderResourceView(resource, (texture->IsTypeless() ? &desc : nullptr), &d3d_view), "Failed creating a shader resource view!");

	*viewOut = new ShaderResourceView(d3d_view);
}

void Graphics::CreateRenderTargetView(Texture* texture, const DXGI_FORMAT fmt, RenderTargetView** viewOut) {
	assert(m_Device);
	assert(texture);

	ID3D11Resource* resource = texture->m_Resource;

	D3D11_RENDER_TARGET_VIEW_DESC desc;
	ZeroMemory(&desc, sizeof(desc));

	// handle typeless resources
	if (texture->IsTypeless()) {
		D3D11_RESOURCE_DIMENSION dimension;
		resource->GetType(&dimension);
		assert(dimension != D3D11_RESOURCE_DIMENSION_UNKNOWN);

		DXGI_FORMAT viewFormat = fmt;

		if (dimension == D3D11_RESOURCE_DIMENSION_TEXTURE1D) {
			desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE1D;

			if (viewFormat == DXGI_FORMAT_UNKNOWN) {
				viewFormat = texture->GetFormat();
			}
		}
		else if (dimension == D3D11_RESOURCE_DIMENSION_TEXTURE2D) {
			desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;

			if (viewFormat == DXGI_FORMAT_UNKNOWN) {
				viewFormat = texture->GetFormat();
			}
		}
		else if (dimension == D3D11_RESOURCE_DIMENSION_TEXTURE3D) {
			desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE3D;

			if (viewFormat == DXGI_FORMAT_UNKNOWN) {
				viewFormat = texture->GetFormat();
			}
		}

		desc.Format = viewFormat;
	}

	ID3D11RenderTargetView *d3d_view = nullptr;
	if (FAILED(m_Device->CreateRenderTargetView(resource, texture->IsTypeless() ? &desc : nullptr, &d3d_view))) {
		throw GraphicsException("Failed creating a render target view!");
	}

	*viewOut = new RenderTargetView(d3d_view);	
}

void Graphics::CreateDepthStencilView(Texture* texture, const DXGI_FORMAT fmt, bool inReadOnly, DepthStencilView** viewOut) {
	assert(m_Device);
	assert(texture);

	ID3D11Resource* resource = texture->m_Resource;

	D3D11_DEPTH_STENCIL_VIEW_DESC desc;
	ZeroMemory(&desc, sizeof(desc));

	if (texture->IsTypeless()) {
		D3D11_RESOURCE_DIMENSION dimension;
		resource->GetType(&dimension);
		assert(dimension != D3D11_RESOURCE_DIMENSION_UNKNOWN);

		if (dimension == D3D11_RESOURCE_DIMENSION_TEXTURE1D) {
			desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE1D;
		}
		else if (dimension == D3D11_RESOURCE_DIMENSION_TEXTURE2D) {
			desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		}

		desc.Format = fmt;
		if (inReadOnly) {
			desc.Flags |= (D3D11_DSV_READ_ONLY_DEPTH | D3D11_DSV_READ_ONLY_STENCIL);
		}
	}

	ID3D11DepthStencilView *d3d_view = nullptr;
	if (FAILED(m_Device->CreateDepthStencilView(resource, texture->IsTypeless() ? &desc : nullptr, &d3d_view))) {
		throw GraphicsException("Failed creating a depth/stencil view!");
	}

	*viewOut = new DepthStencilView(d3d_view);
}

void Graphics::CreateShaderFromBlob(ID3DBlob* sourceBlob, ERenderTechnique technique, Shader** shaderOut) {
	assert(m_Device);

	UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;

#if defined(_DEBUG) || !defined(NDEBUG)
	flags |= D3DCOMPILE_DEBUG;
#else
	flags |= (D3DCOMPILE_OPTIMIZATION_LEVEL3 | D3DCOMPILE_WARNINGS_ARE_ERRORS);
#endif

	std::map<EShaderType, ShaderInstance> instances;

	for (uint32_t i = 0; i < (uint32_t)EShaderType::SHADER_NUM_TYPES; i++) {
		ShaderInstance instance;

		const std::string profile = Shader::sGetProfile(i);
		const std::string entry_point = Shader::sGetEntryPoint(i, technique);

		ID3DBlob* errors = nullptr;

		HRESULT hr = D3DCompile(sourceBlob->GetBufferPointer(), sourceBlob->GetBufferSize(), "Assets\\Shaders\\temp.hlsl", nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, entry_point.c_str(), profile.c_str(), flags, 0, &instance.Code, &errors);
		
		if (SUCCEEDED(hr)) {

			const void* code = instance.Code->GetBufferPointer();
			const size_t code_size = instance.Code->GetBufferSize();

			EShaderType type = (EShaderType)(1 << i);
			switch (type) {
			case EShaderType::SHADER_TYPE_VERTEX:
				hr = m_Device->CreateVertexShader(code, code_size, nullptr, (ID3D11VertexShader**)(&instance.Shader));
				break;

			case EShaderType::SHADER_TYPE_PIXEL:
				hr = m_Device->CreatePixelShader(code, code_size, nullptr, (ID3D11PixelShader**)(&instance.Shader));
				break;

			case EShaderType::SHADER_TYPE_GEOMETRY:
				hr = m_Device->CreateGeometryShader(code, code_size, nullptr, (ID3D11GeometryShader**)(&instance.Shader));
				break;

			case EShaderType::SHADER_TYPE_DOMAIN:
				hr = m_Device->CreateDomainShader(code, code_size, nullptr, (ID3D11DomainShader**)(&instance.Shader));
				break;

			case EShaderType::SHADER_TYPE_HULL:
				hr = m_Device->CreateHullShader(code, code_size, nullptr, (ID3D11HullShader**)(&instance.Shader));
				break;

			case EShaderType::SHADER_TYPE_COMPUTE:
				hr = m_Device->CreateComputeShader(code, code_size, nullptr, (ID3D11ComputeShader**)(&instance.Shader));
				break;

			default:
				throw GraphicsException("Unsupported shader type!");
				break;
			}

			if (SUCCEEDED(hr)) {
				instances[type] = instance;
			}
		}
		else {
			if (errors != nullptr) {
				const char* data = (const char*)errors->GetBufferPointer();
				gTrace(data);
			}

			SRELEASE(instance.Code);
		}

		SRELEASE(errors);
	}

	*shaderOut = new Shader();
	for (auto& it : instances) {
		(*shaderOut)->AddShaderInstance(it.first, it.second);
	}

	(*shaderOut)->Reflect();

	ID3DBlob* vs_blob = nullptr;
	const std::vector<D3D11_INPUT_ELEMENT_DESC>& input_layout_desc = (*shaderOut)->getInputLayoutDescription(&vs_blob);

	// TODO: cache this and re-use
	m_Device->CreateInputLayout(&input_layout_desc[0], (UINT)input_layout_desc.size(), vs_blob->GetBufferPointer(), vs_blob->GetBufferSize(), &(*shaderOut)->m_Layout);
}

void Graphics::CreateShader(const std::wstring& filename, ERenderTechnique technique, Shader** shaderOut) {
	ID3DBlob *blob = nullptr;
	D3DReadFileToBlob(filename.c_str(), &blob);
	CreateShaderFromBlob(blob, technique, shaderOut);
}

void Graphics::CreateRenderPipelineSetup(RenderPipelineDesc* const inDesc, RenderPipelineSetup** outRenderPipelineSetup) {
	gAssert(inDesc->ShaderInstance != nullptr, "RenderPipelineDesc must define a valid shader instance");

	ID3D11BlendState *blend_state = nullptr;
	if (flags(inDesc->CreationFlags & ERenderPipelineSetupFlags::RPS_CREATE_BLEND_STATE)) gAssertD3D(m_Device->CreateBlendState(&inDesc->BlendDesc, &blend_state), "Failed creating a blend state");
	
	ID3D11RasterizerState* rasterizer_state = nullptr;
	if (flags(inDesc->CreationFlags & ERenderPipelineSetupFlags::RPS_CREATE_RASTERIZER_STATE)) gAssertD3D(m_Device->CreateRasterizerState(&inDesc->RasterizerDesc, &rasterizer_state), "Failed creating a rasterizer state");

	ID3D11DepthStencilState* ds_state = nullptr;
	if (flags(inDesc->CreationFlags & ERenderPipelineSetupFlags::RPS_CREATE_DEPTH_STENCIL_STATE)) gAssertD3D(m_Device->CreateDepthStencilState(&inDesc->DepthStencilDesc, &ds_state), "Failed creating a depth/stencil state");

	*outRenderPipelineSetup = new RenderPipelineSetup(blend_state, rasterizer_state, ds_state, inDesc->ShaderInstance, inDesc->SampleMask, inDesc->ApplyInputLayout);
}

//
// Pipeline functions
//
void Graphics::SetGraphicsResourceBindings(GraphicsResourceBindingContainer* const inContainer) {
	gAssert(inContainer != nullptr, "Expects a valid GraphicsResourceBindingContainer instance");

	SetIndexBuffer(inContainer->IB);
	SetVertexBuffers(inContainer->VBSlot, inContainer->VB);
	SetConstantBuffers(inContainer->CB);
	SetResources(inContainer->SRV);
	SetSamplers(inContainer->Samplers);
}

void Graphics::SetComputeResourceBindings(ComputeResourceBindingContainer* const inContainer) {
	gAssert(inContainer != nullptr, "Expects a valid ComputeResourceBindingContainer instance");
}

void Graphics::SetRenderPipelineSetup(RenderPipelineSetup* const inRenderPipeline) {
	SetRenderPipelineSetup(inRenderPipeline, 0, Color(1, 1, 1, 1));
}

void Graphics::SetRenderPipelineSetup(RenderPipelineSetup* const inRenderPipeline, uint8_t inStencilRef, const Color& inBlendFactor) {
	gAssert(inRenderPipeline != nullptr, "Expected a valid RenderPipelineSetup instance");

	m_Context->RSSetState(inRenderPipeline->RasterizerState);
	m_Context->OMSetDepthStencilState( inRenderPipeline->DepthStencilState, inStencilRef);
	m_Context->OMSetBlendState(inRenderPipeline->BlendState, &inBlendFactor[0], inRenderPipeline->SampleMask);

	SetShader(inRenderPipeline->ShaderInstance, inRenderPipeline->ApplyInputLayout);
}

void Graphics::SetTopology(D3D11_PRIMITIVE_TOPOLOGY inTopology) {
	assert(m_Context);

	m_Context->IASetPrimitiveTopology(inTopology);
}

void Graphics::SetConstantBuffers(const std::vector<ConstantBufferBinding>& inBindings) {
	assert(m_Context);
	assert( inBindings.size() < D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT);
	
	uint32_t num_buffers = (uint32_t)inBindings.size();
	
	for (uint32_t i = 0; i < num_buffers; ++i) {
		EShaderType stage = inBindings[i].ShaderStage;
		ConstantBuffer* buffer = inBindings[i].Buffer;
		ShaderBindingHandle* binding_handle = inBindings[i].BindingHandle;

		if (!binding_handle) continue;

		if (buffer && buffer->m_Buffer) {
			if (flags(stage & EShaderType::SHADER_TYPE_VERTEX) && binding_handle->IsValid(EShaderType::SHADER_TYPE_VERTEX)) {
				m_Context->VSSetConstantBuffers(binding_handle->GetSlot(EShaderType::SHADER_TYPE_VERTEX), 1, &buffer->m_Buffer);
			}

			if (flags(stage & EShaderType::SHADER_TYPE_PIXEL) && binding_handle->IsValid(EShaderType::SHADER_TYPE_PIXEL)) {
				m_Context->PSSetConstantBuffers(binding_handle->GetSlot(EShaderType::SHADER_TYPE_PIXEL), 1, &buffer->m_Buffer);
			}

			if (flags(stage & EShaderType::SHADER_TYPE_GEOMETRY) && binding_handle->IsValid(EShaderType::SHADER_TYPE_GEOMETRY)) {
				m_Context->GSSetConstantBuffers(binding_handle->GetSlot(EShaderType::SHADER_TYPE_GEOMETRY), 1, &buffer->m_Buffer);
			}

			if (flags(stage & EShaderType::SHADER_TYPE_DOMAIN) && binding_handle->IsValid(EShaderType::SHADER_TYPE_DOMAIN)) {
				m_Context->DSSetConstantBuffers(binding_handle->GetSlot(EShaderType::SHADER_TYPE_DOMAIN), 1, &buffer->m_Buffer);
			}

			if (flags(stage & EShaderType::SHADER_TYPE_HULL) && binding_handle->IsValid(EShaderType::SHADER_TYPE_HULL)) {
				m_Context->HSSetConstantBuffers(binding_handle->GetSlot(EShaderType::SHADER_TYPE_HULL), 1, &buffer->m_Buffer);
			}

			if (flags(stage & EShaderType::SHADER_TYPE_COMPUTE) && binding_handle->IsValid(EShaderType::SHADER_TYPE_COMPUTE)) {
				m_Context->CSSetConstantBuffers(binding_handle->GetSlot(EShaderType::SHADER_TYPE_COMPUTE), 1, &buffer->m_Buffer);
			}
		}
	}
}

void Graphics::SetViewports(uint32_t inNumViewports, Viewport* const* viewports) {
	assert(m_Context);
	assert(viewports && ((inNumViewports > 0) && (inNumViewports < MAGMA_MAX_VIEWPORTS)));

	D3D11_VIEWPORT* vp[MAGMA_MAX_VIEWPORTS];

	for (uint32_t i = 0; i < inNumViewports; i++) {
		vp[i] = &viewports[i]->m_Viewport;
	}
	
	m_Context->RSSetViewports(inNumViewports, &(*vp[0]));
}

void Graphics::SetMRT(RenderTarget* const inRenderTarget) {
	assert(m_Context);
	
	// unbind resources, in case we have the mrt bound for reading
	ID3D11ShaderResourceView* null_srv[1] = { nullptr };
	m_Context->VSSetShaderResources(0, 1, null_srv);
	m_Context->PSSetShaderResources(0, 1, null_srv);
	m_Context->GSSetShaderResources(0, 1, null_srv);
	m_Context->DSSetShaderResources(0, 1, null_srv);
	m_Context->HSSetShaderResources(0, 1, null_srv);
	m_Context->CSSetShaderResources(0, 1, null_srv);
	//

	RenderTarget* target = inRenderTarget;
	if (!target) {
		target = m_BackbufferTarget;
	}

	ID3D11RenderTargetView* rtv_array[D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT];
	if (target) {

		ID3D11DepthStencilView* dsv = target->GetDepthTarget().DSV->GetDSV();
		for (uint32_t i = 0; i < target->GetColorTargetCount(); ++i) {
			rtv_array[i] = target->GetColorTarget(i).RTV->GetRTV();
		}

		m_Context->OMSetRenderTargets(target->GetColorTargetCount(), &rtv_array[0], dsv);
	}

	m_CurrentRenderTarget = target;
}

void Graphics::ClearMRT(EClearFlag inClearFlag, const RenderTargetClear& inClear, RenderTarget* const inRenderTarget) {
	if (flags(inClearFlag & EClearFlag::CLEAR_DEPTH) || flags(inClearFlag & EClearFlag::CLEAR_STENCIL)) {
		assert(inRenderTarget->GetDepthTarget().DSV);
		ClearDepthStencilView(inRenderTarget->GetDepthTarget().DSV, inClearFlag, inClear.ClearDepth, inClear.ClearStencil);
	}

	if (flags(inClearFlag & EClearFlag::CLEAR_COLOR)) {
		for (uint32_t i = 0; i < inRenderTarget->GetColorTargetCount(); ++i) {
			assert(inRenderTarget->GetColorTarget(i).RTV);
			ClearRenderTargetView(inRenderTarget->GetColorTarget(i).RTV, inClear.ClearColor[i]);
		}
	}
}

void Graphics::ClearRenderTargetView(RenderTargetView* view, const Color& color) {
	assert(m_Context);
	assert(view);

	m_Context->ClearRenderTargetView(view->GetRTV(), &color[0]);
}

void Graphics::ClearDepthStencilView(DepthStencilView* const view, EClearFlag clearFlags, float clearDepth, uint8_t clearStencil) {
	assert(m_Context);
	assert(view);

	m_Context->ClearDepthStencilView(view->GetDSV(), (uint32_t)clearFlags, clearDepth, clearStencil);
}

void Graphics::SetShader(Shader* const shader, bool applyLayout) {
	assert(m_Context);
	assert(shader);

	if (applyLayout) {
		assert(shader->m_Layout);
		m_Context->IASetInputLayout(shader->m_Layout);
	}
	else {
		m_Context->IASetInputLayout(nullptr);
	}

	const ShaderInstance& vs = shader->GetShaderInstance(EShaderType::SHADER_TYPE_VERTEX);
	m_Context->VSSetShader((ID3D11VertexShader*)vs.Shader, nullptr, 0);

	const ShaderInstance& ps = shader->GetShaderInstance(EShaderType::SHADER_TYPE_PIXEL);
	m_Context->PSSetShader((ID3D11PixelShader*)ps.Shader, nullptr, 0);

	const ShaderInstance& gs = shader->GetShaderInstance(EShaderType::SHADER_TYPE_GEOMETRY);
	m_Context->GSSetShader((ID3D11GeometryShader*)gs.Shader, nullptr, 0);

	const ShaderInstance& hs = shader->GetShaderInstance(EShaderType::SHADER_TYPE_HULL);
	m_Context->HSSetShader((ID3D11HullShader*)hs.Shader, nullptr, 0);

	const ShaderInstance& ds = shader->GetShaderInstance(EShaderType::SHADER_TYPE_DOMAIN);
	m_Context->DSSetShader((ID3D11DomainShader*)ds.Shader, nullptr, 0);

	const ShaderInstance& cs = shader->GetShaderInstance(EShaderType::SHADER_TYPE_COMPUTE);
	m_Context->CSSetShader((ID3D11ComputeShader*)cs.Shader, nullptr, 0);	
}

void Graphics::SetSamplers(const std::vector<SamplerBinding>& inBindings) {
	assert(m_Context);
	assert(inBindings.size() < D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT);
	
	uint32_t num_buffers = (uint32_t)inBindings.size();

	for (uint32_t i = 0; i < num_buffers; ++i) {
		EShaderType stage = inBindings[i].ShaderStage;
		SamplerState* sampler = inBindings[i].Sampler;
		ShaderBindingHandle* binding_handle = inBindings[i].BindingHandle;

		if (sampler && sampler->m_State) {
			if (flags(stage & EShaderType::SHADER_TYPE_VERTEX) && binding_handle->IsValid(EShaderType::SHADER_TYPE_VERTEX)) {
				m_Context->VSSetSamplers(binding_handle->GetSlot(EShaderType::SHADER_TYPE_VERTEX), 1, &sampler->m_State);
			}

			if (flags(stage & EShaderType::SHADER_TYPE_PIXEL) && binding_handle->IsValid(EShaderType::SHADER_TYPE_PIXEL)) {
				m_Context->PSSetSamplers(binding_handle->GetSlot(EShaderType::SHADER_TYPE_PIXEL), 1, &sampler->m_State);
			}

			if (flags(stage & EShaderType::SHADER_TYPE_GEOMETRY) && binding_handle->IsValid(EShaderType::SHADER_TYPE_GEOMETRY)) {
				m_Context->GSSetSamplers(binding_handle->GetSlot(EShaderType::SHADER_TYPE_GEOMETRY), 1, &sampler->m_State);
			}

			if (flags(stage & EShaderType::SHADER_TYPE_DOMAIN) && binding_handle->IsValid(EShaderType::SHADER_TYPE_DOMAIN)) {
				m_Context->DSSetSamplers(binding_handle->GetSlot(EShaderType::SHADER_TYPE_DOMAIN), 1, &sampler->m_State);
			}

			if (flags(stage & EShaderType::SHADER_TYPE_HULL) && binding_handle->IsValid(EShaderType::SHADER_TYPE_HULL)) {
				m_Context->HSSetSamplers(binding_handle->GetSlot(EShaderType::SHADER_TYPE_HULL), 1, &sampler->m_State);
			}

			if (flags(stage & EShaderType::SHADER_TYPE_COMPUTE) && binding_handle->IsValid(EShaderType::SHADER_TYPE_COMPUTE)) {
				m_Context->CSSetSamplers(binding_handle->GetSlot(EShaderType::SHADER_TYPE_COMPUTE), 1, &sampler->m_State);
			}
		}
	}
}

void Graphics::SetResources(const std::vector<SRVBinding>& inBindings) {
	assert(m_Context);
	assert(inBindings.size() < D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT);

	uint32_t num_buffers = (uint32_t)inBindings.size();

	for (uint32_t i = 0; i < num_buffers; ++i) {
		EShaderType stage = inBindings[i].ShaderStage;
		ShaderResourceView* srv = inBindings[i].SRV;
		ShaderBindingHandle* binding_handle = inBindings[i].BindingHandle;

		if (srv && srv->m_View) {
			if (flags(stage & EShaderType::SHADER_TYPE_VERTEX) && binding_handle->IsValid(EShaderType::SHADER_TYPE_VERTEX)) {
				m_Context->VSSetShaderResources(binding_handle->GetSlot(EShaderType::SHADER_TYPE_VERTEX), 1, &srv->m_View);
			}

			if (flags(stage & EShaderType::SHADER_TYPE_PIXEL) && binding_handle->IsValid(EShaderType::SHADER_TYPE_PIXEL)) {
				m_Context->PSSetShaderResources(binding_handle->GetSlot(EShaderType::SHADER_TYPE_PIXEL), 1, &srv->m_View);
			}

			if (flags(stage & EShaderType::SHADER_TYPE_GEOMETRY) && binding_handle->IsValid(EShaderType::SHADER_TYPE_GEOMETRY)) {
				m_Context->GSSetShaderResources(binding_handle->GetSlot(EShaderType::SHADER_TYPE_GEOMETRY), 1, &srv->m_View);
			}

			if (flags(stage & EShaderType::SHADER_TYPE_DOMAIN) && binding_handle->IsValid(EShaderType::SHADER_TYPE_DOMAIN)) {
				m_Context->DSSetShaderResources(binding_handle->GetSlot(EShaderType::SHADER_TYPE_DOMAIN), 1, &srv->m_View);
			}

			if (flags(stage & EShaderType::SHADER_TYPE_HULL) && binding_handle->IsValid(EShaderType::SHADER_TYPE_HULL)) {
				m_Context->HSSetShaderResources(binding_handle->GetSlot(EShaderType::SHADER_TYPE_HULL), 1, &srv->m_View);
			}

			if (flags(stage & EShaderType::SHADER_TYPE_COMPUTE) && binding_handle->IsValid(EShaderType::SHADER_TYPE_COMPUTE)) {
				m_Context->CSSetShaderResources(binding_handle->GetSlot(EShaderType::SHADER_TYPE_COMPUTE), 1, &srv->m_View);
			}
		}
	}
}

void Graphics::SetVertexBuffers(uint32_t inStartSlot, const std::vector<VertexBufferBinding>& inBindings) {
	assert(m_Context);
	assert(inBindings.size() < D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT - inStartSlot);

	uint32_t num_buffers = (uint32_t)inBindings.size();

	ID3D11Buffer* buffers[D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT];
	uint32_t strides[D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT];
	uint32_t offsets[D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT];

	for (uint32_t i = 0; i < num_buffers; ++i) {
		buffers[i] = inBindings[i].Buffer->m_Buffer;
		strides[i] = (uint32_t)inBindings[i].Stride;
		offsets[i] = inBindings[i].Offset;
	}

	m_Context->IASetVertexBuffers(inStartSlot, num_buffers, 
		num_buffers > 0 ? &buffers[0] : nullptr,
		num_buffers > 0 ? &strides[0] : nullptr, 
		num_buffers > 0 ? &offsets[0] : nullptr);
}

void Graphics::SetIndexBuffer(const IndexBufferBinding& inBinding) {
	assert(m_Context);

	m_Context->IASetIndexBuffer(inBinding.Buffer ? inBinding.Buffer->m_Buffer : nullptr,
		inBinding.Buffer ? inBinding.Format : (DXGI_FORMAT)0,
		inBinding.Buffer ? inBinding.Offset : 0);
}

void Graphics::ExecuteCommandLists(CommandList** const inLists, uint32_t inSize) {
	gAssert(inLists != nullptr, "Expected a valid array of ICommandList instances");
	gAssert(inSize > 0, "Expected a valid array size");

	for (uint32_t i = 0; i < inSize; ++i) {
		inLists[i]->Execute();
	}
}