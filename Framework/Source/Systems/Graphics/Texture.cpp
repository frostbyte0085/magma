#include "Texture.hpp"

using namespace Magma;

namespace Magma {
	bool gIsFormatTypeless(DXGI_FORMAT inFmt) {
		return ((inFmt == DXGI_FORMAT_B8G8R8A8_TYPELESS) ||
			(inFmt == DXGI_FORMAT_B8G8R8X8_TYPELESS) ||
			(inFmt == DXGI_FORMAT_BC1_TYPELESS) ||
			(inFmt == DXGI_FORMAT_BC2_TYPELESS) ||
			(inFmt == DXGI_FORMAT_BC3_TYPELESS) ||
			(inFmt == DXGI_FORMAT_BC4_TYPELESS) ||
			(inFmt == DXGI_FORMAT_BC5_TYPELESS) ||
			(inFmt == DXGI_FORMAT_BC6H_TYPELESS) ||
			(inFmt == DXGI_FORMAT_BC7_TYPELESS) ||
			(inFmt == DXGI_FORMAT_R10G10B10A2_TYPELESS) ||
			(inFmt == DXGI_FORMAT_R16G16B16A16_TYPELESS) ||
			(inFmt == DXGI_FORMAT_R16G16_TYPELESS) ||
			(inFmt == DXGI_FORMAT_R16_TYPELESS) ||
			(inFmt == DXGI_FORMAT_R24G8_TYPELESS) ||
			(inFmt == DXGI_FORMAT_R24_UNORM_X8_TYPELESS) ||
			(inFmt == DXGI_FORMAT_R32G32B32A32_TYPELESS) ||
			(inFmt == DXGI_FORMAT_R32G32B32_TYPELESS) ||
			(inFmt == DXGI_FORMAT_R32G32_TYPELESS) ||
			(inFmt == DXGI_FORMAT_R32G8X24_TYPELESS) ||
			(inFmt == DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS) ||
			(inFmt == DXGI_FORMAT_R32_TYPELESS) ||
			(inFmt == DXGI_FORMAT_R8G8B8A8_TYPELESS) ||
			(inFmt == DXGI_FORMAT_R8G8_TYPELESS) ||
			(inFmt == DXGI_FORMAT_R8_TYPELESS) ||
			(inFmt == DXGI_FORMAT_X24_TYPELESS_G8_UINT) ||
			(inFmt == DXGI_FORMAT_X32_TYPELESS_G8X24_UINT)
			);
	}
}
Texture::Texture(ID3D11Resource* resource) {
	m_Resource = resource;
}

Texture::~Texture() {
	SRELEASE(m_Resource);
}

D3D11_TEXTURE1D_DESC Texture::sDescribe1D(const TextureDesc1D* desc) {
	assert(desc);

	return std::move(desc->Desc);
}

D3D11_TEXTURE2D_DESC Texture::sDescribe2D(const TextureDesc2D* desc) {
	assert(desc);

	return std::move(desc->Desc);
}

D3D11_TEXTURE3D_DESC Texture::sDescribe3D(const TextureDesc3D* desc) {
	assert(desc);

	return std::move(desc->Desc);
}

//
// Texture 1D
//
Texture1D::Texture1D(ID3D11Texture1D* tex1D) : Texture(tex1D) {
	tex1D->GetDesc(&m_Desc);
}

Texture1D::~Texture1D() {

}

//
// Texture 2D
//
Texture2D::Texture2D(ID3D11Texture2D* tex2D) : Texture(tex2D) {
	tex2D->GetDesc(&m_Desc);
}

Texture2D::~Texture2D() {
	
}

//
// Texture 3D
//
Texture3D::Texture3D(ID3D11Texture3D* tex3D) : Texture(tex3D) {
	tex3D->GetDesc(&m_Desc);
}

Texture3D::~Texture3D() {

}