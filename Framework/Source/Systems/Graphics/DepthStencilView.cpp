#include "../../Application/Application.hpp"
#include "DepthStencilView.hpp"
#include "Texture.hpp"

using namespace Magma;

DepthStencilView::DepthStencilView(ID3D11DepthStencilView* const view) : m_DSV(view) {

}

DepthStencilView::~DepthStencilView() {
	SRELEASE(m_DSV);
}
