#include "Buffer.hpp"
#include "../../Application/Application.hpp"

using namespace Magma;

//
// Buffer Base class
//
Buffer::Buffer(ID3D11Buffer* buffer) {
	assert(buffer);
	m_Buffer = buffer;
	m_Buffer->GetDesc(&m_Desc);
}

Buffer::~Buffer() {
	SRELEASE(m_Buffer);
}

void Buffer::Fill(const void* data, size_t dataSize, D3D11_MAP mapType, uint32_t mapFlags, uint32_t subresource) {
	assert(m_Buffer);
	assert(gGfx);

	ID3D11DeviceContext* context = gGfx->GetD3DContext();
	assert(context);

	D3D11_MAPPED_SUBRESOURCE ms;
	context->Map(m_Buffer, subresource, mapType, mapFlags, &ms);
	memcpy(ms.pData, data, dataSize);
	context->Unmap(m_Buffer, subresource);
}

D3D11_BUFFER_DESC Buffer::sDescribe(const BufferDesc* bufferDesc) {
	assert(bufferDesc);
	return std::move(bufferDesc->Desc);
}
//
// VertexBuffer class
//
VertexBuffer::~VertexBuffer() {

}

//
// IndexBuffer class
//
IndexBuffer::~IndexBuffer() {

}