#pragma once

namespace Magma {
	// enums
	ENUM_FLAGS(EShaderType)
	enum class EShaderType {
		SHADER_TYPE_VERTEX = 0x001,
		SHADER_TYPE_PIXEL = 0x002,
		SHADER_TYPE_GEOMETRY = 0x004,
		SHADER_TYPE_DOMAIN = 0x008,
		SHADER_TYPE_HULL = 0x016,
		SHADER_TYPE_COMPUTE = 0x032,
		SHADER_NUM_TYPES = 6,
		SHADER_TYPE_UNKNOWN = 0xFF,
		SHADER_TYPE_GRAPHICS = (SHADER_TYPE_VERTEX | SHADER_TYPE_PIXEL | SHADER_TYPE_GEOMETRY | SHADER_TYPE_HULL | SHADER_TYPE_DOMAIN),
		SHADER_TYPE_ALL = (SHADER_TYPE_VERTEX | SHADER_TYPE_PIXEL | SHADER_TYPE_GEOMETRY | SHADER_TYPE_HULL | SHADER_TYPE_DOMAIN | SHADER_TYPE_COMPUTE)
	};

	ENUM_FLAGS(EVertexStreamChannel)
	enum class EVertexStreamChannel {
		CHANNEL_POSITION = 1 << 0,
		CHANNEL_NORMAL = 1 << 1,
		CHANNEL_TANGENT = 1 << 2,
		CHANNEL_TEXCOORD0 = 1 << 3,
		CHANNEL_TEXCOORD1 = 1 << 4,
		CHANNEL_COLOR0 = 1 << 5,
		CHANNEL_COLOR1 = 1 << 6
	};

	ENUM_FLAGS(ERenderTechnique)
	enum class ERenderTechnique {
		TECHNIQUE_DEFERRED_GEOMETRY_PASS,
		TECHNIQUE_FULLSCREEN_QUAD,
		TECHNIQUE_CUSTOM
	};

	ENUM_FLAGS(EClearFlag)
	enum class EClearFlag {
		CLEAR_DEPTH = D3D11_CLEAR_DEPTH,
		CLEAR_STENCIL = D3D11_CLEAR_STENCIL,
		CLEAR_COLOR = D3D11_CLEAR_STENCIL << 1
	};

	ENUM_FLAGS(EMeshFlags)
	enum class EMeshFlags {
		NONE = 0,
		DYNAMIC_VB = 0x001,
		DYNAMIC_IB = 0x002
	};

	ENUM_FLAGS(ERenderPipelineSetupFlags)
	enum class ERenderPipelineSetupFlags {
		RPS_CREATE_NONE = 0,
		RPS_CREATE_BLEND_STATE = 0x1,
		RPS_CREATE_RASTERIZER_STATE = 0x2,
		RPS_CREATE_DEPTH_STENCIL_STATE = 0x4,
		RPS_CREATE_ALL = (RPS_CREATE_BLEND_STATE | RPS_CREATE_RASTERIZER_STATE | RPS_CREATE_DEPTH_STENCIL_STATE)
	};
	
	enum class EDrawCommandType {
		INDEXED,
		NON_INDEXED
	};

	enum class ECommandListCategory {
		FRAME_START,
		GBUFFER,
		POSTPROCESS,
		MAX_TYPES
	};

	enum class EScreenWipe {
		NONE = -1,
		ALBEDO = 0,
		METALNESS = (ALBEDO + 1),
		ROUGHNESS = (METALNESS + 1),
		NORMALS = (ROUGHNESS + 1),
		GBUFFER_LAST = NORMALS,
		MAX_TYPES = GBUFFER_LAST+1
	};

	ENUM_FLAGS(EDrawableSetupFlags)
	enum class EDrawableSetupFlags {
		NONE = 0x0,
		CULLED = 0x001
	};

	// forward declarations
	class SRVBinding;
	class SamplerBinding;
	class ConstantBufferBinding;
	class VertexBufferBinding;
	class IndexBufferBinding;
	class RenderPipelineSetup;
	struct RenderPipelineDesc;
	class GraphicsResourceBindingContainer;
	class ComputeResourceBindingContainer;
	class SamplerState;
	class RenderTargetView;
	class DepthStencilView;
	class ShaderResourceView;
	class UnorderedAccessView;
	class Viewport;
	class Buffer;
	class VertexBuffer;
	class IndexBuffer;
	class ConstantBuffer;
	class Texture;
	class Texture1D;
	class Texture2D;
	class Texture3D;
	class Shader;
	class ShaderBindingHandle;
	class RenderTarget;
	struct RenderTargetClear;	
	class CommandList;
	class ICommand;
	class DrawCommand;
	class IndexedDrawCommand;
	class SetRenderPipelineSetupCommand;
	class BindResourcesCommand;
	class SetMRTCommand;
	class ClearCommand;
	struct PerFrameBuffer;
	struct PerObjectBuffer;

	// high level
	class FSQ;
	class Renderer;
	class Mesh;
	class DrawableSetup;
	class SceneView;
	class DrawableSetupContext;
	class SortedDrawableSetupContextContainer;
}