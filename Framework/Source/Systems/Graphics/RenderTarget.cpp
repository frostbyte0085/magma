#include "RenderTarget.hpp"
#include "Texture.hpp"
#include "RenderTargetView.hpp"
#include "DepthStencilView.hpp"
#include "SamplerState.hpp"
#include "ShaderResourceView.hpp"
#include <Application/Application.hpp>
#include "DefaultResources.hpp"

using namespace Magma;

RenderTargetClear RenderTargetClear::sClear;

// RenderTargerData (base)
void RenderTargetData::Release() {
	if (OwnTexture) {
		SDELETE(Texture);
	}
}

// RenderTargetColor
void RenderTargetColor::Release() {
	RenderTargetData::Release();

	if (OwnView) {
		SDELETE(RTV);
		SDELETE(SRV);
	}
}

// RenderTargetDepth
void RenderTargetDepth::Release() {
	RenderTargetData::Release();

	if (OwnView) {
		SDELETE(DSVReadOnly);
		SDELETE(DSV);
		SDELETE(SRV);
	}
}

// RenderTarget
RenderTarget::RenderTarget() {

}

RenderTarget::~RenderTarget() {
	SDELETE_ARRAY(m_ColorTargets);
	m_DepthTarget.Release();
}

void RenderTarget::Initialise(uint32_t inColorTargetCount, uint32_t inWidth, uint32_t inHeight, uint32_t inDepth) {
	SDELETE_ARRAY(m_ColorTargets);
	m_DepthTarget.Release();

	assert((inWidth > 0) && (inHeight > 0));

	m_ColorTargetCount = inColorTargetCount;
	m_Width = inWidth;
	m_Height = inHeight;
	m_Depth = inDepth;
	m_ColorTargets = new RenderTargetColor[m_ColorTargetCount];
}

void RenderTarget::createDepthResources(RenderTargetDepth& ioDepth) {

	DXGI_FORMAT dsv_format = ioDepth.Texture->GetFormat();
	DXGI_FORMAT srv_format = ioDepth.Texture->GetFormat();

	switch (ioDepth.Texture->GetFormat()) {
	case DXGI_FORMAT_R24G8_TYPELESS:
		dsv_format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		srv_format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
		break;
	default: break;
	}
		
	ioDepth.Format = dsv_format;

	if (ioDepth.Texture->CanBindDSV()) {
		gGfx->CreateDepthStencilView(ioDepth.Texture, dsv_format, false, &ioDepth.DSV);
		gGfx->CreateDepthStencilView(ioDepth.Texture, dsv_format, true, &ioDepth.DSVReadOnly);
	}

	if (ioDepth.Texture->CanBindSRV()) gGfx->CreateShaderResourceView(ioDepth.Texture, srv_format, &ioDepth.SRV);
}

void RenderTarget::createColorResources(RenderTargetColor& ioColor) {
	
	if (ioColor.Texture->CanBindRTV()) gGfx->CreateRenderTargetView(ioColor.Texture, ioColor.Texture->GetFormat(), &ioColor.RTV);
	if (ioColor.Texture->CanBindSRV()) gGfx->CreateShaderResourceView(ioColor.Texture, ioColor.Texture->GetFormat(), &ioColor.SRV);
}

void RenderTarget::SetColorTarget(uint32_t idx, DXGI_FORMAT fmt, D3D11_USAGE usage) {
	assert((idx >= 0) && (idx < m_ColorTargetCount));

	RenderTargetColor& target = m_ColorTargets[idx];
	target.Release();

	TextureDesc2D texture_desc(m_Width, m_Height, 1, fmt, usage, (D3D11_BIND_FLAG)(D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET));
	gGfx->CreateTexture2D(texture_desc.Desc, nullptr, &target.Texture);

	target.Format = target.Texture->GetFormat();

	target.OwnTexture = true;
	target.OwnView = true;

	createColorResources(target);
}

void RenderTarget::SetColorTarget(uint32_t inIdx, Texture* const inColorTexture) {
	assert((inIdx >= 0) && (inIdx < m_ColorTargetCount));

	RenderTargetColor& target = m_ColorTargets[inIdx];
	target.Release();

	target.Texture = static_cast<Texture2D*>(inColorTexture);

	target.Format = target.Texture->GetFormat();

	target.OwnTexture = false;
	target.OwnView = true;

	createColorResources(target);
}

void RenderTarget::SetColorTarget(uint32_t inIndex, RenderTargetView* const inRTV, ShaderResourceView* const inSRV) {
	assert((inIndex >= 0) && (inIndex < m_ColorTargetCount));

	RenderTargetColor& target = m_ColorTargets[inIndex];
	target.Release();

	D3D11_RENDER_TARGET_VIEW_DESC desc;
	inRTV->GetRTV()->GetDesc(&desc);

	target.OwnTexture = false;
	target.OwnView = false;

	target.Format = desc.Format;

	target.RTV = inRTV;
	target.SRV = inSRV;
}

void RenderTarget::SetDepthTarget(DepthStencilView* const inDSV, ShaderResourceView* const inSRV) {
	D3D11_DEPTH_STENCIL_VIEW_DESC desc;
	inDSV->GetDSV()->GetDesc(&desc);

	m_DepthTarget.OwnTexture = false;
	m_DepthTarget.OwnView = false;

	m_DepthTarget.Format = desc.Format;

	m_DepthTarget.DSV = inDSV;
	m_DepthTarget.SRV = inSRV;
}

void RenderTarget::SetDepthTarget(DXGI_FORMAT fmt, D3D11_USAGE usage) {
	m_DepthTarget.Release();

	// TODO: check if format is valid depth
	uint32_t bind_flags = D3D11_BIND_DEPTH_STENCIL;
	if (gIsFormatTypeless(fmt)) bind_flags |= (D3D11_BIND_SHADER_RESOURCE);

	TextureDesc2D texture_desc(m_Width, m_Height, 1, fmt, usage, (D3D11_BIND_FLAG)bind_flags);
	gGfx->CreateTexture2D(texture_desc.Desc, nullptr, &m_DepthTarget.Texture);
	
	m_DepthTarget.OwnTexture = true;
	m_DepthTarget.OwnView = true;

	createDepthResources(m_DepthTarget);
}

void RenderTarget::SetDepthTarget(Texture* const depthTexture) {
	m_DepthTarget.Release();

	// TODO: check if format is valid depth
	m_DepthTarget.Texture = static_cast<Texture2D*>(depthTexture);
	
	m_DepthTarget.OwnTexture = false;
	m_DepthTarget.OwnView = true;

	createDepthResources(m_DepthTarget);
}