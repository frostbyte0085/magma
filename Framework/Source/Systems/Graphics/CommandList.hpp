#pragma once

#include "Commands.hpp"

namespace Magma {

	class CommandList : public NonCopyable {
	public:
		CommandList() = delete;
		CommandList(uint32_t inMaxCommands) : m_MaxCommands(inMaxCommands) {
			m_Commands = new ICommand*[inMaxCommands];
		}

		~CommandList() {
			SDELETE_ARRAY(m_Commands);
		}

		template<class U, class... Args>
		U* const AddCommand(Args... inArgs) {
			// commands are stored in scratch cpu memory
			U* cmd = ICommand::Allocate<U>(inArgs...);

			m_Commands[m_CurrentIndex++] = cmd;
			return cmd;
		}

		void Execute() {
			// execute
			for (uint32_t i = 0; i < m_CurrentIndex; ++i) {
				ICommand* cmd = m_Commands[i];
				gAssert(cmd != nullptr, "Expected a valid ICommand instance");

				cmd->Execute();

				ICommand::Deallocate(cmd);
			}

			m_CurrentIndex = 0;
		}

	private:
		ICommand** m_Commands;

		uint32_t m_CurrentIndex;
		uint32_t m_MaxCommands;
	};
}