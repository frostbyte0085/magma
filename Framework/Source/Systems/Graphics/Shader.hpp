#pragma once

#include "Graphics.hpp"

namespace Magma {

	extern uint32_t gGetShaderIndexForType(EShaderType type);

	struct ShaderBindingHandleData {
		uint32_t Slot = 0;
		uint32_t SlotCount = 0;
		D3D_SRV_DIMENSION Dimension = D3D_SRV_DIMENSION_UNKNOWN;
		bool IsValid = false;
	};

	class ShaderBindingHandle {
		friend class Shader;
	public:
		ShaderBindingHandle() {}
		~ShaderBindingHandle() {}

		inline uint32_t GetSlot(EShaderType inType) const { return m_Data[gGetShaderIndexForType(inType)].Slot; }
		inline uint32_t GetArraySlotCount(EShaderType inType) const { return m_Data[gGetShaderIndexForType(inType)].SlotCount; }
		inline D3D_SRV_DIMENSION GetSRVDimension(EShaderType inType) const { return m_Data[gGetShaderIndexForType(inType)].Dimension; }
		inline bool IsValid(EShaderType inType) const { return m_Data[gGetShaderIndexForType(inType)].IsValid; }
		inline const std::string& GetName() const { return m_Name; }

	private:
		ShaderBindingHandleData m_Data[(uint32_t)EShaderType::SHADER_NUM_TYPES];

		std::string m_Name;
	};

	struct ShaderInstance {
		ID3DBlob *Code = nullptr;
		ID3D11DeviceChild* Shader = nullptr;
	};

	class Shader : public NonCopyable {
		friend class Graphics;
	public:		
		~Shader();
		
		static inline std::string sGetProfile(uint32_t index) { return std::move(sNames[index] + "_" + sTargetProfile); }
		static inline std::string sGetEntryPoint(uint32_t index, ERenderTechnique technique) { return std::move(sNames[index] + "_" + sEntryPoint + "_" + sRenderTechniqueToFunctionName(technique)); }

		inline void AddShaderInstance(EShaderType type, const ShaderInstance& instance) {
			m_Instances[gGetShaderIndexForType(type)] = instance;
		}

		inline const ShaderInstance& GetShaderInstance(EShaderType type) {
			return m_Instances[gGetShaderIndexForType(type)];
		}

		inline ShaderBindingHandle* const GetTextureHandle(const std::string& inName) const { return GetBindingHandle(inName, D3D_SIT_TEXTURE); }
		inline ShaderBindingHandle* const GetSamplerHandle(const std::string& inName) const { return GetBindingHandle(inName, D3D_SIT_SAMPLER); }
		inline ShaderBindingHandle* const GetCBufferHandle(const std::string& inName) const { return GetBindingHandle(inName, D3D_SIT_CBUFFER); }
		ShaderBindingHandle* const GetBindingHandle(const std::string& inName, D3D_SHADER_INPUT_TYPE inType) const;

		void Reflect();

	private:
		Shader();

		// we use this to create the input layout object for this vertex shader
		// thanks to: https://takinginitiative.wordpress.com/2011/12/11/directx-1011-basic-shader-reflection-automatic-input-layout-creation/
		std::vector<D3D11_INPUT_ELEMENT_DESC> getInputLayoutDescription(ID3DBlob** vsBlobOut);
		ID3D11InputLayout *m_Layout = nullptr;

		// keep track of all the created shaders
		ShaderInstance m_Instances[(uint32_t)EShaderType::SHADER_NUM_TYPES];
		std::map<D3D_SHADER_INPUT_TYPE, std::vector<ShaderBindingHandle*>> m_BindingHandles;
		
		static std::string sRenderTechniqueToFunctionName(ERenderTechnique technique) {
			std::string name;
			switch (technique) {
			case ERenderTechnique::TECHNIQUE_DEFERRED_GEOMETRY_PASS:
				name = "deferred_geometry";
				break;
			case ERenderTechnique::TECHNIQUE_FULLSCREEN_QUAD:
				name = "fsq";
				break;
			case ERenderTechnique::TECHNIQUE_CUSTOM:
				name = "custom";
				break;
			default: break;

			}

			return std::move(name);
		}

		static const std::string sTargetProfile;
		static const std::string sEntryPoint;
		static const std::string sNames[(uint32_t)EShaderType::SHADER_NUM_TYPES];
	};
}