#include "Bindings.hpp"
#include "Shader.hpp"

using namespace Magma;

// ResourceBinding
ResourceBinding::ResourceBinding(ShaderBindingHandle* const inBindingHandle, EShaderType inShaderStage) : ShaderStage(inShaderStage), BindingHandle(inBindingHandle) {

	gAssert(BindingHandle != nullptr, "Expected a valid BindingHandle instance");

	BindingName = inBindingHandle->GetName();
}

ResourceBinding::ResourceBinding(const std::string& inBindingName, EShaderType inShaderStage) :	ShaderStage(inShaderStage), BindingName(inBindingName), BindingHandle(nullptr) {

}

// SRV
SRVBinding::SRVBinding(ShaderResourceView* inSRV, ShaderBindingHandle* const inBindingHandle, EShaderType inShaderStage) : ResourceBinding(inBindingHandle, inShaderStage), SRV(inSRV) {
		
}

SRVBinding::SRVBinding(ShaderResourceView* inSRV, const std::string& inBindingName, EShaderType inShaderStage) : ResourceBinding(inBindingName, inShaderStage),	SRV(inSRV) {

}

// Sampler
SamplerBinding::SamplerBinding(SamplerState* inSampler, ShaderBindingHandle* const inBindingHandle, EShaderType inShaderStage) : ResourceBinding(inBindingHandle, inShaderStage), Sampler(inSampler) {
	
}

SamplerBinding::SamplerBinding(SamplerState* inSampler, const std::string& inBindingName, EShaderType inShaderStage) : ResourceBinding(inBindingName, inShaderStage), Sampler(inSampler) {

}

// CB
ConstantBufferBinding::ConstantBufferBinding(ConstantBuffer* inBuffer, ShaderBindingHandle* const inBindingHandle, EShaderType inShaderStage) : ResourceBinding(inBindingHandle, inShaderStage), Buffer(inBuffer) {

}

ConstantBufferBinding::ConstantBufferBinding(ConstantBuffer* inBuffer, const std::string& inBindingName, EShaderType inShaderStage) : ResourceBinding(inBindingName, inShaderStage), Buffer(inBuffer) {
}
