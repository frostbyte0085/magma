#include "Graphics.hpp"
#include "Bindings.hpp"

#pragma once

namespace Magma {
	// base class for defining resources to be bound to shaders
	class ResourceBindingContainer {
	public:
		ResourceBindingContainer();
		virtual ~ResourceBindingContainer();
		
		std::vector<ConstantBufferBinding> CB;
	};

	// graphics-specific resource container	
	class GraphicsResourceBindingContainer : public ResourceBindingContainer {
	public:
		GraphicsResourceBindingContainer();
		~GraphicsResourceBindingContainer();

		std::vector<SRVBinding> SRV;
		std::vector<SamplerBinding> Samplers;
		std::vector<VertexBufferBinding> VB;
		IndexBufferBinding IB = nullptr;

		uint32_t VBSlot = 0;
	};

	// management
	extern GraphicsResourceBindingContainer* gAllocateGraphicsResourceBindingContainer();
	extern void gDeallocateGraphicsResourceBindingContainer(GraphicsResourceBindingContainer* const inContainer);
	//

	// compute-specific resource container
	class ComputeResourceBindingContainer : public ResourceBindingContainer {
	public:
		ComputeResourceBindingContainer();
		~ComputeResourceBindingContainer();

	private:

	};
	
}