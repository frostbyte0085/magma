#include "Mesh.hpp"
#include "../Buffer.hpp"
#include "../Graphics.hpp"
#include <Application/Application.hpp>

using namespace Magma;

Mesh::Mesh(const std::string& name) : m_Name(name) {

}

Mesh::~Mesh() {
	SDELETE(m_VB);
	SDELETE(m_IB);

	for (auto& kvp : m_Children) {
		SDELETE(kvp.second);
	}

	m_Children.clear();
}

Mesh* Mesh::AddSubMesh(const std::string& name) {
	assert(GetSubMesh(name) == nullptr);

	Mesh* m = new Mesh(name);
	m_Children[name] = m;
	return m;
}

Mesh* Mesh::GetSubMesh(const std::string& name) const {
	auto it = m_Children.find(name);
	if (it != m_Children.end()) {
		return it->second;
	}

	return nullptr;
}

Mesh* Mesh::GetSubMesh(size_t index) const {
	assert((index >= 0) && (index < m_Children.size()));

	uint32_t i = 0;
	for (auto &it : m_Children) {
		if (i++ == index) return it.second;
	}

	return nullptr;
}

void Mesh::DestroySubMesh(const std::string& name) {
	auto it = m_Children.find(name);
	if (it != m_Children.end()) {
		SDELETE(it->second);
		m_Children.erase(it);
	}
}

void Mesh::DestroySubMesh(Mesh* const mesh) {
	assert(mesh);	
	DestroySubMesh(mesh->GetName());
}

void Mesh::prepare(void* initialVertexData, size_t vertexDataSize, void* initialIndexData, size_t indexDataSize, EMeshFlags meshFlags) {
	SDELETE(m_VB);
	SDELETE(m_IB);

	m_Flags = meshFlags;

	// vertex buffer
	D3D11_USAGE usage = D3D11_USAGE_IMMUTABLE;
	uint32_t cpu_flags = 0U;

	void* vb_initial_data = nullptr;
	if (initialVertexData) {
		D3D11_SUBRESOURCE_DATA data;
		data.pSysMem = initialVertexData;

		vb_initial_data = &data;
		
		if (flags(meshFlags & EMeshFlags::DYNAMIC_VB)) {
			usage = D3D11_USAGE_DYNAMIC;
			cpu_flags = D3D11_CPU_ACCESS_WRITE;
		}

	}

	gGfx->CreateBuffer(Buffer::sDescribe(&VertexBufferDesc(usage, (uint32_t)vertexDataSize, cpu_flags)), (D3D11_SUBRESOURCE_DATA*)vb_initial_data, (Buffer**)&m_VB);
	//

	// index buffer, optional
	if (indexDataSize != 0) {
		usage = D3D11_USAGE_IMMUTABLE;
		cpu_flags = 0U;

		void* ib_initial_data = nullptr;
		if (initialIndexData) {
			D3D11_SUBRESOURCE_DATA data;
			data.pSysMem = initialIndexData;

			if (flags(meshFlags & EMeshFlags::DYNAMIC_IB)) {
				usage = D3D11_USAGE_DYNAMIC;
				cpu_flags = D3D11_CPU_ACCESS_WRITE;
			}

			ib_initial_data = &data;
		}

		gGfx->CreateBuffer(Buffer::sDescribe(&IndexBufferDesc(usage, (uint32_t)indexDataSize, cpu_flags)), (D3D11_SUBRESOURCE_DATA*)ib_initial_data, (Buffer**)&m_IB);
		//
	}
}

void Mesh::UpdateVertexBuffer(const void* data, size_t dataSize) {
	assert(m_VB && (flags(m_Flags & EMeshFlags::DYNAMIC_VB)));

	m_VB->Fill(data, dataSize, D3D11_MAP_WRITE_DISCARD);
}

void Mesh::UpdateIndexBuffer(const void* data, size_t dataSize) {
	assert(m_IB && (flags(m_Flags & EMeshFlags::DYNAMIC_IB)));

	m_IB->Fill(data, dataSize, D3D11_MAP_WRITE_DISCARD);
}
