#include "RendererTasksAsync.hpp"
#include <Systems/Scene/Components/RenderProvider.hpp>
#include <TaskManager/TaskManager.hpp>

void Magma::gCullTask(Task* inTask, const void* inData) {
	const RenderTaskData* data = reinterpret_cast<const RenderTaskData*>(inData);

	data->Provider->Cull(*data->View);
}

void Magma::gBuildContextsTask(Task* inTask, const void* inData) {
	const RenderTaskData* data = reinterpret_cast<const RenderTaskData*>(inData);

	data->Provider->BuildContexts(*data->View);
}