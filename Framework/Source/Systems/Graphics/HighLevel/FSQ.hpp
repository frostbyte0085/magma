#pragma once

#include "../RenderTarget.hpp"
#include "../ResourceContainer.hpp"

namespace Magma {
	class FSQ : public NonCopyable {
	public:
		FSQ();
		~FSQ();

		static void sBegin();
		void BeginInstance(Shader* const inShader, RenderTarget* const inMRT);
		void EndInstance();
		static void sEnd();

		void ClearSRVBindings();
		void ClearSamplerBindings();

		void SetSRVBinding(const std::string& inName, ShaderResourceView* const inSRV, EShaderType inShaderStage = EShaderType::SHADER_TYPE_GRAPHICS);		
		void SetSamplerBinding(const std::string& inName, SamplerState* const inSampler, EShaderType inShaderStage = EShaderType::SHADER_TYPE_GRAPHICS);
		void Render();

		GraphicsResourceBindingContainer ResourceBindings;
	private:
		static RenderTarget* sPreviousRenderTarget;

		RenderTarget* m_MRT = nullptr;
		Shader* m_Shader = nullptr;
		CommandList* m_CmdList = nullptr;
	};
}