#pragma once

namespace Magma {
	
	class RenderProvider;
	class SceneView;
	struct Task;

	struct RenderTaskData {
		RenderProvider* Provider = nullptr;
		SceneView* View = nullptr;
	};

	// entry points
	extern void gCullTask(Task* inTask, const void* inData);
	extern void gBuildContextsTask(Task* inTask, const void* inData);
}