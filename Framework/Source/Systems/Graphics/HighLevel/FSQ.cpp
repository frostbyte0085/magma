#include "FSQ.hpp"
#include "../RenderTarget.hpp"
#include "../Shader.hpp"
#include "../RenderPipelineSetup.hpp"
#include "Renderer.hpp"
#include <Application/Application.hpp>

using namespace Magma;

RenderTarget* Magma::FSQ::sPreviousRenderTarget = nullptr;

FSQ::FSQ() {
	
}

FSQ::~FSQ() {
	
}

void FSQ::sBegin() {
	sPreviousRenderTarget = gGfx->GetMRT();
}

void FSQ::BeginInstance(Shader* const inShader, RenderTarget* const inMRT) {
	gAssert(inShader != nullptr, "Expected a valid Shader instance");

	m_Shader = inShader;
	m_MRT = inMRT;

	ClearSamplerBindings();
	ClearSRVBindings();
}

void FSQ::EndInstance() {

}

void FSQ::ClearSRVBindings() {	
	ResourceBindings.SRV.clear();
}

void FSQ::ClearSamplerBindings() {
	ResourceBindings.Samplers.clear();
}

void FSQ::SetSRVBinding(const std::string& inName, ShaderResourceView* const inSRV, EShaderType inShaderStage) {
	gAssert(m_Shader != nullptr, "Expected a valid Shader instance");

	ShaderBindingHandle* binding_handle = m_Shader->GetTextureHandle(inName);
	ResourceBindings.SRV.push_back(SRVBinding(inSRV, binding_handle, inShaderStage));
}

void FSQ::SetSamplerBinding(const std::string& inName, SamplerState* const inSampler, EShaderType inShaderStage) {
	gAssert(m_Shader != nullptr, "Expected a valid Shader instance");

	ShaderBindingHandle* binding_handle = m_Shader->GetSamplerHandle(inName);
	ResourceBindings.Samplers.push_back(SamplerBinding(inSampler, binding_handle, inShaderStage));
}

void FSQ::Render() {
	m_CmdList = &gRenderer->GetNextCommandList();

	m_CmdList->AddCommand<SetMRTCommand>(m_MRT);
	m_CmdList->AddCommand<BindResourcesCommand>(&ResourceBindings);
	m_CmdList->AddCommand<SetRenderPipelineSetupCommand>(RenderPipelineDesc(m_Shader, false, ERenderPipelineSetupFlags::RPS_CREATE_NONE));
	m_CmdList->AddCommand<DrawCommand>(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST, 3, 0);
	m_CmdList->AddCommand<SetMRTCommand>(sPreviousRenderTarget);
}

void FSQ::sEnd() {
	
}

