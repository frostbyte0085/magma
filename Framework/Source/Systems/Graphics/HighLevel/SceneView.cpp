#include "SceneView.hpp"
#include "Renderer.hpp"
#include "../Buffer.hpp"
#include <Systems/Scene/Components/CameraProvider.hpp>
#include <Systems/Scene/Components/Transform.hpp>
#include <Systems/Scene/Components/RenderProvider.hpp>
#include <Application/Application.hpp>
#include <TaskManager/TaskManager.hpp>
#include "RendererTasksAsync.hpp"
#include "SortedDrawableSetupContextContainer.hpp"

using namespace Magma;

SceneView::SceneView() {
	
}

SceneView::~SceneView() {
	SDELETE(m_PerFrameCB);
}

void SceneView::Initialise() {
	gGfx->CreateBuffer(Buffer::sDescribe(&ConstantBufferDesc(D3D11_USAGE_DYNAMIC, sizeof(m_PerFrameData), D3D11_CPU_ACCESS_WRITE)), nullptr, (Buffer**)&m_PerFrameCB);
}

void SceneView::Visit(RenderTarget* const inTarget, CommandList& inCmdList, CameraProvider* const inCamera, const std::vector<RenderProvider*>& inRenderProviders) {
	SetCamera(inCamera);
	m_CurrentGlobalCBCount = 0;

	CameraProvider& camera = *GetCamera();
	Transform* owner_transform = camera.GetComponent<Transform>();
	SetCameraTransform(owner_transform);

	m_PerFrameData.ViewMatrix = owner_transform->GetWorldMatrix().GetInverse();
	m_PerFrameData.ProjectionMatrix = Matrix4x4::Perspective(camera.GetHorizontalFOV(), camera.GetAspectRatio(), camera.GetNearPlane(), camera.GetFarPlane());
	m_PerFrameData.ViewProjectionMatrix = m_PerFrameData.ViewMatrix * m_PerFrameData.ProjectionMatrix;

	m_PerFrameData.InvViewMatrix = m_PerFrameData.ViewMatrix.GetInverse();
	m_PerFrameData.InvProjectionMatrix = m_PerFrameData.ProjectionMatrix.GetInverse();
	m_PerFrameData.InvViewProjectionMatrix = m_PerFrameData.InvViewMatrix * m_PerFrameData.InvProjectionMatrix;
	
	// update buffers cmd
	FillBufferCommand* cmd = inCmdList.AddCommand<FillBufferCommand>(m_PerFrameCB, (void*)&m_PerFrameData, sizeof(m_PerFrameData), D3D11_MAP_WRITE_DISCARD);

	// and the target mrt cmd
	inCmdList.AddCommand<SetMRTCommand>(inTarget);

	// update global per-view constant buffer bindings
	m_GlobalCBs[m_CurrentGlobalCBCount++] = ConstantBufferBinding(GetPerFrameCB(), PER_FRAME_CB_NAME, EShaderType::SHADER_TYPE_ALL);

	// update frustum
	m_Frustum.Calculate(m_PerFrameData.ViewMatrix, m_PerFrameData.ProjectionMatrix);

	// TODO: replace with scratch or stack allocator
	RenderTaskData task_data[256];

	uint32_t idx = 0;
	
	TaskId root_task = gTaskMgr->CreateEmptyTask();
	gTaskMgr->AddTask(root_task);

	for (auto render_provider: inRenderProviders) {
		RenderTaskData& td = task_data[idx];

		td.Provider = render_provider;
		td.View = this;

		TaskId cull_task = gTaskMgr->CreateTask(&gCullTask, &td, root_task);
		TaskId build_task = gTaskMgr->CreateTask(&gBuildContextsTask, &td, cull_task);
		
		gTaskMgr->AddTask(cull_task);
		gTaskMgr->AddTask(build_task);
		++idx;
	}
	
	gTaskMgr->SignalWork();

	gTaskMgr->WaitOnTask(root_task);

	const SortedDrawableSetupContextContainer& sorted_container = gGetSortedDrawableSetupContextContainer();

	// build command list	
	buildCommandList(inCmdList, sorted_container);

	// reset mrt cmd
	inCmdList.AddCommand<SetMRTCommand>(nullptr);
}

void SceneView::buildCommandList(CommandList& inCmdList, const SortedDrawableSetupContextContainer& inContainer) {
	for (uint32_t i = 0; i<inContainer.GetSortCommandCount(); i++) {
		DrawableSetupContextSortCommand& sort_cmd = inContainer.GetSortCommand(i);
		DrawableSetupContext* context = sort_cmd.Context;

		DrawableSetup* ds = context->GetDrawableSetup(sort_cmd.Offset);

		CopyGlobalsToResourceContainer(*ds->ResourceBindings);
		ds->BuildCommandList(inCmdList);
	}
}

void SceneView::CopyGlobalsToResourceContainer(GraphicsResourceBindingContainer& ioContainer) const {
	for (uint32_t i = 0; i < m_CurrentGlobalCBCount; i++) {
		ioContainer.CB.push_back(m_GlobalCBs[i]);
	}
}