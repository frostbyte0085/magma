#include "DrawableSetup.hpp"
#include "Renderer.hpp"
#include "../ResourceContainer.hpp"
#include "../Buffer.hpp"
#include "../ConstantBufferSupport.hpp"
#include "../CommandList.hpp"
#include "../RenderPipelineSetup.hpp"
#include "../Shader.hpp"

#include <Application/Application.hpp>

using namespace Magma;

DrawableSetup::DrawableSetup() {
	ResourceBindings = new GraphicsResourceBindingContainer();

	gGfx->CreateBuffer(Buffer::sDescribe(&ConstantBufferDesc(D3D11_USAGE_DYNAMIC, sizeof(PerObjectBuffer), D3D11_CPU_ACCESS_WRITE)), nullptr, (Buffer**)&m_PerObjectBuffer);
}

DrawableSetup::~DrawableSetup() {
	SDELETE(m_PerObjectBuffer);
	SDELETE(ResourceBindings);
}

void DrawableSetup::BuildCommandList(CommandList& inCmdList) {	
	inCmdList.AddCommand<FillBufferCommand>(m_PerObjectBuffer, (void*)GetPerObjectBufferData(), sizeof(PerObjectBuffer), D3D11_MAP_WRITE_DISCARD);

	ResourceBindings->CB.emplace_back(m_PerObjectBuffer, GetShader()->GetCBufferHandle(PER_OBJECT_CB_NAME));

	// fill in invalid binding handles
	for (auto& cb : ResourceBindings->CB) {
		if (!cb.BindingHandle) {
			cb.BindingHandle = GetShader()->GetCBufferHandle(cb.BindingName);
		}
	}
	
	inCmdList.AddCommand<SetRenderPipelineSetupCommand>(RenderPipelineDesc(GetShader(), true));
	inCmdList.AddCommand<BindResourcesCommand>(ResourceBindings);
	
	const D3D11_PRIMITIVE_TOPOLOGY topology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	if (ResourceBindings->IB.Buffer) {
		inCmdList.AddCommand<IndexedDrawCommand>(topology, IndexedDraw.IndexCount, IndexedDraw.StartIndexLocation, IndexedDraw.BaseVertexLocation);
	}
	else {
		inCmdList.AddCommand<DrawCommand>(topology, Draw.VertexCount, Draw.StartVertexLocation);
	}
}
