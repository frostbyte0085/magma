#pragma once

#include "DrawableSetup.hpp"
#include <Utilities/Sorting.hpp>

namespace Magma {
	static const uint32_t sMaxDrawableSetupsPerContext = 256;
	static const uint32_t sMaxContexts = 64;
	
	// The sort command contains a pointer to the actual context containing the DrawableSetup pointer at the specified offset.
	// By splitting data and sort key, we simplify the sort process.
	ALIGN16
	struct DrawableSetupContextSortCommand {
		class DrawableSetupContext* Context = nullptr;
		uint32_t Offset = 0;
		uint32_t SortKey = 0;
	};

	// The context contains 2 important arrays:
	// 1) The actual pointers to the DrawableSetup instances
	// 2) The sort key data associated with each setup.
	class DrawableSetupContext : public NonCopyable {
		friend DrawableSetupContextSortCommand;
	public:
		DrawableSetupContext();
		~DrawableSetupContext();

		void Reset();
		uint32_t Add(DrawableSetup* const inSetup);

		inline DrawableSetupContextSortCommand& GetSortCommand(uint32_t inIndex) { return m_SortCommands[inIndex]; }
		inline uint32_t GetSortCommandCount() const { return m_CurrentCount; }

		inline DrawableSetup* GetDrawableSetup(uint32_t inIndex) const { return m_Setups[inIndex]; }
		inline uint32_t GetDrawableSetupCount() const { return m_CurrentCount; }
		inline bool IsFull() const { return m_CurrentCount == sMaxDrawableSetupsPerContext; }

	private:
		DrawableSetup* m_Setups[sMaxDrawableSetupsPerContext];
		DrawableSetupContextSortCommand m_SortCommands[sMaxDrawableSetupsPerContext];

		uint32_t m_CurrentCount = 0;
		BasicMutex m_Mutex;
	};
		
	extern DrawableSetupContext& gGetAvailableDrawableSetupContext();
	extern void gResetCommandContexts();
}