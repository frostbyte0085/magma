#include "Renderer.hpp"
#include "FSQ.hpp"
#include "SortedDrawableSetupContextContainer.hpp"

#include <Application/Application.hpp>
#include <Systems/Window/Window.hpp>
#include <Systems/Graphics/Graphics.hpp>
#include <Systems/Graphics/Buffer.hpp>
#include <Systems/Graphics/Texture.hpp>
#include <Systems/Graphics/ShaderResourceView.hpp>

#include <Systems/Scene/Scene.hpp>
#include <Systems/Scene/Components/Transform.hpp>
#include <Systems/Scene/Components/CameraProvider.hpp>
#include <Systems/Scene/Components/RenderProvider.hpp>

#include <TaskManager/TaskManager.hpp>

#include "../DefaultResources.hpp"
#include "../CommandList.hpp"

#include "RendererTasksAsync.hpp"

using namespace Magma;

Renderer* Magma::gRenderer = nullptr;

FSQ gWipeFSQ;

Renderer::Renderer() {

}

Renderer::~Renderer() {
	
	for (uint32_t i = 0; i < (uint32_t)ECommandListCategory::MAX_TYPES; ++i) {
		SDELETE(m_CmdLists[i]);
	}	
}

void Renderer::Initialise() {
	
	for (uint32_t i = 0; i < sMaxViews; ++i) {
		m_Views[i].Initialise();
	}

	for (uint32_t i = 0; i < (uint32_t)ECommandListCategory::MAX_TYPES; ++i) {
		m_CmdLists[i] = new CommandList(4096);
	}

	RenderTarget* backbuffer_target = gGfx->GetBackbufferTarget();

	m_GBuffer.Initialise(3, backbuffer_target->GetWidth(), backbuffer_target->GetHeight());
	m_GBuffer.SetColorTarget(0, DXGI_FORMAT_R8G8B8A8_UNORM); // RGB (Albedo) A (Metalness)
	m_GBuffer.SetColorTarget(1, DXGI_FORMAT_R16G16_UNORM); // RG (Normals)
	m_GBuffer.SetColorTarget(2, DXGI_FORMAT_R10G10B10A2_UNORM); // R (Roughness) GBA (Free)

	m_GBuffer.SetDepthTarget(backbuffer_target->GetDepthTarget().DSV, nullptr);
}

CommandList& Renderer::GetNextCommandList() {
	gAssert(m_CurrentCmdList < sMaxCmdLists, "Max command lists reached! Increase cap?");
	return *m_CmdLists[m_CurrentCmdList++];
}

void Renderer::ResetCommandLists() {
	m_CurrentCmdList = 0;
}

void Renderer::Begin() {
	ResetCommandLists();
	gResetCommandContexts();
}

// sort test
void gTestContext() {
	DrawableSetupContext contexts[2];
	size_t contexts_size = _countof(contexts);

	DrawableSetup setups[64];
	size_t setups_size = _countof(setups);

	std::random_device rd; // obtain a random number from hardware
	std::mt19937 eng(rd()); // seed the generator
	std::uniform_int_distribution<> distr(0, 1000); // define the range

	SortedDrawableSetupContextContainer sorted_context_container;

	for (uint32_t i = 0; i < contexts_size; i++) {
		uint32_t step = setups_size / contexts_size;
		uint32_t start = i * step;
		uint32_t end = (i + 1) * step;

		for (size_t s_idx = start; s_idx < end; s_idx++) {
			setups[s_idx].SetSortKey(distr(eng));

			contexts[i].Add(&setups[s_idx]);
		}

		sorted_context_container.Add(&contexts[i]);
	}
	
	sorted_context_container.Sort();

}

void gEmptyRoot(Task* inTask, const void* inData) {
	gTrace("PROCESSING ROOT");
}

void gDep1(Task* inTask, const void* inData) {
	gTrace("PROCESSING TASK DEP 1");
}

void gDep2(Task* inTask, const void* inData) {
	gTrace("PROCESSING TASK DEP 2");
}

void gTestTasks() {
	gTrace("Start work");
	TaskId root = gTaskMgr->CreateTask(&gEmptyRoot, nullptr, sTaskNoParent);

	TaskId dep1 = gTaskMgr->CreateTask(&gDep1, nullptr, root);
	TaskId dep2 = gTaskMgr->CreateTask(&gDep2, nullptr, dep1);
	
	Task* rootptr = gGetTaskPtr(dep1);

	gTaskMgr->AddTask(root);
	gTaskMgr->AddTask(dep1);
	gTaskMgr->AddTask(dep2);	
	gTaskMgr->SignalWork();

	gTaskMgr->WaitOnTask(root);

	gTrace("Finished both tasks");
}

void Renderer::renderCamera(CameraProvider* const inCamera) {
	//gTestTasks();

	//return;

	gAssert(inCamera != nullptr, "Expected a valid Camera instance");
		
	gAssert(m_CurrentViewIndex < sMaxViews, "Max views reached");
	

	// collect drawables
	std::vector<RenderProvider*>& render_providers = gScene->GetRootNode()->GetComponentsInChildren<RenderProvider>();

	// process main camera view
	SceneView& view = m_Views[m_CurrentViewIndex];
	view.Visit(&m_GBuffer, GetNextCommandList(), inCamera, render_providers);
	
	// render all the passes
	//GBufferPass(view, sorted_container);

	// advance to the next available SceneView index
	++m_CurrentViewIndex;
}

void Renderer::End() {
	m_CurrentViewIndex = 0;

	SceneNode* main_provider_node = gScene->GetMainCameraProviderNode();
	assert(main_provider_node);

	renderCamera(main_provider_node->GetComponent<CameraProvider>());
	
	m_ScreenWipe = EScreenWipe::NORMALS;

	// debug buffer view
	if (m_ScreenWipe != EScreenWipe::NONE) {
		FSQ::sBegin();
		
		Shader* wipe_shader = m_ScreenWipeShader[(uint32_t)m_ScreenWipe];
		ShaderResourceView* wipe_srv = nullptr;
		SamplerState* wipe_sampler = nullptr;

		getScreenWipeSRV(m_ScreenWipe, &wipe_srv, &wipe_sampler);
		
		gAssert( (wipe_srv != nullptr) && (wipe_shader != nullptr) && (wipe_sampler != nullptr), "Invalid screenwipe type!");
				
		gWipeFSQ.BeginInstance(wipe_shader, nullptr);
		gWipeFSQ.SetSRVBinding("Texture", wipe_srv);
		gWipeFSQ.SetSamplerBinding("Sampler", wipe_sampler);
		gWipeFSQ.Render();
		gWipeFSQ.EndInstance();

		FSQ::sEnd();
	}

	gGfx->ExecuteCommandLists(m_CmdLists, m_CurrentCmdList);
}

void Renderer::getScreenWipeSRV(EScreenWipe inWipe, ShaderResourceView** outSRV, SamplerState** outSampler) {
	switch (inWipe) {
	case EScreenWipe::ALBEDO:
	case EScreenWipe::METALNESS:
		*outSRV = m_GBuffer.GetColorTarget(0).SRV;
		*outSampler = gPointSampler;
		break;

	case EScreenWipe::NORMALS:
		*outSRV = m_GBuffer.GetColorTarget(1).SRV;
		*outSampler = gPointSampler;
		break;

	case EScreenWipe::ROUGHNESS:
		*outSRV = m_GBuffer.GetColorTarget(2).SRV;
		*outSampler = gPointSampler;
		break;

	default:break;
	}
}