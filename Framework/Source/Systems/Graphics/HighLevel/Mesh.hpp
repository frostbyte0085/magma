#pragma once

#include <Systems/Graphics/Buffer.hpp>
#include <Systems/Resources/MeshResource.hpp>

namespace Magma {
	
	class Mesh {
	public:
		Mesh(const std::string& name);
		virtual ~Mesh();

		const std::string& GetName() const { return m_Name; }

		Mesh* AddSubMesh(const std::string& name);
		Mesh* GetSubMesh(const std::string& name) const;
		Mesh* GetSubMesh(size_t index) const;
		void DestroySubMesh(const std::string& name);
		void DestroySubMesh(Mesh* const mesh);
		size_t GetSubMeshCount() const { return m_Children.size(); }

		template <class VertexType, class IndexType>
		void PrepareMeshData(VertexType* inVertices, uint32_t inVertexCount, IndexType* inIndices, uint32_t inIndexCount, EMeshFlags inFlags, const std::vector<Vector3> inPositions) {

			m_VertexCount = inVertexCount;
			m_IndexCount = inIndexCount;

			m_VertexDataStructureSize = sizeof(inVertices[0]);
			m_IndexDataTypeSize = sizeof(inIndices[0]);

			prepare((void*)inVertices, m_VertexDataStructureSize * m_VertexCount, (void*)inIndices, m_IndexDataTypeSize * m_IndexCount, inFlags);
			m_LocalBoundingSphere = Sphere(&inPositions[0], static_cast<uint32_t>(inPositions.size()) );
		}

		void UpdateVertexBuffer(const void* data, size_t dataSize);
		void UpdateIndexBuffer(const void* data, size_t dataSize);
				
		// returns how many vertices we have
		inline uint32_t GetVertexCount() const { return m_VertexCount; }
		// returns the size of the vertex structure
		inline size_t GetVertexDataStructureSize() const { return m_VertexDataStructureSize; }
		// returns how many indices we have
		inline uint32_t GetIndexCount() const { return m_IndexCount; }
		// returns the size of the index data type
		inline size_t GetIndexDataTypeSize() const { return m_IndexDataTypeSize; }

		inline VertexBuffer* const GetVertexBuffer() const { assert(m_VB); return m_VB; }
		inline IndexBuffer* const GetIndexBuffer() const { assert(m_IB); return m_IB; }

		inline const Sphere& GetLocalBoundingSphere() const { return m_LocalBoundingSphere; }

	private:
		void prepare(void* initialVertexData, size_t vertexDataSize, void* initialIndexData, size_t indexDataSize, EMeshFlags flags);

		std::string m_Name = "";

		VertexBuffer* m_VB = nullptr;
		IndexBuffer* m_IB = nullptr;
		
		uint32_t m_VertexCount = 0;
		uint32_t m_IndexCount = 0;

		size_t m_VertexDataStructureSize = 0;
		size_t m_IndexDataTypeSize = 0;

		Sphere m_LocalBoundingSphere;
		
		EMeshFlags m_Flags = EMeshFlags::NONE;

		std::unordered_map<std::string, Mesh*> m_Children;
	};
}