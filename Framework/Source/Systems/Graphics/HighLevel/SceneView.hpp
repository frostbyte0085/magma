#pragma once

#include <Systems/Scene/Enumerations.hpp>
#include "../ConstantBufferSupport.hpp"
#include "../Bindings.hpp"
#include "../Graphics.hpp"

namespace Magma {
	static const uint32_t sMaxGlobalCBs = 8;

	class SceneView : public NonCopyable {
	public:
		SceneView();
		~SceneView();

		void Visit(RenderTarget* const inTarget, CommandList& inCmdList, CameraProvider* const inCamera, const std::vector<RenderProvider*>& inRenderProviders);
		void Initialise();

		void CopyGlobalsToResourceContainer(GraphicsResourceBindingContainer& ioContainer) const;
		inline ConstantBuffer* const GetPerFrameCB() const { return m_PerFrameCB; }

		inline void SetCamera(CameraProvider* const inCamera) { m_Camera = inCamera; }
		inline CameraProvider* const GetCamera() const { return m_Camera; }

		inline void SetCameraTransform(Transform* const inTransform) { m_CameraTransform = inTransform; }
		inline Transform* const GetCameraTransform() const { return m_CameraTransform; }

		inline const Frustum& GetFrustum() const { return m_Frustum; }

	private:
		void buildCommandList(CommandList& inCmdList, const SortedDrawableSetupContextContainer& inContainer);

		// per view buffers
		ConstantBuffer* m_PerFrameCB = nullptr;
		PerFrameBuffer m_PerFrameData;

		CameraProvider* m_Camera = nullptr;
		Transform* m_CameraTransform = nullptr;

		ConstantBufferBinding m_GlobalCBs[sMaxGlobalCBs];
		uint32_t m_CurrentGlobalCBCount = 0;

		Frustum m_Frustum;
	};
}