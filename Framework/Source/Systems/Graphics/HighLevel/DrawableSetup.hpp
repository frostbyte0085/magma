#pragma once

#include "../Graphics.hpp"

namespace Magma {
	class Shader;

	struct DrawDesc {
		DrawDesc(uint32_t inVertexCount, uint32_t inStartVertexLocation) : VertexCount(inVertexCount), StartVertexLocation(inStartVertexLocation) {}

		uint32_t VertexCount = 0;
		uint32_t StartVertexLocation = 0;
	};

	struct IndexedDrawDesc {
		IndexedDrawDesc(uint32_t inIndexCount, uint32_t inStartIndexLocation, uint32_t inBaseVertexLocation) : IndexCount(inIndexCount), StartIndexLocation(inStartIndexLocation), BaseVertexLocation(inBaseVertexLocation) {}

		uint32_t IndexCount = 0;
		uint32_t StartIndexLocation = 0;
		uint32_t BaseVertexLocation = 0;
	};

	class DrawableSetup : public NonCopyable {
	public:
		DrawableSetup();
		~DrawableSetup();

		void BuildCommandList(CommandList& inCmdList);

		// drawable properties and data
		inline void SetShader(Shader* const inShader) { m_Shader = inShader; }
		inline Shader* const GetShader() const { return m_Shader; }

		inline void SetPerObjectBufferData(PerObjectBuffer* const inBufferData) { m_PerObjectBufferData = inBufferData; }
		inline PerObjectBuffer* GetPerObjectBufferData() const { return m_PerObjectBufferData; }

		inline void SetLocalBoundingSphere(const Sphere& inSphere) { m_LocalBoundingSphere = inSphere; }
		inline const Sphere& GetLocalBoundingSphere() const { return m_LocalBoundingSphere; }

		inline void SetWorldBoundingSphere(const Sphere& inSphere) { m_WorldBoundingSphere = inSphere; }
		inline const Sphere& GetWorldBoundingSphere() const { return m_WorldBoundingSphere; }

		inline void SetSortKey(uint32_t inKey) { m_SortKey = inKey; }
		inline uint32_t GetSortKey() const { return m_SortKey; }

		GraphicsResourceBindingContainer* ResourceBindings = nullptr;

		union {
			DrawDesc Draw;
			IndexedDrawDesc IndexedDraw;
		};

		inline void SetCulled() { m_Flags |= (EDrawableSetupFlags::CULLED); }
		inline bool IsCulled() const { return flags(m_Flags & EDrawableSetupFlags::CULLED); }

	private:		
		ConstantBuffer* m_PerObjectBuffer = nullptr;
		Shader* m_Shader = nullptr;
		PerObjectBuffer* m_PerObjectBufferData = nullptr;
		Sphere m_LocalBoundingSphere;
		Sphere m_WorldBoundingSphere;

		uint32_t m_SortKey;
		EDrawableSetupFlags m_Flags = EDrawableSetupFlags::NONE;
	};
}