#pragma once

#include "DrawableSetupContext.hpp"

namespace Magma {

	static constexpr uint32_t sGetMaxMergedSortCommands() { return sMaxDrawableSetupsPerContext * sMaxContexts; }

	// takes an array of DrawableSetupContext instances and sorts them by their DrawableSetupContextSortCommand
	// merges the sort commands in one array and keeps track of the individual pointers to the DrawableSetup instances
	class SortedDrawableSetupContextContainer : public NonCopyable {
	public:
		SortedDrawableSetupContextContainer();
		~SortedDrawableSetupContextContainer();

		void Reset();
		uint32_t Add(DrawableSetupContext* const inContext);
		inline DrawableSetupContextSortCommand& GetSortCommand(uint32_t inIndex) const { return *m_SortCommands[inIndex]; }
		inline uint32_t GetSortCommandCount() const { return m_SortCommandCount; }
		void Sort();

	private:
		DrawableSetupContextSortCommand* m_SortCommands[sGetMaxMergedSortCommands()];
		uint32_t m_SortCommandCount = 0;
	};
		
	// Returns a populated and sorted SortedDrawableSetupContextContainer reference
	// It will contain a sorted array of all the populated DrawableSetupContext instances for the current SceneView instance	
	extern SortedDrawableSetupContextContainer& gGetSortedDrawableSetupContextContainer();
}