#pragma once

#include "../RenderTarget.hpp"
#include "../CommandList.hpp"
#include "SceneView.hpp"
#include <Systems/Scene/Enumerations.hpp>
#include "../Enumerations.hpp"

namespace Magma {

	static const uint16_t sMaxCmdLists = 256;

	class Renderer : public NonCopyable {
		friend class Graphics;
	public:
		~Renderer();

		void Initialise();
		
		void Begin();
		void End();

		inline RenderTarget& GetGBuffer() { return m_GBuffer; }
		CommandList& GetNextCommandList();
		void ResetCommandLists();

		inline void UpdateScreenWipeShaders(Shader* const inShaders[(uint32_t)EScreenWipe::MAX_TYPES]) {
			for (uint32_t i = 0; i < sGetMaxScreenWipes(); i++) {
				m_ScreenWipeShader[i] = inShaders[i];
			}
		}

		static uint32_t sGetMaxScreenWipes() {
			return (uint32_t)EScreenWipe::MAX_TYPES;
		}

		inline void ResetScreenWipe() { m_ScreenWipe = EScreenWipe::NONE; }
		inline void SetScreenWipe(EScreenWipe inWipe) { m_ScreenWipe = inWipe; }

	private:
		static const uint32_t sMaxViews = 32;

		Renderer();

		void renderCamera(CameraProvider* const inCamera);

		RenderTarget m_GBuffer;

		// SceneViews
		SceneView m_Views[sMaxViews];
		std::atomic_uint32_t m_CurrentViewIndex = 0;

		// command lists
		CommandList* m_CmdLists[sMaxCmdLists];
		uint16_t m_CurrentCmdList;

		// debug - screenwipe
		EScreenWipe m_ScreenWipe = EScreenWipe::NONE;
		Shader* m_ScreenWipeShader[(uint32_t)EScreenWipe::MAX_TYPES];
		void getScreenWipeSRV(EScreenWipe inWipe, ShaderResourceView** outSRV, SamplerState** outSampler);
	};

	extern Renderer* gRenderer;
}