#include "DrawableSetupContext.hpp"
#include "SortedDrawableSetupContextContainer.hpp"
#include <Utilities/Mutex.hpp>

using namespace Magma;

namespace Magma {	
	DrawableSetupContext gContexts[sMaxContexts];
	SortedDrawableSetupContextContainer gCurrentSortedContext;
	std::atomic_uint32_t gCurrentContextIndex = 0;

	DrawableSetupContext& gGetAvailableDrawableSetupContext() {
		do {
			gCurrentContextIndex++;
			gCurrentContextIndex = gCurrentContextIndex % sMaxContexts;
		} while (gContexts[gCurrentContextIndex].IsFull());

		DrawableSetupContext& context = gContexts[gCurrentContextIndex];		
		return context;
	}

	void gResetCommandContexts() {
		for (uint32_t i = 0; i < sMaxContexts; i++) {
			gContexts[i].Reset();
		}
	}

	SortedDrawableSetupContextContainer& gGetSortedDrawableSetupContextContainer() {
		// populate the container and prepare it for sorting
		gCurrentSortedContext.Reset();

		for (uint32_t i = 0; i < sMaxContexts; i++) {
			gCurrentSortedContext.Add(&gContexts[i]);
		}

		// sort
		gCurrentSortedContext.Sort();

		return gCurrentSortedContext;
	}
}

DrawableSetupContext::DrawableSetupContext() {

}

DrawableSetupContext::~DrawableSetupContext() {

}

void DrawableSetupContext::Reset() {
	for (uint32_t i = 0; i < m_CurrentCount; i++) {
		m_Setups[i] = nullptr;
		m_SortCommands[i].Context = nullptr;
	}

	m_CurrentCount = 0;
}

uint32_t DrawableSetupContext::Add(DrawableSetup* const inSetup) {	
	if (!inSetup->IsCulled()) {
		ScopedLock<BasicMutex> lock(&m_Mutex);

		DrawableSetupContextSortCommand& sort_cmd = m_SortCommands[m_CurrentCount];
		sort_cmd.Context = this;
		sort_cmd.Offset = m_CurrentCount;
		sort_cmd.SortKey = inSetup->GetSortKey();

		m_Setups[m_CurrentCount] = inSetup;

		++m_CurrentCount;
	}

	return m_CurrentCount;
}