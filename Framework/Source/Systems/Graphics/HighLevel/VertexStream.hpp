#pragma once

#include "../Buffer.hpp"

namespace Magma {

	class VertexStream {
	public:
		VertexStream();
		~VertexStream();

		
	private:
		EVertexStreamChannel m_Channels;
		VertexBuffer* m_Buffer;
	};
}