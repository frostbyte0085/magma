#include "SortedDrawableSetupContextContainer.hpp"

using namespace Magma;

SortedDrawableSetupContextContainer::SortedDrawableSetupContextContainer() {

}

SortedDrawableSetupContextContainer::~SortedDrawableSetupContextContainer() {

}

void SortedDrawableSetupContextContainer::Reset() {
	for (uint32_t i=0; i<m_SortCommandCount; i++) {
		m_SortCommands[i] = nullptr;
	}

	m_SortCommandCount = 0;
}

uint32_t  SortedDrawableSetupContextContainer::Add(DrawableSetupContext* const inContext) {
	gAssert(inContext != nullptr, "Expected a valid DrawableSetupContext instance");

	for (uint32_t i = 0; i < inContext->GetSortCommandCount(); i++) {
		DrawableSetupContextSortCommand& sort_cmd = inContext->GetSortCommand(i);

		m_SortCommands[m_SortCommandCount++] = &sort_cmd;
	}

	return m_SortCommandCount;
}

void SortedDrawableSetupContextContainer::Sort() {
	std::sort(m_SortCommands, m_SortCommands + m_SortCommandCount, [](const DrawableSetupContextSortCommand* inLHS, const DrawableSetupContextSortCommand* inRHS) {
		return inLHS->SortKey < inRHS->SortKey;
	});
}