#include "../../Application/Application.hpp"
#include "RenderTargetView.hpp"
#include "Texture.hpp"

using namespace Magma;

RenderTargetView::RenderTargetView(ID3D11RenderTargetView* const view) : m_RTV(view) {

}

RenderTargetView::~RenderTargetView() {
	SRELEASE(m_RTV);
}
