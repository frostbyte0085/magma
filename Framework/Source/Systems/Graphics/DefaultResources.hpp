#pragma once

#include "SamplerState.hpp"

namespace Magma {
	extern void gCreateDefaultGfxResources();
	extern void gDestroyDefaultGfxResources();

	// Samplers
	extern SamplerState* gPointSampler;
	extern SamplerState* gLinearSampler;
}