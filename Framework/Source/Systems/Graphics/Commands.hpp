#pragma once

#include "Graphics.hpp"
#include "ConstantBufferSupport.hpp"
#include "ResourceContainer.hpp"
#include "RenderTarget.hpp"
#include "RenderPipelineSetup.hpp"
#include <Systems/Allocators.hpp>

namespace Magma {
	
	class ICommand {
	public:
		ICommand() {}
		virtual ~ICommand() {}

		virtual void Execute() = 0;

		template<class T, class... Args>
		static T* Allocate(Args... inArgs) {
			//return gScratchAlloc->AllocateType<T>(inArgs...);
			return new T(inArgs...);
		}

		static void Deallocate(ICommand* inCmd) {
			gAssert(inCmd != nullptr, "Expected a valid ICommand instance");
			//gScratchAlloc->FreeType(inCmd);
			delete inCmd;
		}
	};

	class EmptyCommand : public ICommand {
	public:
		EmptyCommand(const std::string& inTestString);
		~EmptyCommand();

		virtual void Execute() override;

	private:
		std::string m_TestString;
	};

	class BindResourcesCommand : public ICommand {
	public:
		BindResourcesCommand() {}
		BindResourcesCommand(GraphicsResourceBindingContainer* const inBindings) : ResourceBindings(inBindings) {}

		GraphicsResourceBindingContainer* ResourceBindings = nullptr;

		virtual void Execute() override;
	};

	class SetRenderPipelineSetupCommand : public ICommand {
	public:
		SetRenderPipelineSetupCommand() {}
		SetRenderPipelineSetupCommand(const RenderPipelineDesc& inRPSDesc);

		RenderPipelineSetup* RPS = nullptr;

		virtual void Execute() override;
	};

	class FillBufferCommand : public ICommand {
	public:
		FillBufferCommand() {}
		FillBufferCommand(Buffer *inBuffer, const void* inData, size_t inSize, D3D11_MAP inMapType, uint32_t inMapFlags = 0, uint32_t inSubresource = 0) :
			TargetBuffer(inBuffer), Data(inData), Size(inSize), MapType(inMapType), MapFlags(inMapFlags), SubResource(inSubresource) {}

		D3D11_MAP MapType = D3D11_MAP_WRITE;
		const void* Data = nullptr;
		Buffer* TargetBuffer = nullptr;		
		uint32_t SubResource = 0;
		uint32_t MapFlags = 0;
		size_t Size = 0;

		virtual void Execute() override;
	};

	class DrawCommandBase : public ICommand {
	public:
		DrawCommandBase() {}
		DrawCommandBase(D3D11_PRIMITIVE_TOPOLOGY inTopology) : Topology(inTopology) {}
		
		D3D11_PRIMITIVE_TOPOLOGY Topology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

		virtual void Execute() override;
	};

	class DrawCommand : public DrawCommandBase {
	public:
		DrawCommand() {}
		DrawCommand(D3D11_PRIMITIVE_TOPOLOGY inTopology, uint32_t vertexCount, uint32_t startVertexLocation) : 
			DrawCommandBase(inTopology), VertexCount(vertexCount), StartVertexLocation(startVertexLocation) {}
				
		uint32_t VertexCount = 0;
		uint32_t StartVertexLocation = 0;

		virtual void Execute() override;
	};

	class IndexedDrawCommand : public DrawCommandBase {
	public:
		IndexedDrawCommand() {}
		IndexedDrawCommand(D3D11_PRIMITIVE_TOPOLOGY inTopology, uint32_t indexCount, uint32_t startIndexLocation, uint32_t baseVertexLocation) : 
			DrawCommandBase(inTopology), IndexCount(indexCount), StartIndexLocation(startIndexLocation), BaseVertexLocation(baseVertexLocation) {}

		uint32_t IndexCount = 0;
		uint32_t StartIndexLocation = 0;
		uint32_t BaseVertexLocation = 0;

		virtual void Execute() override;
	};

	class SetMRTCommand : public ICommand {
	public:
		SetMRTCommand() {}
		SetMRTCommand(RenderTarget* const inMRT) : MRT(inMRT) {}

		RenderTarget* MRT = nullptr;

		virtual void Execute() override;
	};

	class ClearCommand : public ICommand {
	public:
		ClearCommand() {}
		ClearCommand(EClearFlag inClearFlags, const RenderTargetClear& inClear) : Clear(inClear), Flags(inClearFlags) {}

		RenderTargetClear Clear;
		EClearFlag Flags;

		virtual void Execute() override;
	};

}