#pragma once

#include "Graphics.hpp"

namespace Magma {
	class UnorderedAccessView : public NonCopyable {
		friend class Graphics;
	public:
		~UnorderedAccessView();

	private:
		UnorderedAccessView(ID3D11UnorderedAccessView* view);

		ID3D11UnorderedAccessView* m_View;
		D3D11_UNORDERED_ACCESS_VIEW_DESC m_Desc;
	};
}