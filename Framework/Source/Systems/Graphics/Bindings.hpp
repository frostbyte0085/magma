#include "Graphics.hpp"

#pragma once

namespace Magma {
#define PER_FRAME_CB_NAME "gPerFrameCB"
#define PER_OBJECT_CB_NAME "gPerObjectCB"

	class ResourceBinding {
	public:
		ResourceBinding() {}
		ResourceBinding(ShaderBindingHandle* const inBindingHandle, EShaderType inShaderStage = EShaderType::SHADER_TYPE_GRAPHICS);
		ResourceBinding(const std::string& inBindingName, EShaderType inShaderStage = EShaderType::SHADER_TYPE_GRAPHICS);
		
		ResourceBinding(const ResourceBinding& other) {
			*this = other;
		}

		ResourceBinding& operator=(const ResourceBinding& other) {
			if (this != &other) {
				ShaderStage = other.ShaderStage;
				BindingHandle = other.BindingHandle;
				BindingName = other.BindingName;
			}

			return *this;
		}

		EShaderType ShaderStage = EShaderType::SHADER_TYPE_GRAPHICS;
		ShaderBindingHandle* BindingHandle = nullptr;
		std::string BindingName = "";
	};

	class SRVBinding : public ResourceBinding {
	public:
		SRVBinding() {}
		SRVBinding(ShaderResourceView* inSRV, ShaderBindingHandle* const inBindingHandle, EShaderType inShaderStage = EShaderType::SHADER_TYPE_GRAPHICS);
		SRVBinding(ShaderResourceView* inSRV, const std::string& inBindingName, EShaderType inShaderStage = EShaderType::SHADER_TYPE_GRAPHICS);
		
		SRVBinding(const SRVBinding& other) : ResourceBinding(other) {
			*this = other;
		}

		SRVBinding& operator=(const SRVBinding& other) {
			ResourceBinding::operator=(other);

			if (this != &other) {
				SRV = other.SRV;
			}

			return *this;
		}
		
		ShaderResourceView* SRV = nullptr;		
	};

	class SamplerBinding : public ResourceBinding {
	public:
		SamplerBinding() {}
		SamplerBinding(SamplerState* inSampler, ShaderBindingHandle* const inBindingHandle, EShaderType inShaderStage = EShaderType::SHADER_TYPE_GRAPHICS);
		SamplerBinding(SamplerState* inSampler, const std::string& inBindingName, EShaderType inShaderStage = EShaderType::SHADER_TYPE_GRAPHICS);
		
		SamplerBinding(const SamplerBinding& other) : ResourceBinding(other) {
			*this = other;
		}

		SamplerBinding& operator=(const SamplerBinding& other) {
			ResourceBinding::operator=(other);

			if (this != &other) {
				Sampler = other.Sampler;
			}

			return *this;
		}

		SamplerState* Sampler = nullptr;
	};

	class ConstantBufferBinding : public ResourceBinding {
	public:
		ConstantBufferBinding() {}
		ConstantBufferBinding(ConstantBuffer* inBuffer, ShaderBindingHandle* const inBindingHandle, EShaderType inShaderStage = EShaderType::SHADER_TYPE_GRAPHICS);
		ConstantBufferBinding(ConstantBuffer* inBuffer, const std::string& inBindingName, EShaderType inShaderStage = EShaderType::SHADER_TYPE_GRAPHICS);
				
		ConstantBufferBinding(const ConstantBufferBinding& other) : ResourceBinding(other) {
			*this = other;
		}

		ConstantBufferBinding& operator=(const ConstantBufferBinding& other) {
			ResourceBinding::operator=(other);

			if (this != &other) {
				Buffer = other.Buffer;
			}

			return *this;
		}

		ConstantBuffer* Buffer = nullptr;
	};

	class VertexBufferBinding {
	public:
		VertexBufferBinding() {}
		VertexBufferBinding(VertexBuffer* vb, size_t stride, uint32_t offset = 0) :Buffer(vb), Stride(stride), Offset(offset) { }
		
		VertexBufferBinding(const ConstantBufferBinding& other) {
			*this = other;
		}

		VertexBufferBinding& operator=(const VertexBufferBinding& other) {
			if (this != &other) {
				Buffer = other.Buffer;
				Stride = other.Stride;
				Offset = other.Offset;
			}

			return *this;
		}
		
		VertexBuffer* Buffer = nullptr;
		size_t Stride = 0;
		uint32_t Offset = 0;
	};

	class IndexBufferBinding {
	public:
		IndexBufferBinding() {}
		IndexBufferBinding(IndexBuffer* buffer, DXGI_FORMAT fmt = DXGI_FORMAT_R16_UINT, uint32_t offset = 0) : Buffer(buffer), Format(fmt), Offset(0) {}

		IndexBufferBinding(const ConstantBufferBinding& other) {
			*this = other;
		}

		IndexBufferBinding& operator=(const IndexBufferBinding& other) {
			if (this != &other) {
				Buffer = other.Buffer;
				Format = other.Format;
				Offset = other.Offset;
			}

			return *this;
		}
		
		IndexBuffer* Buffer = nullptr;
		DXGI_FORMAT Format = DXGI_FORMAT_R16_UINT;
		uint32_t Offset = 0;
	};
}
