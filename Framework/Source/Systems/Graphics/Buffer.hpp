#pragma once

#include "Graphics.hpp"

namespace Magma {
	
	struct BufferDesc {
		BufferDesc() {
			ZeroMemory(&Desc, sizeof(Desc));
		}

		virtual ~BufferDesc() {}

		D3D11_BUFFER_DESC Desc;
	};

	struct VertexBufferDesc : public BufferDesc {
		VertexBufferDesc() : BufferDesc() {}

		explicit VertexBufferDesc(D3D11_USAGE usage, uint32_t byteWidth, uint32_t cpuAccessFlags=0) {
			
			Desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

			Desc.CPUAccessFlags = cpuAccessFlags;
			Desc.ByteWidth = byteWidth;
			Desc.Usage = usage;			
		}
		
	};

	struct IndexBufferDesc : public BufferDesc {
		IndexBufferDesc() : BufferDesc() {}

		explicit IndexBufferDesc(D3D11_USAGE usage, uint32_t byteWidth, uint32_t cpuAccessFlags = 0) {

			Desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
			
			Desc.CPUAccessFlags = cpuAccessFlags;
			Desc.ByteWidth = byteWidth;
			Desc.Usage = usage;
		}
	};

	struct ConstantBufferDesc : public BufferDesc {
		ConstantBufferDesc() : BufferDesc() {}

		explicit ConstantBufferDesc(D3D11_USAGE usage, uint32_t byteWidth, uint32_t cpuAccessFlags = 0) {
			Desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

			Desc.CPUAccessFlags = cpuAccessFlags;
			Desc.ByteWidth = byteWidth;
			Desc.Usage = usage;
		}
	};

	struct StructuredBufferDesc : public BufferDesc {
		StructuredBufferDesc() : BufferDesc() {}

		explicit StructuredBufferDesc(D3D11_USAGE usage, D3D11_BIND_FLAG binding, uint32_t byteWidth, uint32_t structureByteStride, uint32_t cpuAccessFlags = 0) {
			Desc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
			
			Desc.CPUAccessFlags = cpuAccessFlags;
			Desc.Usage = usage;
			Desc.ByteWidth = byteWidth;
			Desc.StructureByteStride = structureByteStride;
			Desc.BindFlags = binding;
		}
	};

	struct TypedBufferDesc : public BufferDesc {
		TypedBufferDesc() : BufferDesc() {}

		explicit TypedBufferDesc(D3D11_USAGE usage, D3D11_BIND_FLAG binding, uint32_t byteWidth, uint32_t structureByteStride, uint32_t cpuAccessFlags = 0) {
			Desc.CPUAccessFlags = cpuAccessFlags;
			Desc.Usage = usage;
			Desc.ByteWidth = byteWidth;
			Desc.StructureByteStride = structureByteStride;
			Desc.BindFlags = binding;
		}
	};

	class Buffer : public NonCopyable {
		friend class Graphics;
	public:		
		virtual ~Buffer();

		static D3D11_BUFFER_DESC sDescribe(const BufferDesc* bufferDesc);
		void Fill(const void* data, size_t dataSize, D3D11_MAP mapType, uint32_t mapFlags=0, uint32_t subresource=0);

		ID3D11Buffer* GetD3DBuffer() const { return m_Buffer; }

	protected:
		ID3D11Buffer *m_Buffer = nullptr;
		D3D11_BUFFER_DESC m_Desc;

	private:
		Buffer(ID3D11Buffer* buffer);
	};

	class VertexBuffer : public Buffer {
	public:
		~VertexBuffer();
		
	private:

	};

	class IndexBuffer : public Buffer {
	public:
		~IndexBuffer();

	private:

	};

	class ConstantBuffer : public Buffer {
	public:
		~ConstantBuffer();

	private:

	};

	class StructuredBuffer : public Buffer {
	public:
		~StructuredBuffer();

	private:

	};

	class TypedBuffer : public Buffer {
	public:
		~TypedBuffer();

	private:

	};
}