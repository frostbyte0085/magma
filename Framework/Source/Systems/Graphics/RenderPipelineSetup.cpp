#include "RenderPipelineSetup.hpp"
#include <Application/Application.hpp>
#include <Utilities/Hash.hpp>

using namespace Magma;

namespace Magma {
	std::unordered_map<size_t, RenderPipelineSetup*> gRPSInstances;

	RenderPipelineSetup* gGetRenderPipelineSetup(RenderPipelineDesc* inDesc) {
		RenderPipelineSetup* rps = nullptr;

		size_t hash = Hash::HashState<RenderPipelineDesc>(inDesc);

		auto it = gRPSInstances.find(hash);
		if (it == gRPSInstances.end()) {
			gGfx->CreateRenderPipelineSetup(inDesc, &rps);
			rps->SetHash(hash);

			gRPSInstances[hash] = rps;
		}
		else {
			rps = it->second;
		}

		return rps;
	}

	void gDeallocateRenderPipelineSetup(RenderPipelineSetup* const inRPS) {
		gAssert(inRPS != nullptr, "Expected a valid RenderPipelineSetup instance");

		auto it = gRPSInstances.find(inRPS->GetHash());
		if (it != gRPSInstances.end()) {
			gRPSInstances.erase(it);
		}

		delete inRPS;
	}

	void gFreeRenderPipelineSetups() {
		/*
		iterator not dereferencable?

		for (auto& it : gRPSInstances) {
			RenderPipelineSetup* rps = it.second;
			SDELETE(rps);
		}

		gRPSInstances.clear();
		*/
	}
}