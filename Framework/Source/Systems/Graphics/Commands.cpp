#include "Commands.hpp"
#include "RenderPipelineSetup.hpp"
#include "Shader.hpp"
#include "Bindings.hpp"
#include "Buffer.hpp"
#include <Application/Application.hpp>

using namespace Magma;

//
// EmptyCommand
//
EmptyCommand::EmptyCommand(const std::string& inTestString) : m_TestString(inTestString) {

}

EmptyCommand::~EmptyCommand() {

}

void EmptyCommand::Execute() {
	gTrace(m_TestString.c_str());
}

//
// BindResourcesCommand
//
void BindResourcesCommand::Execute() {
	gGfx->SetGraphicsResourceBindings(ResourceBindings);
}

//
// SetRenderPipelineSetupCommand
//
SetRenderPipelineSetupCommand::SetRenderPipelineSetupCommand(const RenderPipelineDesc& inRPSDesc) {
	RPS = gGetRenderPipelineSetup((RenderPipelineDesc*)&inRPSDesc);
}

void SetRenderPipelineSetupCommand::Execute() {	
	gAssert(RPS != nullptr, "Expected a valid RenderPipelineSetup instance");

	gGfx->SetRenderPipelineSetup(RPS);
}

//
// FillBufferCommand
//
void FillBufferCommand::Execute() {
	ID3D11DeviceContext* context = gGfx->GetD3DContext();
	assert(context);
	
	D3D11_MAPPED_SUBRESOURCE ms;
	context->Map(TargetBuffer->GetD3DBuffer(), SubResource, MapType, MapFlags, &ms);
	memcpy(ms.pData, Data, Size);
	context->Unmap(TargetBuffer->GetD3DBuffer(), SubResource);
}

//
//  DrawCommandBase
//
void DrawCommandBase::Execute() {	
	gGfx->SetTopology(Topology);	
}

//
// DrawCommand
//
void DrawCommand::Execute() {
	DrawCommandBase::Execute();

	gGfx->Draw(VertexCount, StartVertexLocation);
}

//
// IndexedDrawCommand
//
void IndexedDrawCommand::Execute() {
	DrawCommandBase::Execute();
	
	gGfx->DrawIndexed(IndexCount, StartIndexLocation, BaseVertexLocation);
}

//
// SetMRTCommand
//
void SetMRTCommand::Execute() {
	gGfx->SetMRT(MRT);
}

//
// ClearCommand
//
void ClearCommand::Execute() {
	gGfx->ClearMRT(Flags, Clear, gGfx->GetMRT());
}