#pragma once

#include "Graphics.hpp"

namespace Magma {
	class DepthStencilView : public NonCopyable {
		friend class Graphics;
	public:
		~DepthStencilView();

		inline ID3D11DepthStencilView* const GetDSV() const { return m_DSV; }
	private:
		DepthStencilView(ID3D11DepthStencilView* const view);
		ID3D11DepthStencilView *m_DSV = nullptr;
	};
}