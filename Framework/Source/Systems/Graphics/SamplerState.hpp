#pragma once

#include "Graphics.hpp"

namespace Magma {

	struct SamplerDesc {
		explicit SamplerDesc() {
			ZeroMemory(&Desc, sizeof(Desc));

			Desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
			Desc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
			Desc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
			Desc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
			Desc.MipLODBias = 0;
			Desc.MaxAnisotropy = 1;
			Desc.ComparisonFunc = D3D11_COMPARISON_NEVER;
			Desc.BorderColor[0] = 1.0f;
			Desc.BorderColor[1] = 1.0f;
			Desc.BorderColor[2] = 1.0f;
			Desc.BorderColor[3] = 1.0f;
			Desc.MinLOD = -3.402823466e+38F; // -FLT_MAX
			Desc.MaxLOD = 3.402823466e+38F; // FLT_MAX
		}

		explicit SamplerDesc(D3D11_FILTER filter, D3D11_TEXTURE_ADDRESS_MODE addressU, D3D11_TEXTURE_ADDRESS_MODE addressV, D3D11_TEXTURE_ADDRESS_MODE addressW, float mipLODBias, uint32_t maxAnisotropy,
			D3D11_COMPARISON_FUNC comparisonFunc, const float* borderColor, float minLOD, float maxLOD) {
			ZeroMemory(&Desc, sizeof(Desc));
			
			Desc.Filter = filter;
			Desc.AddressU = addressU;
			Desc.AddressV = addressV;
			Desc.AddressW = addressW;
			Desc.MipLODBias = mipLODBias;
			Desc.MaxAnisotropy = maxAnisotropy;
			Desc.ComparisonFunc = comparisonFunc;
			const float defaultColor[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
			if (!borderColor) borderColor = defaultColor;
			Desc.BorderColor[0] = borderColor[0];
			Desc.BorderColor[1] = borderColor[1];
			Desc.BorderColor[2] = borderColor[2];
			Desc.BorderColor[3] = borderColor[3];
			Desc.MinLOD = minLOD;
			Desc.MaxLOD = maxLOD;
		}

		D3D11_SAMPLER_DESC Desc;
	};

	class SamplerState : public NonCopyable {
		friend class Graphics;
	public:
		~SamplerState();

		static D3D11_SAMPLER_DESC sDescribe(const SamplerDesc* samplerDesc);
	private:
		SamplerState(ID3D11SamplerState* state);

		ID3D11SamplerState* m_State;
		D3D11_SAMPLER_DESC m_Desc;
	};
}