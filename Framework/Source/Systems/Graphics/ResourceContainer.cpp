#include "ResourceContainer.hpp"
#include "Buffer.hpp"
#include "Texture.hpp"
#include <Systems/Allocators.hpp>

using namespace Magma;

//
// BASE
//
ResourceBindingContainer::ResourceBindingContainer() {

}

ResourceBindingContainer::~ResourceBindingContainer() {

}

//
// GRAPHICS
//
GraphicsResourceBindingContainer::GraphicsResourceBindingContainer() {

}

GraphicsResourceBindingContainer::~GraphicsResourceBindingContainer() {

}

namespace Magma {
	// TODO: implement hashing and caching and don't fragment memory like this.
	GraphicsResourceBindingContainer* gAllocateGraphicsResourceBindingContainer() {
		GraphicsResourceBindingContainer* container = gScratchAlloc->AllocateType<GraphicsResourceBindingContainer>();//new GraphicsResourceBindingContainer();

		return container;
	}

	void gDeallocateGraphicsResourceBindingContainer(GraphicsResourceBindingContainer* const inContainer) {
		gAssert(inContainer != nullptr, "Expected a valid GraphicsResourceBindingContainer instance");

		//delete inContainer;
	}
}
//
// COMPUTE
//
ComputeResourceBindingContainer::ComputeResourceBindingContainer() {

}

ComputeResourceBindingContainer::~ComputeResourceBindingContainer() {

}