#pragma once

#include "Graphics.hpp"

namespace Magma {
	class ShaderResourceView : public NonCopyable {
		friend class Graphics;
	public:
		~ShaderResourceView();

	private:
		ShaderResourceView(ID3D11ShaderResourceView* view);

		ID3D11ShaderResourceView* m_View;
		D3D11_SHADER_RESOURCE_VIEW_DESC m_Desc;
	};
}