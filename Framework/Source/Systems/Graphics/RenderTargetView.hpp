#pragma once

#include "Graphics.hpp"

namespace Magma {
	class RenderTargetView : public NonCopyable {
		friend class Graphics;
	public:		
		~RenderTargetView();

		inline ID3D11RenderTargetView* const GetRTV() const { return m_RTV; }
	private:
		RenderTargetView(ID3D11RenderTargetView* const view);
		ID3D11RenderTargetView *m_RTV = nullptr;
	};
}