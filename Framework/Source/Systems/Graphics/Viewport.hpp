#pragma once

#include "Graphics.hpp"

namespace Magma {
	class Viewport {
		friend class Graphics;
	public:
		Viewport() { ZeroMemory(&m_Viewport, sizeof(m_Viewport)); }
		Viewport(const Viewport& other) { memcpy(&m_Viewport, &other.m_Viewport, sizeof(m_Viewport)); }
		~Viewport() {}

		inline void Set(float x, float y, float width, float height, float minZ, float maxZ) {
			m_Viewport.TopLeftX = x;
			m_Viewport.TopLeftY = y;
			m_Viewport.Width = width;
			m_Viewport.Height = height;
			m_Viewport.MinDepth = minZ;
			m_Viewport.MaxDepth = maxZ;
		}

		inline float GetX() const { return m_Viewport.TopLeftX; }
		inline float GetY() const { return m_Viewport.TopLeftY; }
		inline float GetWidth() const { return m_Viewport.Width; }
		inline float GetHeight() const { return m_Viewport.Height; }
		inline float GetMinZ() const { return m_Viewport.MinDepth; }
		inline float GetMaxZ() const { return m_Viewport.MaxDepth; }
		
	private:
		D3D11_VIEWPORT m_Viewport;
	};
}