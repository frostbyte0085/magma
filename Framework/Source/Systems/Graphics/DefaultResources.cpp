#include "DefaultResources.hpp"
#include <Application/Application.hpp>

using namespace Magma;

SamplerState* Magma::gPointSampler = nullptr;
SamplerState* Magma::gLinearSampler = nullptr;

void Magma::gCreateDefaultGfxResources() {
	SamplerDesc linear_desc;
	gGfx->CreateSamplerState(SamplerState::sDescribe(&SamplerDesc()), &gLinearSampler);

	SamplerDesc point_desc;
	point_desc.Desc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	gGfx->CreateSamplerState(SamplerState::sDescribe(&point_desc), &gPointSampler);
}

void Magma::gDestroyDefaultGfxResources() {
	SDELETE(gPointSampler);
	SDELETE(gLinearSampler);
}
