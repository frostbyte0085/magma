#include "UnorderedAccessView.hpp"

using namespace Magma;

UnorderedAccessView::UnorderedAccessView(ID3D11UnorderedAccessView* view) {
	assert(view);
	m_View = view;
	m_View->GetDesc(&m_Desc);
}

UnorderedAccessView::~UnorderedAccessView() {
	SRELEASE(m_View);
}