#pragma once

#include "Graphics.hpp"
#include "../../Math/Vector2.h"
#include "../../Math/Vector3.h"
#include "../../Math/Vector4.h"
#include "../../Math/Color.h"

namespace Magma {
	namespace VertexFormats {

		struct VertexP0 {
			int Position;
		};

		static bool operator==(const VertexP0& lhs, const VertexP0& rhs) {
			return (lhs.Position == rhs.Position);
		}

		struct VertexP0C0 {
			Vector3 Position;
			Color	VColor;
		};

		static bool operator==(const VertexP0C0& lhs, const VertexP0C0& rhs) {
			return (lhs.Position == rhs.Position) && (lhs.VColor == rhs.VColor);
		}

		struct VertexP0N0UV0 {
			Vector3 Position;
			Vector3 Normal;
			Vector2 UV;						
		};

		static bool operator==(const VertexP0N0UV0& lhs, const VertexP0N0UV0& rhs) {
			return (lhs.Position == rhs.Position) && (lhs.Normal == rhs.Normal) && (lhs.UV == rhs.UV);
		}
	}
}

MAKE_HASHABLE(Magma::VertexFormats::VertexP0, t.Position)
MAKE_HASHABLE(Magma::VertexFormats::VertexP0C0, t.Position, t.VColor)
MAKE_HASHABLE(Magma::VertexFormats::VertexP0N0UV0, t.Position, t.Normal, t.UV)