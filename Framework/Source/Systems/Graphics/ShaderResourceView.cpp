#include "ShaderResourceView.hpp"

using namespace Magma;

ShaderResourceView::ShaderResourceView(ID3D11ShaderResourceView* view) {
	assert(view);
	m_View = view;
	m_View->GetDesc(&m_Desc);
}

ShaderResourceView::~ShaderResourceView() {
	SRELEASE(m_View);
}
