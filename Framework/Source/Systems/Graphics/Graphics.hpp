//
//  Graphics.hpp
//  TerrainEngine
//
//  Created by Pantelis Lekakis on 07/02/2016.
//  Copyright © 2016 Pantelis Lekakis. All rights reserved.
//

#ifndef Graphics_hpp
#define Graphics_hpp

#include "../System.hpp"
#include "Enumerations.hpp"
#include <Math/MathInclude.h>

#ifndef MAGMA_MAX_VIEWPORTS
#define MAGMA_MAX_VIEWPORTS 16
#endif

namespace Magma {
	
	class Color;
	class ShaderResource;

	class Graphics : public System {
	public:
		Graphics();
		~Graphics();

		virtual void Begin() override;
		virtual void End() override;
		virtual void Update(float dt) override;
		virtual void Initialise() override;

		inline ID3D11DeviceContext* const GetD3DContext() const { return m_Context; }
		inline ID3D11Device* const GetD3DDevice() const { return m_Device; }

		// Creation functions
		void CreateRenderPipelineSetup(RenderPipelineDesc* const desc, RenderPipelineSetup** outRenderPipelineSetup);
		void CreateTexture1D(const D3D11_TEXTURE1D_DESC& desc, const D3D11_SUBRESOURCE_DATA* initialData, Texture1D** texOut);
		void CreateTexture2D(const D3D11_TEXTURE2D_DESC& desc, const D3D11_SUBRESOURCE_DATA* initialData, Texture2D** texOut);
		void CreateTexture2DFromCurrentSwapChain(uint32_t bufferIndex, Texture2D** texOut);
		void CreateTexture3D(const D3D11_TEXTURE3D_DESC& desc, const D3D11_SUBRESOURCE_DATA* initialData, Texture3D** texOut);
		void CreateViewport(float x, float y, float width, float height, float minZ, float maxZ, Viewport** viewportOut);
		void CreateSamplerState(const D3D11_SAMPLER_DESC& desc, SamplerState** samplerStateOut);
		void CreateBuffer(const D3D11_BUFFER_DESC& desc, const D3D11_SUBRESOURCE_DATA* initialData, Buffer** bufferOut);
		void CreateRenderTargetView(Texture* texture, const DXGI_FORMAT fmt, RenderTargetView** viewOut);
		void CreateDepthStencilView(Texture* texture, const DXGI_FORMAT fmt, bool inReadOnly, DepthStencilView** viewOut);
		void CreateShaderResourceView(Texture* texture, const DXGI_FORMAT fmt, ShaderResourceView** viewOut);
		void CreateShaderResourceView(Buffer* buffer, uint32_t numElementsInBuffer, ShaderResourceView** viewOut);
		void CreateUnorderedAccessView(Texture* texture, const DXGI_FORMAT fmt, UnorderedAccessView** viewOut);
		void CreateUnorderedAccessView(Buffer* buffer, uint32_t numElementsInBuffer, bool isAppendBuffer, const DXGI_FORMAT typedBufferFmt, UnorderedAccessView** viewOut);
		void CreateShader(const std::wstring& filename, ERenderTechnique technique, Shader** shaderOut);
		void CreateShaderFromBlob(ID3DBlob* sourceBlob, ERenderTechnique technique, Shader** shaderOut);

		RenderTarget* GetMRT() const { return m_CurrentRenderTarget; }

		// Pipeline functions
		void SetMRT(RenderTarget* const inRenderTarget);
		void ClearMRT(EClearFlag inClearFlag, const RenderTargetClear& inClear, RenderTarget* const inRenderTarget);
		void ClearRenderTargetView(RenderTargetView* const view, const Color& color);
		void ClearDepthStencilView(DepthStencilView* const view, EClearFlag clearFlags, float clearDepth, uint8_t clearStencil);
		void SetShader(Shader* const shader, bool applyLayout=true);
		void SetConstantBuffers(const std::vector<ConstantBufferBinding>& inBindings);
		void SetVertexBuffers(uint32_t inStartSlot, const std::vector<VertexBufferBinding>& inBindings);
		void SetIndexBuffer(const IndexBufferBinding& inBinding);
		void SetViewports(uint32_t numViewports, Viewport* const* viewports);
		void SetResources(const std::vector<SRVBinding>& inBindings);
		void SetSamplers(const std::vector<SamplerBinding>& inBindings);
		void SetRenderPipelineSetup(RenderPipelineSetup* const inRenderPipeline);
		void SetRenderPipelineSetup(RenderPipelineSetup* const inRenderPipeline, uint8_t inStencilRef, const Color& inBlendFactor);
		void SetGraphicsResourceBindings(GraphicsResourceBindingContainer* const inContainer);
		void SetComputeResourceBindings(ComputeResourceBindingContainer* const inContainer);
		void SetTopology(D3D11_PRIMITIVE_TOPOLOGY inTopology);

		// Drawing
		inline void Draw(uint32_t vertexCount, uint32_t startVertexLocation) {
			assert(m_Context);

			m_Context->Draw(vertexCount, startVertexLocation);
		}

		inline void DrawAuto() {
			assert(m_Context);

			m_Context->DrawAuto();
		}

		inline void DrawIndexed(uint32_t indexCount, uint32_t startIndexLocation, uint32_t baseVertexLocation) {
			assert(m_Context);

			m_Context->DrawIndexed(indexCount, startIndexLocation, baseVertexLocation);
		}	

		RenderTarget* const GetBackbufferTarget() const { return m_BackbufferTarget; }

		void ExecuteCommandLists(CommandList** const inLists, uint32_t inSize);

	private:		
		void createDevice();
		void getClientSize();
		void updateViewport();
		void postInitialize();

		const DXGI_FORMAT kBackbufferFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
		const uint32_t kSwapChainBufferCount = 1;
		
		HWND m_hWnd;

		ID3D11DeviceContext* m_Context = nullptr;
		ID3D11Device* m_Device = nullptr;
		IDXGISwapChain* m_Swapchain = nullptr;

		Viewport* m_Viewport = nullptr;
		
		RenderTarget* m_BackbufferTarget = nullptr;
		RenderTarget* m_CurrentRenderTarget = nullptr;
	};
}
#endif /* Graphics_hpp */
