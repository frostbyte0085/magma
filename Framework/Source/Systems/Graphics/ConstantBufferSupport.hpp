#pragma once

#include <Math/Matrix4x4.h>

namespace Magma {
	struct PerFrameBuffer {
		Matrix4x4 ViewMatrix;
		Matrix4x4 ProjectionMatrix;
		Matrix4x4 ViewProjectionMatrix;
		Matrix4x4 InvViewMatrix;
		Matrix4x4 InvProjectionMatrix;
		Matrix4x4 InvViewProjectionMatrix;
	};

	struct PerObjectBuffer {
		Matrix4x4 WorldMatrix;
	};
}