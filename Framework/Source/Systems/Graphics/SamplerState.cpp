#include "SamplerState.hpp"

using namespace Magma;

SamplerState::SamplerState(ID3D11SamplerState* state) {
	assert(state);
	m_State = state;
	m_State->GetDesc(&m_Desc);
}

SamplerState::~SamplerState() {
	SRELEASE(m_State);
}

D3D11_SAMPLER_DESC SamplerState::sDescribe(const SamplerDesc* samplerDesc) {
	assert(samplerDesc);

	return std::move(samplerDesc->Desc);
}