#pragma once

#include "Graphics.hpp"

namespace Magma {
	
	extern bool gIsFormatTypeless(DXGI_FORMAT inFmt);

	struct TextureDesc1D {
		TextureDesc1D() { ZeroMemory(&Desc, sizeof(Desc)); }

		explicit TextureDesc1D(uint32_t width, uint32_t mipLevels, DXGI_FORMAT fmt, D3D11_USAGE usage=D3D11_USAGE_DEFAULT, D3D11_BIND_FLAG binding=D3D11_BIND_SHADER_RESOURCE, uint32_t arraySize=1, uint32_t cpuAccessFlags=0, uint32_t miscFlags=0) {
			ZeroMemory(&Desc, sizeof(Desc));

			Desc.Width = width;
			Desc.MipLevels = mipLevels;
			Desc.Format = fmt;
			Desc.Usage = usage;
			Desc.BindFlags = binding;
			Desc.ArraySize = arraySize;
			Desc.CPUAccessFlags = cpuAccessFlags;
			Desc.MiscFlags = miscFlags;
		}

		D3D11_TEXTURE1D_DESC Desc;
	};

	struct TextureDesc2D {
		TextureDesc2D() { ZeroMemory(&Desc, sizeof(Desc)); }

		explicit TextureDesc2D(uint32_t width, uint32_t height, uint32_t mipLevels, DXGI_FORMAT fmt, D3D11_USAGE usage = D3D11_USAGE_DEFAULT, D3D11_BIND_FLAG binding = D3D11_BIND_SHADER_RESOURCE, uint32_t arraySize = 1, uint32_t cpuAccessFlags = 0, uint32_t miscFlags = 0) {
			ZeroMemory(&Desc, sizeof(Desc));

			Desc.Width = width;
			Desc.Height = height;
			Desc.MipLevels = mipLevels;
			Desc.Format = fmt;
			Desc.Usage = usage;
			Desc.BindFlags = binding;
			Desc.ArraySize = arraySize;
			Desc.CPUAccessFlags = cpuAccessFlags;
			Desc.MiscFlags = miscFlags;

			Desc.SampleDesc.Count = 1;
		}

		D3D11_TEXTURE2D_DESC Desc;
	};

	struct TextureDesc3D {
		TextureDesc3D() { ZeroMemory(&Desc, sizeof(Desc)); }

		explicit TextureDesc3D(uint32_t width, uint32_t height, uint32_t depth, uint32_t mipLevels, DXGI_FORMAT fmt, D3D11_USAGE usage = D3D11_USAGE_DEFAULT, D3D11_BIND_FLAG binding = D3D11_BIND_SHADER_RESOURCE, uint32_t cpuAccessFlags = 0, uint32_t miscFlags = 0) {
			ZeroMemory(&Desc, sizeof(Desc));
			
			Desc.Width = width;
			Desc.Height = height;
			Desc.Depth = depth;
			Desc.MipLevels = mipLevels;
			Desc.Format = fmt;
			Desc.Usage = usage;
			Desc.BindFlags = binding;
			Desc.CPUAccessFlags = cpuAccessFlags;
			Desc.MiscFlags = miscFlags;
		}

		D3D11_TEXTURE3D_DESC Desc;
	};

	class Texture : public NonCopyable {
		friend class Graphics;
	public:		
		virtual ~Texture();
		
		static D3D11_TEXTURE1D_DESC sDescribe1D(const TextureDesc1D* desc);
		static D3D11_TEXTURE2D_DESC sDescribe2D(const TextureDesc2D* desc);
		static D3D11_TEXTURE3D_DESC sDescribe3D(const TextureDesc3D* desc);

		virtual inline DXGI_FORMAT GetFormat() const = 0;
		virtual inline bool CanBindSRV() const = 0;
		virtual inline bool CanBindRTV() const = 0;
		virtual inline bool CanBindUAV() const = 0;
		virtual inline bool CanBindDSV() const = 0;

		bool IsTypeless() const {
			const DXGI_FORMAT& fmt = GetFormat();
			return gIsFormatTypeless(fmt);
		}
	protected:
		Texture(ID3D11Resource* resource);
		ID3D11Resource *m_Resource = nullptr;
	};

	class Texture1D : public Texture {
		friend class Graphics;
	public:		
		~Texture1D();
		
		virtual inline DXGI_FORMAT GetFormat() const { return m_Desc.Format; }
		virtual inline bool CanBindSRV() const { return (m_Desc.BindFlags & D3D11_BIND_SHADER_RESOURCE) != 0; }
		virtual inline bool CanBindRTV() const { return (m_Desc.BindFlags & D3D11_BIND_RENDER_TARGET) != 0; }
		virtual inline bool CanBindUAV() const { return (m_Desc.BindFlags & D3D11_BIND_UNORDERED_ACCESS) != 0; }
		virtual inline bool CanBindDSV() const { return (m_Desc.BindFlags & D3D11_BIND_DEPTH_STENCIL) != 0; }

	private:
		Texture1D(ID3D11Texture1D* tex1D);
		D3D11_TEXTURE1D_DESC m_Desc;
	};

	class Texture2D : public Texture {
		friend class Graphics;
	public:		
		~Texture2D();
		
		virtual inline DXGI_FORMAT GetFormat() const { return m_Desc.Format; }
		virtual inline bool CanBindSRV() const { return (m_Desc.BindFlags & D3D11_BIND_SHADER_RESOURCE) != 0; }
		virtual inline bool CanBindRTV() const { return (m_Desc.BindFlags & D3D11_BIND_RENDER_TARGET) != 0; }
		virtual inline bool CanBindUAV() const { return (m_Desc.BindFlags & D3D11_BIND_UNORDERED_ACCESS) != 0; }
		virtual inline bool CanBindDSV() const { return (m_Desc.BindFlags & D3D11_BIND_DEPTH_STENCIL) != 0; }

	private:
		Texture2D(ID3D11Texture2D* tex2D);
		D3D11_TEXTURE2D_DESC m_Desc;
	};

	class Texture3D : public Texture {
		friend class Graphics;
	public:
		~Texture3D();

		virtual inline DXGI_FORMAT GetFormat() const { return m_Desc.Format; }
		virtual inline bool CanBindSRV() const { return (m_Desc.BindFlags & D3D11_BIND_SHADER_RESOURCE) != 0; }
		virtual inline bool CanBindRTV() const { return (m_Desc.BindFlags & D3D11_BIND_RENDER_TARGET) != 0; }
		virtual inline bool CanBindUAV() const { return (m_Desc.BindFlags & D3D11_BIND_UNORDERED_ACCESS) != 0; }
		virtual inline bool CanBindDSV() const { return (m_Desc.BindFlags & D3D11_BIND_DEPTH_STENCIL) != 0; }

	private:
		Texture3D(ID3D11Texture3D* tex3D);
		D3D11_TEXTURE3D_DESC m_Desc;
	};
}