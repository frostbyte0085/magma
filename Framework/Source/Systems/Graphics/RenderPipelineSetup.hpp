#include "Graphics.hpp"

#pragma once

namespace Magma {
	struct RenderPipelineDesc {
		RenderPipelineDesc() { 
			BlendDesc = CD3D11_BLEND_DESC::CD3D11_BLEND_DESC(CD3D11_DEFAULT());
			RasterizerDesc = CD3D11_RASTERIZER_DESC::CD3D11_RASTERIZER_DESC(CD3D11_DEFAULT());
			DepthStencilDesc = CD3D11_DEPTH_STENCIL_DESC::CD3D11_DEPTH_STENCIL_DESC(CD3D11_DEFAULT());
		}

		RenderPipelineDesc(Shader* const inShader, bool inApplyInputLayout, ERenderPipelineSetupFlags inCreationFlags = ERenderPipelineSetupFlags::RPS_CREATE_ALL) { 
			BlendDesc = CD3D11_BLEND_DESC::CD3D11_BLEND_DESC(CD3D11_DEFAULT());
			RasterizerDesc = CD3D11_RASTERIZER_DESC::CD3D11_RASTERIZER_DESC(CD3D11_DEFAULT());
			DepthStencilDesc = CD3D11_DEPTH_STENCIL_DESC::CD3D11_DEPTH_STENCIL_DESC(CD3D11_DEFAULT());
			ApplyInputLayout = inApplyInputLayout;

			ShaderInstance = inShader;
		}

		D3D11_RASTERIZER_DESC RasterizerDesc;
		D3D11_BLEND_DESC BlendDesc;
		D3D11_DEPTH_STENCIL_DESC DepthStencilDesc;
		uint32_t SampleMask = 0xffffffff;
		bool ApplyInputLayout = true;

		ERenderPipelineSetupFlags CreationFlags = ERenderPipelineSetupFlags::RPS_CREATE_ALL;

		Shader* ShaderInstance = nullptr;
	};

	class RenderPipelineSetup {
		friend class Graphics;
	public:
		~RenderPipelineSetup() {
			SRELEASE(BlendState);
			SRELEASE(RasterizerState);
			SRELEASE(DepthStencilState);
		}

		inline void SetHash(size_t inHash) { Hash = inHash; }
		inline size_t GetHash() const { return Hash; }

		Shader* const GetShaderInstance() const { return ShaderInstance; }

	private:
		RenderPipelineSetup(ID3D11BlendState* const inBlendState, ID3D11RasterizerState* const inRasterizerState, ID3D11DepthStencilState* const inDepthStencilState, Shader* const inShader, uint32_t inSampleMask, bool inApplyInputLayout) {
			BlendState = inBlendState;
			RasterizerState = inRasterizerState;
			DepthStencilState = inDepthStencilState;
			SampleMask = inSampleMask;
			ApplyInputLayout = inApplyInputLayout;

			ShaderInstance = inShader;
		}

		ID3D11BlendState *BlendState = nullptr;
		ID3D11RasterizerState *RasterizerState = nullptr;
		ID3D11DepthStencilState *DepthStencilState = nullptr;

		uint32_t SampleMask = 0xffffffff;
		bool ApplyInputLayout = true;
		Shader* ShaderInstance = nullptr;

		size_t Hash = 0;
	};

	// RenderPipelineSetup management
	extern RenderPipelineSetup* gGetRenderPipelineSetup(RenderPipelineDesc* inDesc);
	extern void gDeallocateRenderPipelineSetup(RenderPipelineSetup* const inRPS);
	extern void gFreeRenderPipelineSetups();
	//
}
