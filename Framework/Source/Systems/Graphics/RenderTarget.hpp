#pragma once

#include "Graphics.hpp"
#include <Math/Color.h>

namespace Magma {
	struct RenderTargetClear {
		static RenderTargetClear sClear;

		RenderTargetClear() {
			for (uint32_t i = 0; i < D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT; i++) {
				ClearColor[i] = Color(0.0f, 0.0f, 0.0f, 0.0f);
			}
		}

		RenderTargetClear(const Color& inClearColor, float inClearDepth = 1.0f, uint8_t inClearStencil = 0) {

		}

		Color ClearColor[D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT];
		float ClearDepth = 1.0f;
		uint8_t ClearStencil = 0;
	};

	class RenderTargetData {
	public:
		virtual ~RenderTargetData() { Release(); }

		virtual void Release();

		bool OwnTexture = false;
		bool OwnView = false;
		DXGI_FORMAT Format = DXGI_FORMAT_UNKNOWN;

		union {
			Texture2D *Texture = nullptr;
		};
	};

	class RenderTargetColor : public RenderTargetData {
	public:
		~RenderTargetColor() {}

		virtual void Release() override;
				
		RenderTargetView* RTV = nullptr;
		ShaderResourceView* SRV = nullptr;
	};

	class RenderTargetDepth : public RenderTargetData {
	public:
		~RenderTargetDepth() {}
		
		virtual void Release() override;

		DepthStencilView* DSVReadOnly = nullptr;
		DepthStencilView* DSV = nullptr;
		ShaderResourceView* SRV = nullptr;
	};

	class RenderTarget : public NonCopyable {
	public:
		RenderTarget();
		~RenderTarget();
		
		void Initialise(uint32_t inCount, uint32_t inWidth, uint32_t inHeight, uint32_t inDepth = 0);
		void SetColorTarget(uint32_t inIndex, DXGI_FORMAT fmt, D3D11_USAGE usage = D3D11_USAGE_DEFAULT);
		void SetColorTarget(uint32_t inIndex, Texture* const colorTexture);
		void SetColorTarget(uint32_t inIndex, RenderTargetView* const inRTV, ShaderResourceView* const inSRV);
		void SetDepthTarget(DXGI_FORMAT fmt, D3D11_USAGE usage = D3D11_USAGE_DEFAULT);
		void SetDepthTarget(Texture* const depthTexture);
		void SetDepthTarget(DepthStencilView* const inDSV, ShaderResourceView* const inSRV);

		inline const RenderTargetColor& GetColorTarget(uint32_t idx) { assert((idx >= 0) && (idx < m_ColorTargetCount)); return m_ColorTargets[idx]; }
		inline const RenderTargetDepth& GetDepthTarget() { return m_DepthTarget; }

		uint32_t GetColorTargetCount() const { return m_ColorTargetCount; }
		inline bool HasDepth() const { return m_DepthTarget.DSV != nullptr; }
		inline uint32_t GetWidth() const { return m_Width; }
		inline uint32_t GetHeight() const { return m_Height; }
		inline uint32_t GetDepth() const { return m_Depth; }
	private:
		RenderTargetColor *m_ColorTargets = nullptr;
		RenderTargetDepth m_DepthTarget;

		uint32_t m_ColorTargetCount = 0;
		uint32_t m_Width = 0;
		uint32_t m_Height = 0;
		uint32_t m_Depth = 0;

		void createColorResources(RenderTargetColor& ioColor);
		void createDepthResources(RenderTargetDepth& ioDepth);
	};
}