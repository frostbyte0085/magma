#include "Library.h"

DEFERRED_PASS_OUTPUT ComputeDeferredOutput(VS_OUTPUT_DEFERRED_GBUFFER input) {
	DEFERRED_PASS_OUTPUT output;

	output.Albedo = input.Color.xyz;
	output.Normal = EncodeNormal(input.Normal);
	output.Reflectivity = float4(0.5, 0, 0, 0);

	return output;
}