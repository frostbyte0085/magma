#ifndef LIBRARY_H_
#define LIBRARY_H_

#include "TechniqueVertexStructures.h"
#include "ConstantBufferSupport.h"
#include "TechniqueEntryFunctions.h"

//
// Post
//
void GetNDCTriangle(uint vid, out float4 position, out float2 uv) {
	position = float4((float)(vid / 2) * 4.0 - 1.0, (float)(vid % 2) * 4.0 - 1.0, 0.0, 1.0);	
	uv = float2((float)(vid / 2) * 2.0, 1.0 - (float)(vid % 2) * 2.0);
}

//
// Normal Encode/Decode
//
float3 DecodeNormal(float2 inEncodedNormal) {
	float2 fenc = inEncodedNormal * 4 - 2;
	float f = dot(fenc, fenc);
	float g = sqrt(1 - f / 4);

	float3 n;
	n.xy = fenc*g;
	n.z = 1 - f / 2;
	return n;
}

float3 DecodeNormal(float3 inEncodedNormal) {
	return DecodeNormal(inEncodedNormal.xy);
}

float3 DecodeNormal(float4 inEncodedNormal) {
	return DecodeNormal(inEncodedNormal.xy);
}

float2 EncodeNormal(float3 inNormal) {
	float f = sqrt(8 * inNormal.z + 8);
	return inNormal.xy / f + 0.5;
}

float2 EncodeNormal(float4 inNormal) {
	return EncodeNormal(inNormal.xyz);
}

#endif // LIBRARY_H_