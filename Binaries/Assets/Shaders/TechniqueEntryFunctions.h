#ifndef TECHNIQUE_ENTRY_FUNCTIONS_H_
#define TECHNIQUE_ENTRY_FUNCTIONS_H_

// DEFERRED GEOMETRY PASS
struct DEFERRED_PASS_OUTPUT {
	float3 Albedo;
	float2 Normal;
	float4 Reflectivity;
};

DEFERRED_PASS_OUTPUT ComputeDeferredOutput(VS_OUTPUT_DEFERRED_GBUFFER input);

VS_OUTPUT_DEFERRED_GBUFFER vs_main_deferred_geometry(const VS_INPUT_DEFERRED_GBUFFER input)
{
	VS_OUTPUT_DEFERRED_GBUFFER output = (VS_OUTPUT_DEFERRED_GBUFFER)0;

	float4x4 mvp = mul(ViewProjectionMatrix, WorldMatrix);

	output.Position = mul(mvp, float4(input.Position, 1.0));

	float4x4 mv = mul(ViewMatrix, WorldMatrix);
	output.Normal = mul(ViewMatrix, float4(input.Normal, 0.0)).xyz;

	return output;
}

PS_OUTPUT_DEFERRED_GBUFFER ps_main_deferred_geometry(const VS_OUTPUT_DEFERRED_GBUFFER input)
{
	PS_OUTPUT_DEFERRED_GBUFFER output;

	DEFERRED_PASS_OUTPUT deferred_output = ComputeDeferredOutput(input);

	output.Albedo = float4(deferred_output.Albedo, 1.0);
	output.Normal = float4(deferred_output.Normal, 0.0, 0.0);
	output.Reflectivity = deferred_output.Reflectivity;

	return output;
}
//

#endif // TECHNIQUE_ENTRY_FUNCTIONS_H_