#include "Library.h"

struct VS_OUTPUT
{
    float4      Position        : SV_POSITION;
	float2		UV				: TEXCOORD0;
};

Texture2D Texture;
SamplerState Sampler;

VS_OUTPUT vs_main_fsq(uint id: SV_VertexID)
{
    VS_OUTPUT output = (VS_OUTPUT)0;

	GetNDCTriangle(id, output.Position, output.UV);

    return output;
}

float4 ps_main_fsq(const VS_OUTPUT input): SV_TARGET
{	
	float3 normal = DecodeNormal(Texture.Sample(Sampler, input.UV));

	return float4(normal, 1.0);
}