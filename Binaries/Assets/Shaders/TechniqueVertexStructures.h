#ifndef TECHNIQUE_VERTEX_STRUCTURES_H_
#define TECHNIQUE_VERTEX_STRUCTURES_H_

struct VS_INPUT_DEFERRED_GBUFFER
{
	float3      Position : POSITION;
	float3		Normal : NORMAL;
	float3		Tangent : TANGENT;
	float4		Color : COLOR;
	float2		UV : TEXCOORD0;
};

struct VS_OUTPUT_DEFERRED_GBUFFER
{
	float4      Position : SV_POSITION;
	float3		Normal : TEXCOORD0;
	float4		Color : COLOR;
};

struct PS_OUTPUT_DEFERRED_GBUFFER
{
	float4		Albedo : SV_Target0;
	float4		Normal : SV_Target1;
	float4		Reflectivity : SV_Target2;
};

#endif // TECHNIQUE_VERTEX_STRUCTURES_H_