cbuffer gPerFrameCB : register(b0) {
	float4x4 ViewMatrix;
	float4x4 ProjectionMatrix;
	float4x4 ViewProjectionMatrix;
	float4x4 InverseViewMatrix;
	float4x4 InverseProjectionMatrix;
	float4x4 InverseViewProjectionMatrix;
};


cbuffer gObjectCB : register(b1) {
	float4x4 WorldMatrix;
};

struct VS_INPUT
{
    float3      Position        : POSITION;
	float4		Color0			: COLOR0;
	float4		Color1			: COLOR1;
};

struct VS_OUTPUT
{
    float4      Position        : SV_POSITION;
	float4		Color			: COLOR;
};

VS_OUTPUT vs_main(const VS_INPUT input)
{
    VS_OUTPUT output = (VS_OUTPUT)0;

	float4x4 mvp = mul(ViewProjectionMatrix, WorldMatrix);

	output.Position = mul(mvp, float4(input.Position, 1.0f));

	output.Color = input.Color1;

    return output;
}

float4 ps_main(const VS_OUTPUT input): SV_TARGET
{
	return input.Color;
}