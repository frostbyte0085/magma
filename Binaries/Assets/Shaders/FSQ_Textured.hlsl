#include "Library.h"

struct VS_OUTPUT
{
    float4      Position        : SV_POSITION;
	float2		UV				: TEXCOORD0;
};

Texture2D GBuffer0;
Texture2D GBuffer1;

SamplerState GBufferSampler;

VS_OUTPUT vs_main_fsq(uint id: SV_VertexID)
{
    VS_OUTPUT output = (VS_OUTPUT)0;

	GetNDCTriangle(id, output.Position, output.UV);

    return output;
}

float4 ps_main_fsq(const VS_OUTPUT input): SV_TARGET
{
	//float3 normal = DecodeNormal(GBuffer1.Sample(GBufferSampler, input.UV).xy);
	float normal = GBuffer1.Sample(GBufferSampler, input.UV).x;

	return float4(normal, 0.0, 0.0, 1.0);
}