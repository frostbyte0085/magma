#ifndef CONSTANT_BUFFER_SUPPORT_H_
#define CONSTANT_BUFFER_SUPPORT_H_

cbuffer gPerFrameCB {
	float4x4 ViewMatrix;
	float4x4 ProjectionMatrix;
	float4x4 ViewProjectionMatrix;
	float4x4 InverseViewMatrix;
	float4x4 InverseProjectionMatrix;
	float4x4 InverseViewProjectionMatrix;
};


cbuffer gPerObjectCB {
	float4x4 WorldMatrix;
};

#endif // CONSTANT_BUFFER_SUPPORT_H_